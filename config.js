/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let config = {};

config.TOKEN = 'theworldisending';
config.GMAPS_KEY = 'AIzaSyA8nF6nrH77ue5Pcs8Y81TEIg5m0aDclmA';

config.OSRM_URL = 'http://46.101.30.43:5000/';

config.PORT = process.env.SERVICE_PORT || 3000;

// Set config options according to the environment the server is set to
if (process.env.NODE_ENV === 'development') {
    config.DB_URL = "mongodb://127.0.0.1:27017/StuberDB";

    config.BYPASS_AUTH = false;
    config.CONSOLE_LOG = true;
    config.CLEAR_MONGO_COLLECTIONS = false;
    config.CLEAR_REDIS_COLLECTIONS = true;

    config.KUE_DELAY = 10000;
    config.ACTIVE_LOCK_TIME = 15000;
} else if (process.env.NODE_ENV === 'test') {
    config.DB_URL = "mongodb://127.0.0.1:27017/StuberDB";

    config.BYPASS_AUTH = false;
    config.CONSOLE_LOG = false;
    config.CLEAR_MONGO_COLLECTIONS = true;
    config.CLEAR_REDIS_COLLECTIONS = true;

    config.KUE_DELAY = 10000;
    config.ACTIVE_LOCK_TIME = 10000;
}
// Default to production settings
else {
    config.DB_URL = 'mongodb://user:6330@ds147052.mlab.com:47052/stuber';

    config.BYPASS_AUTH = false;
    config.CONSOLE_LOG = false;
    config.CLEAR_MONGO_COLLECTIONS = false;
    config.CLEAR_REDIS_COLLECTIONS = true;

    config.KUE_DELAY = 120000;
    config.ACTIVE_LOCK_TIME = 90000;
}

module.exports = config;
