Run the command `npm install -g frontail`

This installs a package that will allow the live streaming of log files over the internet and provide the ability
to view them in any webbrowser. Simply run the `run.sh` bash script to start two instances in headless mode, each streaming a main
logging file of the Stuber server. The logs can be accessed with URLs {serverip}:9001 and {serverip}:9002.

NOTE: Syntax highlighting not working currently. ( Can't get it to work :'[ )

----
Go to the Github page for more info:
https://github.com/mthenw/frontail
----