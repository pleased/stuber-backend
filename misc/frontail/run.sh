#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd ${DIR}

frontail -n 250 -p 9001 -d --pid-path ./instance1.pid ../../log/logging.log
frontail -n 250 -p 9002 -d --pid-path ./instance2.pid ../../log/access.log