/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
module.exports = {

    // Inbox message types
    MESSAGE_TYPES: {
        STANDARD: 'STANDARD',
        SCHEDULE: 'SCHEDULE', // Anything that has to do with schedules
        SCHEDULE_ACTIVATED: 'SCHEDULE_ACTIVATED', // When a scheduled trip has activated
        CONFIRM: 'CONFIRM', // For a driver that has to accept or reject
        CONFIRM_RESULT_FALSE: 'CONFIRM_RESULT_FALSE', // For a passenger who waited for a response from driver
        CONFIRM_RESULT_TRUE: 'CONFIRM_RESULT_TRUE' // For a passenger who waited for a response from driver
    },

    // In seconds
    TIME_FRAMES: {
        PICKUP_BEFORE: 600,
        PICKUP_AFTER: 300,
        DROPOFF_AFTER: 300
    }
};
