/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let log4js = require('log4js');
let cluster = require('cluster');

let config = require('./config.js');

if (config.CONSOLE_LOG) {
    log4js.configure({
        appenders: [
            { type: 'console' },
            { type: 'file', filename: './log/logging.log' }
        ]
    });
} else {
    log4js.configure({
        appenders: [
            { type: 'file', filename: './log/logging.log' }
        ]
    });
}

let logger = log4js.getLogger('Stuber');
logger.setLevel('DEBUG');

module.exports = {

    logDebug: function (message) {
      logger.debug((cluster.worker ? '[Worker #' + cluster.worker.id + '] ' : '[Master] ') +  message);
    },

    logInfo: function (message) {
        logger.info((cluster.worker ? '[Worker #' + cluster.worker.id + '] ' : '[Master] ') +  message);
    },

    logWarn: function (message) {
      logger.warn((cluster.worker ? '[Worker #' + cluster.worker.id + '] ' : '[Master] ') +  message);
    },

    logFatal: function (message) {
        logger.fatal((cluster.worker ? '[Worker #' + cluster.worker.id + '] ' : '[Master] ') +  message);
    },

    logRequest: function (url, headers, body) {
        logger.info('Request on ' + url);
        logger.debug('Request headers = ' + JSON.stringify(headers, null, 2));
        logger.debug('Request body = ' + JSON.stringify(body, null, 2));
    }
};
