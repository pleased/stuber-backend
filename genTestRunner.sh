#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILE="$DIR/test.sh"

echo "#!/usr/bin/env bash" > ${FILE}
echo "" >> ${FILE}
echo "DIR=\"\$( cd \"\$( dirname \"\${BASH_SOURCE[0]}\" )\" && pwd )\"" >> ${FILE}
echo "cd \${DIR}" >> ${FILE}
echo "" >> ${FILE}

for filename in ${DIR}/tests/*.js; do
    echo "Found file" ${filename}
    echo "./node_modules/mocha/bin/_mocha \${DIR}/tests/${filename##*/} --compilers js:babel-core/register --timeout 40000 --reporter list;" >> ${FILE}
done