# Stuber

### Setting up the frontend
**TODO**

----
### Setting up the Backend

#### Installing

Install the following

 * [NodeJS v8.9.x](https://nodejs.org/en/)
 * [MongoDB v3.4.x](https://www.mongodb.com/)
 * [Redis](https://redis.io/)

Check that it was installed correctly

`node --version` should display `v8.9.x`

After having installed the software required by the server we need to install the NodeJS packages. Run the following commands to do so.

    $ cd backend
    $ npm install

This installs the packages defined in `package.json` in the `node_modules` folder where the server can access it.

The following installs NodeJS packages in a global folder.

    $ npm install -g nodemon
    $ npm install -g cross-env
    $ npm install -g apidoc

#### Configuration

There are three configurations that the server can be run in and each has a bash file to start the specific configuration.

1. Production => `start.sh` or `npm run-script start`
2. Development => `start-dev.sh` or `npm run-script dev`
3. Test => `start-test.sh` or `npm run-script test`

Each configuration can be configured in the `config.js` file.

#### Running the server

To run the server simply use the command

    $ npm start

To run the server in headless mode, a package called `pm2` is required. Install it with

    $ npm install -g pm2

Now start the server with `pm2` with

    $ pm2 start npm -- start

To automatically start the server when the server machine is turned on or rebooted run

    $ pm2 startup

This requires the server to have been started with `pm2 start npm -- start` first.

#### Documentation

Automatic documentation for the REST endpoints can also be generated. To do this run

    $ ./genRestApiDocs.sh

This will create a folder `apidoc`. In it there will be a `index.html` that when opened will present the documentation in a clean
looking website format.

### Setup OSRM server

Follow [this guide](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-osrm-server-on-ubuntu-14-04) to setup your own OSRM server or use the Docker image provided on the [OSRM Github](https://github.com/Project-OSRM/osrm-backend) page. Alternatively the free public OSRM server at [https://router.project-osrm.org](https://router.project-osrm.org) can be used for testing the system. It is not intended for production use.

#### Misc

In the folder `/misc/frontail` there is a bash script that will allow for live streaming of log files from the server. Read the readme located in the folder for more details.
