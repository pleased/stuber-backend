/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let checks = require('../util/checks.js');

module.exports = function(router) {

    /**
     * @api {get} /gps/ Get GPS coordinates of all drivers
     * @apiName GetAllGPS
     * @apiGroup GPS
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the GPS coordinates have successfully been retrieved.
     * @apiSuccess {[Object]} data
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully retrieved all GPS coordinates",
     *       "data": [
     *          {
     *            "sid": "ByOFSUmBZ",
     *            "currLoc": {
     *              "lat": -33.5678,
     *              "lon": 33.5678
     *            }
     *          }
     *       ]
     *     }
     *
     * @apiError NoActiveDrivers No active drivers to retrieve GPS coordinates from
     * @apiErrorExample No Active Drivers:
     *     HTTP/1.1 204 NoContent
     *     {
     *       "success": True,
     *       "message": "No GPS coordinates exist"
     *     }
     *
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/gps', function(req, res) {

        mdb.mdbModules.active.getAllLatLong().then(items => {
            if (items.length === 0) {
                res.status(204).json({success: true, message: 'No GPS coordinates exist'});
            } else {
                res.status(200).json({success: true, message: 'Successfully retrieved all GPS coordinates', data: items});
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /gps/:id Get GPS coordinates of driver
     * @apiName GetGPS
     * @apiGroup GPS
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the GPS coordinates have successfully been retrieved.
     * @apiSuccess {Object} data
     * @apiSuccess {Number} data.lat Current latitude of the driver
     * @apiSuccess {Number} data.lon Current longitude of the driver
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully retrieved user's GPS coordinates",
     *       "data": {
     *          "lat": -33.5678,
     *          "lon": 33.5678
     *       }
     *     }
     *
     * @apiUse UserNotOnTrip
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/gps/:id', function(req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return mdb.mdbModules.util.checkActiveExists({sid: sid});
            }, 'The user is not currently on a trip');
        }).then(() => {
            // Get the GPS coordinates of a specific user
            return mdb.mdbModules.active.getLatLong(sid);
        }).then(item => {
            res.status(200).json({success: true, message: 'Successfully retrieved user\'s GPS coordinates', data: item});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {put} /gps/:id Update GPS coordinates of driver
     * @apiName UpdateGPS
     * @apiGroup GPS
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {Number} lat Driver's new latitude.
     * @apiParam {Number} lon Driver's new longitude.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the driver's position has successfully been updated.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully updated GPS coordinates"
     *     }
     *
     * @apiUse UserNotOnTrip
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.put('/gps/:id', function (req, res) {

        let sid = req.params.id;
        let json = req.body;

        let pass = checks.validGPSUpdateSettings(json);
        if (pass !== true) {
            return res.status(400).json({success: false, message: pass});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return mdb.mdbModules.util.checkActiveExists({sid: sid});
            }, 'The user is not currently on a trip');
        }).then(() => {
            // Update/Add GPS coordinates for an active user
            return mdb.mdbModules.active.setLatLong(sid, json.lat, json.lon);
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully updated GPS coordinates'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });
};
