/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let logger = require('../logger.js');

module.exports = function (router) {

    /**
     * Get a list of all endpoints that are available on the server
     */
    router.get('/debug/endpoints', function (req, res) {

        let routes = [];

        router.stack.forEach(item => {
            routes.push(Object.keys(item['route']['methods'])[0].toUpperCase() + ' : ' + item['route']['path']);
        });

        res.status(200).json(routes);
    });

    /**
     * Get all the users that are in the system
     */
    router.get('/debug/users', function(req, res) {

        logger.logRequest(req.originalUrl, req.headers, req.body);

        // Get all users
        mdb.mdbModules.debug.getAllUsers((err, message, items) => {
            if (!err) {
                res.status(200).json({success: true, message: message, data: items});
            } else {
                res.status(400).json({success: false, message: message});
            }
        });
    });

    /**
     * Delete everything in the MongoDB and RedisDB
     */
    router.delete('/debug/clean', function (req, res) {

        logger.logRequest(req.originalUrl, req.headers, req.body);

        mdb.dropCollections().then(rdb.dropCollections).then(() => {
            res.status(200).json("Cleared everything");
        }).catch(err => {
            res.status(400).json({
                message: "Could not clear everything",
                error: err
            });
        });
    });
};
