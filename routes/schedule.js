/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let consts = require('../consts.js');

let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let checks = require('../util/checks.js');
let myProcess = require('../util/process.js'); // Called this to not overwrite native process variable

let kue = require('../controller/kue.js');
let logger = require('../logger.js');

module.exports = function (router) {

    /**
     * Schedule a trip for a user
     */
    router.post('/user/:id/trip/schedule', function (req, res) {

        let sid = req.params.id;
        let json = req.body;
        let Tid;

        // Check that valid settings were provided in the request
        let pass = checks.validTripScheduleSettings(json);
        if (pass !== true) {
            return res.status(400).json({success: false, message: pass});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return rdb.vidExists(json.vid);
            }, 'Vehicle does not exist');
        }).then(() => {
            return checks.isWrapper(() => {
                return mdb.mdbModules.user.isUserDriver(sid);
            }, 'User is not a driver');
        }).then(() => {
            return mdb.mdbModules.vehicle.getVehicle(json.vid);
        }).then(vehicle => {
            // TODO: Check that the user has not scheduled a trip in specified time-slot already
            // Check if the user is an owner of the vehicle
            if (vehicle.owners.includes(sid) === false) {
                return Promise.reject('User is not the owner of the vehicle');
            }

            // Check that the number of open seats specified is less than the number of seats the vehicle has
            // 1 subtracted for driver seat
            let pass = checks.validScheduleOpenSeats(json, vehicle.seats - 1);
            if (pass !== true) {
                return Promise.reject(pass);
            }

            myProcess.time(json);
            return mdb.mdbModules.schedule.scheduleTrip(sid, json);
        }).then(tid => {
            Tid = tid;
            return rdb.addTid(Tid);
        }).then(() => {
            res.status(200).json({success: true, message: 'Trip successfully scheduled', data: {tid: Tid}});

            if (json.singleDate) {
                kue.addTripSingleDateJob({tid: Tid, sid: sid}).then(() => {
                    logger.logInfo('Successfully added TripSingleDateJob for user ' + sid);
                }).catch(err => {
                    logger.logWarn('Something went wrong with TripSingleDateJob job for user ' + sid);
                });
            } else if (json.weekRepeat) {
                kue.addTripWeeklyJob({tid: Tid, sid: sid}).then(() => {
                    logger.logInfo('Successfully added TripWeeklyJob for user ' + sid);
                }).catch(err => {
                    logger.logWarn('Something went wrong with TripWeeklyJob job for user ' + sid);
                });
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/trip/schedule Get scheduled trips
     * @apiName GetScheduledTrips
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiDescription Get the scheduled trips that the user has made
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the list of scheduled trips were retrieved
     * @apiSuccess {Object} data
     * @apiSuccess {Object} [trips] The list of schedules trips
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Successfully retrieved user's scheduled trips",
     *            "data": {
     *                "trips": [
     *                    {
     *                        "vid": "SyMKaHlEjb",
     *                        "weekRepeat": true,
     *                        "singleDate": false,
     *                        "date": "2017-09-23T17:41:48.186Z",
     *                        "startTime": "2017-09-23T03:45:48.301Z",
     *                        "endTime": "2017-09-23T05:20:48.301Z",
     *                        "sid": "SJY6HxEob",
     *                        "tid": "r1EypG4sW",
     *                        "weekRepeatOpenSeats": {
     *                            "sunday": 0,
     *                            "saturday": 0,
     *                            "friday": 1,
     *                            "thursday": 1,
     *                            "wednesday": 2,
     *                            "tuesday": 0,
     *                            "monday": 1
     *                        },
     *                        "weekRepeatRoute": {
     *                            "sunday": [],
     *                            "saturday": [],
     *                            "friday": [
     *                                {
     *                                    "lat": -33.934272,
     *                                    "lon": 18.862424
     *                                }
     *                            ],
     *                            "thursday": [
     *                                {
     *                                    "lat": -33.934272,
     *                                    "lon": 18.862424
     *                                }
     *                            ],
     *                            "wednesday": [
     *                                {
     *                                    "lat": -33.934272,
     *                                    "lon": 18.862424
     *                                }
     *                            ],
     *                            "tuesday": [],
     *                            "monday": [
     *                                {
     *                                    "lat": -33.934272,
     *                                    "lon": 18.862424
     *                                }
     *                            ]
     *                        },
     *                        "passengers": {
     *                            "sunday": [],
     *                            "saturday": [],
     *                            "friday": [],
     *                            "thursday": [],
     *                            "wednesday": [],
     *                            "tuesday": [],
     *                            "monday": []
     *                        },
     *                        "days": {
     *                            "sunday": false,
     *                            "saturday": false,
     *                            "friday": true,
     *                            "thursday": true,
     *                            "wednesday": true,
     *                            "tuesday": false,
     *                            "monday": true
     *                        },
     *                        "singleDateRoute": [],
     *                        "singleDateOpenSeats": 2,
     *                        "singleDatePassengers": [],
     *                        "destination": {
     *                            "lat": -33.934272,
     *                            "lon": 18.862424
     *                        },
     *                        "origin": {
     *                            "lat": -33.927337,
     *                            "lon": 18.863926
     *                        }
     *                    }
     *                ]
     *            }
     *        }
     *
     * @apiSuccessExample Success-Response 204:
     *     HTTP/1.1 204 No Content
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/trip/schedule', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.schedule.getScheduledTrips(sid);
        }).then(items => {
            if (items.length !== 0) {
                res.status(200).json({
                    success: true,
                    message: 'Successfully retrieved user\'s scheduled trips',
                    data: {trips: items}
                });
            } else {
                res.status(204).json({success: true, message: 'User does not have any scheduled trips'});
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {delete} /user/:id/trip/schedule/:tid Cancel scheduled trip
     * @apiName DeleteScheduleTrip
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {String} tid The ID of the lift to delete
     *
     * @apiDescription Delete the specified scheduled trip. THe trip itself will be deleted in about 90 seconds
     * after the request has been made
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the trip has been scheduled for deletion.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "The trip has been scheduled for deletion"
     *        }
     *
     * @apiUse UserNotFoundError
     * @apiUse ScheduledTripDoesNotExist
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.delete('/user/:id/trip/schedule/:tid', function (req, res) {

        let sid = req.params.id;
        let tid = req.params.tid;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return rdb.tidExists(tid);
            }, 'Trip with specified ID does not exist');
        }).then(() => {
            // Set this trip to be deleted. If any kue jobs detect this id has been marked for deletion they will cancel (fail)
            return rdb.addMarkedForDeletionId(tid);
        }).then(() => {
            return kue.addRemoveScheduledTrip({tid: tid});
        }).then(() => {
            res.status(200).json({success: true, message: 'The trip has been scheduled for deletion'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /user/:id/trip/schedule/:tid/activate Activate scheduled trip
     * @apiName ActivateScheduleTrip
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {String} tid The ID of the lift to activate
     *
     * @apiDescription Activate the specified scheduled trip. If will only activate if the singleDate, if singelDate trip,
     * or a weekRepeat day, if weekRepeat trip, matches the current date.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the trip has been scheduled for deletion.
     * @apiSuccess {Object} data
     * @apiSuccess {String} aid The AID for the trip that was activated
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Trip has been activated. Enjoy your trip",
     *            "data": {
     *                "tid": "rybOkV4sZ"
     *            }
     *        }
     *
     * @apiUse UserNotFoundError
     * @apiUse ScheduledTripDoesNotExist
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/trip/schedule/:tid/activate', function (req, res) {

        let sid = req.params.id;
        let tid = req.params.tid;

        // TODO: Check that the user is not already on a trip
        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return rdb.tidExists(tid);
            }, 'Trip with specified ID does not exist');
        }).then(() => {
            return mdb.mdbModules.schedule.getScheduledTrip(tid);
        }).then(item => {
            if (item.length === 0) {
                return Promise.reject('Could not active the scheduled trip');
            } else {
                item = item[0];

                let now = new Date();
                let scheduleDate = item.date;

                let Aid;
                let passengerSid = [];

                // Check if it is a singleDate or weekRepeat trip. This will determine what settings to use
                if (item.singleDate) {

                    // Check that the date is correct before activating trip
                    if (now.getUTCFullYear() === scheduleDate.getUTCFullYear() && now.getUTCMonth() === scheduleDate.getUTCMonth() && now.getUTCDate() === scheduleDate.getUTCDate()) {
                        // Get the information of all the passengers
                        mdb.mdbModules.schedule.getMultiScheduledLifts(item.singleDatePassengers).then(items => {
                            for (let i = 0; i < items.length; i = i + 1) {
                                passengerSid.push(items[i].sid);
                            }

                            return mdb.mdbModules.active.activateScheduledTrip({
                                vid: item.vid,
                                sid: item.sid,

                                destination: item.destination,
                                origin: item.origin,

                                startTime: item.startTime,
                                endTime: item.endTime,

                                currLoc: item.origin,

                                openSeats: item.singleDateOpenSeats,

                                route: item.singleDateRoute
                            }, items);
                        }).then(aid => {
                            Aid = aid;

                            // Send messages to all the passengers
                            let calls = [];
                            for (let i = 0; i < passengerSid.length; i = i + 1) {
                                calls.push(mdb.mdbModules.inbox.addMessage(passengerSid[i], `The trip (${tid}) that you were scheduled to has activated. Prepare yourself`, consts.MESSAGE_TYPES.SCHEDULE_ACTIVATED));
                            }

                            return Promise.all(calls);
                        }).then(() => {
                            res.status(200).json({
                                success: true,
                                message: `Trip has been activated. Enjoy your trip`,
                                data: {aid: Aid}
                            });
                        }).catch(err => {
                            res.status(400).json({success: false, message: err});
                        });
                    } else {
                        return Promise.reject('Scheduled date does not match current date');
                    }
                } else {
                    let day = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'][new Date().getDay()];

                    // Check that the day of the week is correct before activating trip
                    if (item.days[day] === true) {
                        // Get the information of all the passengers
                        mdb.mdbModules.schedule.getMultiScheduledLifts(item.passengers[day]).then(items => {
                            for (let i = 0; i < items.length; i = i + 1) {
                                passengerSid.push(items[i].sid);
                            }

                            return mdb.mdbModules.active.activateScheduledTrip({
                                vid: item.vid,
                                sid: item.sid,

                                destination: item.destination,
                                origin: item.origin,

                                startTime: item.startTime,
                                endTime: item.endTime,

                                currLoc: item.origin,

                                openSeats: item.weekRepeatOpenSeats[day],

                                route: item.weekRepeatRoute[day]
                            }, items);
                        }).then(aid => {
                            Aid = aid;

                            // Send messages to all the passengers
                            let calls = [];
                            for (let i = 0; i < passengerSid.length; i = i + 1) {
                                calls.push(mdb.mdbModules.inbox.addMessage(passengerSid[i], `The trip (${tid}) that you were scheduled to has activated. Prepare yourself`, consts.MESSAGE_TYPES.SCHEDULE_ACTIVATED));
                            }

                            return Promise.all(calls);
                        }).then(() => {
                            res.status(200).json({
                                success: true,
                                message: `Trip has been activated. Enjoy your trip`,
                                data: {aid: Aid}
                            });
                        }).catch(err => {
                            res.status(400).json({success: false, message: err});
                        });
                    } else {
                        return Promise.reject('Scheduled day does not match current day');
                    }
                }
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * Schedule a lift for a user
     */
    router.post('/user/:id/lift/schedule', function (req, res) {

        let sid = req.params.id;
        let json = req.body;
        let Lid;

        // Check that valid settings were provided in the request
        let pass = checks.validLiftScheduleSettings(json);
        if (pass !== true) {
            return res.status(400).json({success: false, message: pass});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            // TODO: Check that the user has not scheduled a trip in specified time-slot already
            myProcess.time(json);
            return mdb.mdbModules.schedule.scheduleLift(sid, json);
        }).then(lid => {
            Lid = lid;
            return rdb.addLid(Lid);
        }).then(() => {
            // TODO: This might not be required if front-end uses messages supplied by responses
            return mdb.mdbModules.inbox.addMessage(sid, 'You will be notified when you have been assigned to a trip', consts.MESSAGE_TYPES.SCHEDULE);
        }).then(() => {
            res.status(200).json({
                success: true,
                message: 'Lift successfully scheduled. Check inbox for messages when assigned to a driver',
                data: {lid: Lid}
            });

            if (json.singleDate) {
                kue.addLiftSingleDateJob({lid: Lid, sid: sid}).then(() => {
                    logger.logInfo('Successfully added LiftSingleDateJob for user ' + sid);
                }).catch(err => {
                    logger.logWarn('Something went wrong with LiftSingleDateJob job for user ' + sid);
                });
            } else if (json.weekRepeat) {
                kue.addLiftWeeklyJob({lid: Lid, sid: sid}).then(() => {
                    logger.logInfo('Successfully added LiftWeeklyJob for user ' + sid);
                }).catch(err => {
                    logger.logWarn('Something went wrong with LiftWeeklyJob job for user ' + sid);
                });
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/lift/schedule Get scheduled lifts
     * @apiName GetScheduledLifts
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiDescription Get the scheduled lifts that the user has made
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the list of scheduled lifts were retrieved
     * @apiSuccess {Object} data
     * @apiSuccess {Object} [lifts] The list of schedules lifts
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *           "success": true,
     *           "message": "Successfully retrieved user's scheduled lifts",
     *           "data": {
     *               "lifts": [
     *                   {
     *                       "weekRepeat": true,
     *                       "singleDate": false,
     *                       "date": "2017-09-23T17:46:27.213Z",
     *                       "startTime": "2017-09-23T02:45:27.268Z",
     *                       "endTime": "2017-09-23T05:20:27.268Z",
     *                       "sid": "SygK6HlVsW",
     *                       "lid": "rkix0MVsW",
     *                       "driver": {
     *                           "sunday": "",
     *                           "saturday": "",
     *                           "friday": "",
     *                           "thursday": "",
     *                           "wednesday": "",
     *                           "tuesday": "",
     *                           "monday": ""
     *                       },
     *                       "days": {
     *                           "sunday": true,
     *                           "saturday": false,
     *                           "friday": false,
     *                           "thursday": true,
     *                           "wednesday": true,
     *                           "tuesday": false,
     *                           "monday": true
     *                       },
     *                       "singleDateDriver": "",
     *                       "destination": {
     *                           "lat": -33.93429,
     *                           "lon": 18.862424
     *                       },
     *                       "origin": {
     *                           "lat": -33.927337,
     *                           "lon": 18.864
     *                       }
     *                   }
     *               ]
     *           }
     *       }
     *
     * @apiSuccessExample Success-Response 204:
     *     HTTP/1.1 204 No Content
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/lift/schedule', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.schedule.getScheduledLifts(sid);
        }).then(items => {
            if (items.length === 0) {
                res.status(204).json({success: true, message: 'User does not have any scheduled lifts'});
            } else {
                res.status(200).json({
                    success: true,
                    message: 'Successfully retrieved user\'s scheduled lifts',
                    data: {lifts: items}
                });
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {delete} /user/:id/lift/schedule/:lid Cancel scheduled lift
     * @apiName DeleteScheduleLift
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {String} lid The ID of the lift to delete
     *
     * @apiDescription Delete the specified scheduled lift. THe lift itself will be deleted in about 90 seconds
     * after the request has been made
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the lift has been scheduled for deletion.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "The lift has been scheduled for deletion"
     *        }
     *
     * @apiUse UserNotFoundError
     * @apiUse ScheduledLiftDoesNotExist
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.delete('/user/:id/lift/schedule/:lid', function (req, res) {

        let sid = req.params.id;
        let lid = req.params.lid;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return rdb.lidExists(lid);
            }, 'Lift with specified ID does not exist');
        }).then(() => {
            // Set this lift to be deleted. If any kue jobs detect this id has been marked for deletion they will cancel (fail)
            return rdb.addMarkedForDeletionId(lid);
        }).then(() => {
            return kue.addRemoveScheduledLift({lid: lid});
        }).then(() => {
            res.status(200).json({success: true, message: 'The lift has been scheduled for deletion'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });
};
