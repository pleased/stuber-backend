/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let config = require('../config.js');
let consts = require('../consts.js');

let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let checks = require('../util/checks.js');
let myProcess = require('../util/process.js'); // Called this to not overwrite native process variable

let script = require('../controller/script.js');
let logger = require('../logger.js');

module.exports = function (router) {

    /**
     * @api {post} /user/:id/trip Set a trip
     * @apiName SetTrip
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiParam {String} vid Vehicle unique ID.
     * @apiParam {Object} origin
     * @apiParam {Number} origin.lat Latitude of the trip's origin.
     * @apiParam {Number} origin.lon Longitude of the trip's origin.
     * @apiParam {Object} destination
     * @apiParam {Number} destination.lat Latitude of the trip's destination.
     * @apiParam {Number} destination.lon Longitude of the trip's destination.
     * @apiParam {Object} currLoc
     * @apiParam {Number} currLoc.lat Latitude of the current location of the driver.
     * @apiParam {Number} currLoc.lon Longitude of the current location of the driver.
     * @apiParam {Object} startTime
     * @apiParam {Number} startTime.hour The hour portion of when the driver leaves from their origin.
     * @apiParam {Number} startTime.minute The minute portion of when driver leaves from their origin.
     * @apiParam {Object} endTime
     * @apiParam {Number} endTime.hour The hour portion of when the drivers wants to reach their destination.
     * @apiParam {Number} endTime.minute The minute portion of when the drivers wants to reach their destination.
     * @apiParam {Number} openSeats The number of passengers the driver's vehicle can support on the trip.
     *
     * @apiExample {json} Example
     * {
     *    "vid": "HJGQz3_78",
     *    "destination": {
     *      "lat": -33.934272,
     *      "lon": 18.862424
     *    },
     *    "origin": {
     *      "lat": -33.927337,
     *      "lon": 18.863926
     *    },
     *    "currLoc": {
     *      "lat": -33.927337,
     *      "lon": 18.863926
     *    },
     *
     *    "startTime": {
     *      "hour": 8,
     *      "minute": 30
     *    },
     *     "endTime": {
     *      "hour": 11,
     *      "minute": 50
     *    },
     *
     *    "openSeats": 1
     *  }
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that trip was set successfully.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.trip Unique ID for the trip.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Trip set successfully",
     *       "data": {
     *          "trip": "TD74_a2TY"
     *       }
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse VehicleNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/trip', function (req, res) {

        let sid = req.params.id;
        let json = req.body;
        let Aid;

        // Check that valid settings were provided in the request
        let pass = checks.validTripSettings(json);
        if (pass !== true) {
            return res.status(400).json({success: false, message: pass});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return rdb.vidExists(json.vid);
            }, 'Vehicle does not exist');
        }).then(() => {
            return checks.isNotWrapper(() => {
                return mdb.mdbModules.util.checkActiveExists({sid: sid})
            }, 'User is already on a trip');
        }).then(() => {
            return checks.isWrapper(() => {
                return mdb.mdbModules.user.isUserDriver(sid);
            }, 'User is not a driver');
        }).then(() => {
            return mdb.mdbModules.vehicle.getVehicle(json.vid);
        }).then(vehicle => {
            // Check if the user is an owner of the vehicle
            if (vehicle.owners.includes(sid) === false) {
                return Promise.reject('User is not the owner of the vehicle');
            }

            // Check that the number of open seats specified is less than the number of seats the vehicle has
            // 1 subtracted for driver seat
            if (json.openSeats > vehicle.seats - 1) {
                return Promise.reject('Number of open seats specified is more than the vehicle supports');
            }

            // All checks passes. Set the trip
            myProcess.time(json);
            return mdb.mdbModules.active.setTrip(sid, json);
        }).then(aid => {
            Aid = aid;

            // Save the trip setting to user's history
            return mdb.mdbModules.user.addTripHistory(sid, json);
        }).then(() => {
            return rdb.addAid(Aid);
        }).then(() => {
            res.status(200).json({success: true, message: 'Trip set successfully', data: {aid: Aid}});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });


    /**
     * @api {get} /user/:id/trip Get the trip the user is the driver of
     * @apiName GetDriverTrip
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that trip was retrieved successfully.
     * @apiSuccess {Object} data
     * @apiSuccess {Object} data.trip
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *       "success": true,
     *       "message": "Successfully retrieved user's trip",
     *       "data": {
     *           "trip": {
     *               "vid": "rJlhdOj78W",
     *               "startTime": "2017-07-24T06:30:56.437Z",
     *               "endTime": "2017-07-24T09:50:56.437Z",
     *               "openSeats": 1,
     *               "sid": "SJouOjmU-",
     *               "aid": "ryEY_o7Ub",
     *               "traveled": 0,
     *               "originalTravelDistance": 0,
     *               "route": [],
     *               "pickups": [],
     *               "passengers": [],
     *               "currLoc": {
     *                   "lat": -33.927337,
     *                   "lon": 18.863926
     *               },
     *               "origin": {
     *                   "lat": -33.927337,
     *                   "lon": 18.863926
     *               },
     *               "destination": {
     *                   "lat": -33.934272,
     *                   "lon": 18.862424
     *               }
     *             }
     *         }
     *      }
     *
     * @apiError UserNotOnTrip The User is not the owner of a trip
     * @apiErrorExample User Not On Trip:
     *     HTTP/1.1 204 NoContent
     *     {
     *       "success": True,
     *       "message": "The user is not currently on a trip"
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse VehicleNotFoundError
     * @apiUse UserNotOnTrip
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/trip', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.active.getTripWithSid(sid);
        }).then(item => {
            if (item.length === 0) {
                res.status(204).json({success: true, message: 'The user is not currently on a trip'});
            } else {
                res.status(200).json({
                    success: true,
                    message: 'Successfully retrieved user\'s trip',
                    data: {trip: item[0]}
                });
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /trip/:aid Get the specified trip
     * @apiName GetTrip
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} aid Active's unique ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that trip was retrieved successfully.
     * @apiSuccess {Object} data
     * @apiSuccess {Object} data.trip
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *        {
     *          "success": true,
     *          "message": "Successfully retrieved trip",
     *          "data": {
     *                "trip": {
     *                  "vid": "HJfg66MxoW",
     *                  "startTime": "2017-09-20T06:30:19.812Z",
     *                  "endTime": "2017-09-20T09:50:19.812Z",
     *                  "openSeats": 2,
     *                  "sid": "Hkg6pMej-",
     *                  "aid": "Sk4MlXxiZ",
     *                  "traveled": 0,
     *                  "originalTravelDistance": 0,
     *                  "route": [
     *                     {
     *                         "lat": -33.934272,
     *                         "lon": 18.862424
     *                     }
     *                  ],
     *                  "pickups": [],
     *                  "passengers": [],
     *                  "currLoc": {
     *                     "lat": -33.927337,
     *                     "lon": 18.863926
     *                  },
     *                  "origin": {
     *                      "lat": -33.927337,
     *                      "lon": 18.863926
     *                  },
     *                  "destination": {
     *                     "lat": -33.934272,
     *                     "lon": 18.862424
     *                  }
     *              }
     *          }
     *      }
     *
     * @apiUse ActiveDoesNotExist
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/trip/:aid', function (req, res) {

        let aid = req.params.aid;

        checks.isWrapper(() => {
            return rdb.aidExists(aid);
        }, 'Trip does not exist').then(() => {
            return mdb.mdbModules.active.getTripWithAid(aid);
        }).then(item => {
            res.status(200).json({success: true, message: 'Successfully retrieved trip', data: {trip: item[0]}});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /user/:id/trip/complete Complete the trip the user is the driver of
     * @apiName CompleteTrip
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that trip was successfully completed.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *        {
     *           "success": true,
     *           "message": "Successfully marked the trip as complete"
     *       }
     *
     * @apiUse UserNotFoundError
     * @apiUse UserNotOnTrip
     * @apiUse PassengersPresent
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/trip/complete', function (req, res) {

        let sid = req.params.id;
        let Aid;

        rdb.sidExists(sid).then(is => {
            return checks.is(is, 'User does not exist');
        }).then(() => {
            return mdb.mdbModules.util.checkActiveExists({sid: sid});
        }).then(is => {
            Aid = is;
            return checks.is(is, 'User is not on a trip');
        }).then(() => {
            return mdb.mdbModules.active.tripHasLifts(sid);
        }).then(has => {
            if (has) {
                return Promise.reject('Trip could not be set as complete as it still has pickups/passengers in it');
            } else {
                return mdb.mdbModules.active.removeTrip(sid);
            }
        }).then(() => {
            return rdb.removeAid(Aid);
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully marked the trip as complete'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/trip/requests/:aid Get passenger requesting lift
     * @apiName GetRequestingPassenger
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {String} aid Active ID.
     *
     * @apiDescription Get the details of the passengers that are waiting for a accept/reject from the driver.
     * This endpoint will have content after a passenger has selected a driver.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the passenger could be retrieved.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *           "success": true,
     *           "message": "Successfully retrieved the passengers on the driver's waiting list",
     *           "data": {
     *               "passenger": {
     *                   "startTime": "2017-09-23T06:45:26.283Z",
     *                   "endTime": "2017-09-23T09:25:26.283Z",
     *                   "sid": "Skg1RakNjb",
     *                   "origin": {
     *                       "lat": -33.930313,
     *                       "lon": 18.863897
     *                   },
     *                   "destination": {
     *                       "lat": -33.933589,
     *                       "lon": 18.86227
     *                   }
     *               }
     *           }
     *       }
     *
     * @apiSuccessExample Success-Response 204:
     *     HTTP/1.1 204 No Content
     *
     * @apiUse UserNotFoundError
     * @apiUse ActiveDoesNotExist
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/trip/requests/:aid', function (req, res) {

        let sid = req.params.id;
        let aid = req.params.aid;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return rdb.aidExists(aid);
            }, 'The supplied AID does not exist in the system');
        }).then(() => {
            return mdb.mdbModules.waitingList.getDriverWaitingList(aid);
        }).then(item => {
            if (!!item) {
                mdb.mdbModules.user.getUser(item.sid).then(user => {
                    item.fullname = user.fullname;
                    res.status(200).json({
                        success: true,
                        message: 'Successfully retrieved the passengers on the driver\'s waiting list',
                        data: {passenger: item}
                    });
                }).catch(err => {
                    res.status(400).json({success: false, message: err});
                });
            } else {
                res.status(204).json({success: true, message: 'The driver has no passengers in their waiting list'});
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /user/:id/trip/:aid/decided Driver decide
     * @apiName DriverDecide
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {String} aid Active ID.
     *
     * @apiParam {Boolean} decided True for accept and False for reject.
     * @apiParam {String} passenger The SID of the passenger the decision is for.
     *
     * @apiDescription The driver either accepts or rejects a passenger's lift request from them. A message is placed
     * in the passenger's inbox to notify them of the driver's' decision
     *
     * @apiExample {json} Example
     *    {
     *        "decided": true,
     *        "passenger": "Byxx_MeEsW"
     *    }
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the decision has been made.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *          "success": true,
     *          "message": "Successfully notified the requesting user of your choice"
     *        }
     *
     * @apiUse UserNotFoundError
     * @apiUse ActiveDoesNotExist
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/trip/:aid/decided', function (req, res) {

        let driverSid = req.params.id;
        let aid = req.params.aid;
        let passengerSid = req.body.passenger;
        let json = req.body;

        // Check that valid settings were provided in the request
        let pass = checks.validLiftDecidedSettings(json);
        if (pass !== true) {
            return res.status(400).json({success: false, message: pass});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(driverSid);
        }, 'User does not exist').then(() => {
            return checks.isWrapper(() => {
                return rdb.sidExists(passengerSid);
            }, 'Passenger does not exist');
        }).then(() => {
            return checks.isWrapper(() => {
                return rdb.isPassengerInWaitingList(passengerSid);
            }, 'Passenger is not a waiting list for a driver');
        }).then(() => {
            return checks.isWrapper(() => {
                return rdb.aidExists(aid);
            }, 'The supplied AID does not exist in the system');
        }).then(() => {
            // Check that the lock time has not expired yet
            return rdb.getActiveRelease(aid);
        }).then(time => {

            let now = new Date().getTime();
            let expired;
            if (time !== null) {
                expired = (now - Number(time)) > config.ACTIVE_LOCK_TIME;
            }

            if (time === null || expired) {
                // Delete this user from the waitingList
                mdb.mdbModules.waitingList.removePassenger(passengerSid).then(() => {
                    return rdb.removePassengerFromWaitingList(passengerSid);
                }).then(() => {
                    logger.logInfo('Removed passenger from waiting lift after selection time period has expired');
                }).catch(err => {
                    logger.logWarn('Could not remove passenger from waiting lift after selection time period has expired
' + err);
                });

                return Promise.reject('You took to long to accept/reject the lift request. It has become invalid');
            } else {
                // Continue
                return Promise.resolve();
            }
        }).then(() => {
            if (json.decided === true) {
                // The driver has decided to give a lift to the passenger
                // Add the passenger to the trips active document and let the passenger know
                let passenger;
                mdb.mdbModules.waitingList.getAndRemovePassenger(passengerSid).then(item => {
                    passenger = item;
                    let route = item.drivers.filter(item => item.aid === aid)[0].route;

                    return mdb.mdbModules.active.addPickupToLift(passengerSid, aid, route, item);
                }).then(() => {
                    return mdb.mdbModules.inbox.addMessage(passengerSid, 'You have been assigned to your request driver', consts.MESSAGE_TYPES.CONFIRM_RESULT_TRUE);
                }).then(() => {
                    return rdb.removePassengerFromWaitingList(passengerSid);
                }).then(() => {
                    res.status(200).json({
                        success: true,
                        message: 'Successfully notified the requesting user of your choice'
                    });

                    return Promise.resolve();
                }).then(() => {
                    // Remove the lock from all the drivers the passenger had claimed
                    let calls = [];
                    for (let driver of passenger.drivers) {
                        calls.push(rdb.removeActiveRelease(driver.aid));
                    }

                    return Promise.all(calls);
                }).catch(err => {
                    res.status(400).json({success: false, message: err});
                });
            } else {
                // The driver has decided not to give a lift to the passenger
                // Remove the passenger from all waiting lists. If they still want a lift they have to redo everything. (States might have changed to much in the meantime this is why)
                let calls = [
                    mdb.mdbModules.inbox.addMessage(passengerSid, 'The driver that you requested has rejected your request for providing a lift', consts.MESSAGE_TYPES.CONFIRM_RESULT_FALSE),
                    mdb.mdbModules.waitingList.getAndRemovePassenger(passengerSid),

                    rdb.removePassengerFromWaitingList(passengerSid)
                ];

                Promise.all(calls).then(v => {
                    res.status(200).json({
                        success: true,
                        message: 'Successfully notified the requesting user of your choice'
                    });

                    return Promise.resolve(v[1]);
                }).then(passenger => {
                    // Remove the lock from all the drivers the passenger had claimed
                    let calls = [];
                    for (let driver of passenger.drivers) {
                        calls.push(rdb.removeActiveRelease(driver.aid));
                    }

                    return Promise.all(calls);
                }).catch(err => {
                    res.status(400).json({success: false, message: err});
                });
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /user/:id/lift Request a lift
     * @apiName RequestLift
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiParam {Object} origin
     * @apiParam {Number} origin.lat Latitude of the lift's origin.
     * @apiParam {Number} origin.lon Longitude of the lift's origin.
     * @apiParam {Object} destination
     * @apiParam {Number} destination.lat Latitude of the lift's destination.
     * @apiParam {Number} destination.lon Longitude of the lift's destination.
     * @apiParam {Object} startTime
     * @apiParam {Number} startTime.hour The hour portion of when the passenger wants to be picked up.
     * @apiParam {Number} startTime.minute The minute portion of when the passenger wants to be picked up.
     * @apiParam {Object} endTime
     * @apiParam {Number} endTime.hour The hour portion of when the passenger wants to be dropped off.
     * @apiParam {Number} endTime.minute The minute portion of when the passenger wants to be dropped off.
     *
     * @apiDescription Request a lift from drivers. A list containing drivers(AIDs) that the user can choose from is returned
     *
     * @apiExample {json} Example
     *    {
     *      "destination": {
     *        "lat": -33.933589,
     *        "lon": 18.862270
     *      },
     *      "origin": {
     *        "lat": -33.930313,
     *        "lon": 18.863897
     *      },
     *
     *      "startTime": {
     *        "hour": 8,
     *        "minute": 45
     *      },
     *      "endTime": {
     *        "hour": 11,
     *        "minute": 25
     *      }
     *    }
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that a list of drivers have been retrieved.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Successfully retrieved possible trip matches",
     *            "data": {
     *                "matches": [
     *                    "rkF8HeEob"
     *                ]
     *            }
     *        }
     *
     * @apiSuccessExample Success-Response 204:
     *     HTTP/1.1 204 No Content
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/lift', function (req, res) {

        let sid = req.params.id;
        let json = req.body;

        // Check that valid settings were provided in the request
        let pass = checks.validLiftSettings(json);
        if (pass !== true) {
            return res.status(400).json({success: false, message: pass});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isNotWrapper(() => {
                return rdb.isPassengerInWaitingList(sid);
            }, 'User is already on the waiting list for a trip');
        }).then(() => {
            return checks.isNotWrapper(() => {
                return mdb.mdbModules.util.checkActiveExists({sid: sid});
            }, 'Cannot request lift as user is already driver of active trip');
        }).then(() => {
            return mdb.mdbModules.active.getLift(sid);
        }).then(item => {
            if (item.length > 0) {
                return Promise.reject('User is already assigned to a driver');
            } else {
                json.sid = sid;
                myProcess.time(json);
                return script.getFittingTrips(json);
            }
        }).then(possibles => {
            // If there are no matches return a 204 status code
            if (possibles === 204) {
                return res.status(204).json({success: true, message: 'No trips available'});
            }

            mdb.mdbModules.user.addLiftHistory(sid, json).then(() => {
                if (json.instant === true) {
                    if (process.env.NODE_ENV === 'production') {
                        // Until further notice instant mode is not supported in official production mode due to race conditions
                        return Promise.reject('Until further notice Instant Mode is not supported in official production mode');
                    }

                    let driver = possibles[0][0];
                    let route = possibles[0][1];

                    mdb.mdbModules.active.addPickupToLift(sid, driver.aid, route, json).then(() => {
                        res.status(200).json({
                            success: true,
                            message: 'User successfully added to a trip',
                            data: {aid: driver.aid}
                        });
                    }).catch(err => {
                        res.status(400).json({success: false, message: err});
                    });
                } else {
                    // Mark the possibles as temporary taken. This prevents other lifts from stealing them
                    let calls = [];
                    let now = new Date().getTime();
                    for (let possible of possibles) {
                        calls.push(rdb.addActiveRelease(possible[0].aid, now));
                    }

                    Promise.all(calls).then(() => {
                        return mdb.mdbModules.waitingList.addPassenger(sid, json, possibles);
                    }).then(() => {
                        return rdb.addPassengerToWaitingList(sid);
                    }).then(() => {
                        // Get just the aid of the possibles
                        possibles = possibles.map(item => item[0].aid);
                        res.status(200).json({
                            success: true,
                            message: 'Successfully retrieved possible trip matches',
                            data: {matches: possibles}
                        });
                    }).catch(err => {
                        res.status(400).json({success: false, message: err});
                    });
                }
            }).catch(err => {
                res.status(400).json({success: false, message: err});
            });
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {delete} /user/:id/lift Cancel a lift
     * @apiName DeleteLift
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiDescription Cancel a lift request the user has made. This is only possible if the user has not
     * yet chosen a driver.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the lift request has been cancelled.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Successfully canceled the requested lift"
     *        }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.delete('/user/:id/lift', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            // Check that the user has not already decided on a driver
            return checks.isWrapper(() => {
                return mdb.mdbModules.waitingList.checkSelectedDriverEmpty(sid);
            }, 'Cannot cancel as a driver has already been decided on');
        }).then(() => {
            return mdb.mdbModules.waitingList.removePassenger(sid);
        }).then(() => {
            return rdb.removePassengerFromWaitingList(sid);
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully canceled the requested lift'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/lift Get lift
     * @apiName GetLift
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiDescription Get the trip data of which the user is a passenger of. This endpoint will only return data
     * if the user is an actual passenger, ie the driver has accepted their lift request.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the data could be retrieved.
     * @apiSuccess {Object} data
     * @apiSuccess {Object} trip Contains the data of the trip the user is a passenger of.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": false,
     *            "message": "Successfully retrieved the trip details the user is on",
     *            "data": {
     *                "trip": {
     *                    "vid": "SyMKaHlEjb",
     *                    "startTime": "2017-09-23T06:30:00.107Z",
     *                    "endTime": "2017-09-23T09:50:00.107Z",
     *                    "openSeats": 1,
     *                    "sid": "SJY6HxEob",
     *                    "aid": "rJnTBxEo-",
     *                    "traveled": 0,
     *                    "originalTravelDistance": 0,
     *                    "route": [
     *                        {
     *                            "lat": -33.930313,
     *                            "lon": 18.863897
     *                        },
     *                        {
     *                            "lat": -33.933589,
     *                            "lon": 18.86227
     *                        },
     *                        {
     *                            "lat": -33.934272,
     *                            "lon": 18.862424
     *                        }
     *                    ],
     *                    "pickups": [
     *                        {
     *                            "destination": {
     *                                "lat": -33.933589,
     *                                "lon": 18.86227
     *                            },
     *                            "origin": {
     *                                "lat": -33.930313,
     *                                "lon": 18.863897
     *                            },
     *                            "sid": "SygK6HlVsW",
     *                            "startTime": "2017-09-23T06:45:50.448Z",
     *                            "endTime": "2017-09-23T09:25:50.448Z"
     *                        }
     *                    ],
     *                    "passengers": [],
     *                    "currLoc": {
     *                        "lat": -33.927337,
     *                        "lon": 18.863926
     *                    },
     *                    "origin": {
     *                        "lat": -33.927337,
     *                        "lon": 18.863926
     *                    },
     *                    "destination": {
     *                        "lat": -33.934272,
     *                        "lon": 18.862424
     *                    }
     *                }
     *            }
     *        }
     *
     * @apiSuccessExample Success-Response 204:
     *     HTTP/1.1 204 No Content
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/lift', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.active.getLift(sid);
        }).then(item => {
            if (item.length > 0) {
                res.status(200).json({
                    success: false,
                    message: 'Successfully retrieved the trip details the user is on',
                    data: {trip: item[0]}
                });
            } else {
                res.status(204).json({success: true, message: 'The user is not currently assigned to a trip'});
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /user/:id/lift/selected Select driver
     * @apiName SelectDriver
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {String} aid The AID of the driver.
     *
     * @apiDescription Select a driver from the list that was returned when the user requested a lift. The selected driver
     * gets a message in their inbox.
     *
     * @apiExample {json} Example
     *    {
     *       "aid": "SkCr1Q4ob"
     *   }
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the lift request has been cancelled.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Successfully sent the request to the indicated driver"
     *        }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/lift/selected', function (req, res) {

        let passengerSid = req.params.id;
        let json = req.body;

        // Check that valid settings were provided in the request
        let pass = checks.validLiftSelectedSettings(json);
        if (pass !== true) {
            return res.status(400).json({success: false, message: pass});
        }

        checks.isWrapper(() => {
            return rdb.isPassengerInWaitingList(passengerSid);
        }, 'User is not a waiting list for a driver').then(() => {
            return checks.isWrapper(() => {
                return rdb.aidExists(json.aid);
            }, 'The supplied AID does not exist in the system');
        }).then(() => {
            return checks.isWrapper(() => {
                return mdb.mdbModules.waitingList.checkAidInDrivers(passengerSid, json.aid);
            }, 'The supplied AID is not registered a possibility for the user\'s trip');
        }).then(() => {
            return checks.isWrapper(() => {
                return mdb.mdbModules.waitingList.checkSelectedDriverEmpty(passengerSid);
            }, 'You already made a selection for a driver. Cannot do that again');
        }).then(() => {
            // Check that the lock time has not expired yet
            return rdb.getActiveRelease(json.aid);
        }).then(time => {
            if (time === null) {
                return Promise.reject('Your selection time period has expired (NULL). Please re-request a lift');
            } else {
                let now = new Date().getTime();
                let expired = (now - Number(time)) > config.ACTIVE_LOCK_TIME;

                if (time === null || expired) {
                    // Delete this user from the waitingList
                    mdb.mdbModules.waitingList.removePassenger(passengerSid).then(() => {
                        return rdb.removePassengerFromWaitingList(passengerSid);
                    }).then(() => {
                        logger.logInfo('Removed passenger from waiting lift after selection time period has expired');
                    }).catch(err => {
                        logger.logWarn('Could not remove passenger from waiting lift after selection time period has expired
' + err);
                    });

                    return Promise.reject('Your selection time period has expired. Please re-request a lift');
                } else {
                    // Get the driver's sid for the selected active
                    return mdb.mdbModules.active.getDriver(json.aid);
                }
            }
        }).then(driverSid => {
            // Add a message to the driver's inbox telling them that they have to accept or reject
            // TODO: This should potentially be two separate calls linked and not use a Promise.all
            let calls = [
                mdb.mdbModules.inbox.addMessage(driverSid, `Please accept or reject giving a lift to user ${passengerSid}`, consts.MESSAGE_TYPES.CONFIRM),
                mdb.mdbModules.waitingList.addDriverToPassenger(passengerSid, json.aid)
            ];

            return Promise.all(calls);
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully sent the request to the indicated driver'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /user/:id/lift/pickup Pickup passenger
     * @apiName PickupPassenger
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiDescription Pickup a passenger. The passenger is moved from the pickup list to the dropoff list in
     * the trip that they are in.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the passenger has been picked up
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Successfully set the user as being picked up"
     *        }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/lift/pickup', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            // Get the lift where the user still has to be picked up
            return mdb.mdbModules.active.getLiftPickup(sid);
        }).then(item => {
            if (item.length > 0) {
                // Pickup the user - User is moved to the passenger list
                return mdb.mdbModules.active.pickupLift(sid, item[0].aid);
            } else {
                return Promise.reject('The user is not currently in a pickup list on any trip');
            }
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully set the user as being picked up'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /user/:id/lift/complete Dropoff passenger
     * @apiName CompleteLift
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiDescription Dropoff a passenger. The lift is completed and the passenger is removed from the
     * trip that they were on. Note that a passenger has to have been picked up before this request will work.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the lift has been completed
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Successfully marked the user's trip as completed"
     *        }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/lift/complete', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            // Get the lift where the user still has to be picked up
            return mdb.mdbModules.active.getLiftPassenger(sid);
        }).then(item => {
            if (item.length > 0) {
                // The user is removed from the active document entirely - In this case the passengers list
                return mdb.mdbModules.active.removeLift(sid, item[0].aid)
            } else {
                return Promise.reject('The user is not currently in a passenger list on any trip');
            }
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully marked the user\'s trip as completed'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/trip/history Get trip history
     * @apiName GetTripHistory
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {Number} [num=3] The number of items to get.
     *
     * @apiDescription Get a list of trips that the user has set in the past.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the trip history items were retrieved.
     * @apiSuccess {Object} data
     * @apiSuccess {Object} [trips] List of trip settings.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Successfully retrieved user's trip history",
     *            "data": {
     *                "trips": [
     *                    {
     *                        "destination": {
     *                            "lat": -33.934272,
     *                            "lon": 18.862424
     *                        },
     *                        "origin": {
     *                            "lat": -33.927337,
     *                            "lon": 18.863926
     *                        },
     *                        "startTime": "2017-09-23T06:30:00.107Z",
     *                        "endTime": "2017-09-23T09:50:00.107Z",
     *                        "timestamp": 1506178500116
     *                    }
     *                ]
     *            }
     *        }
     *
     * @apiSuccessExample Success-Response 204:
     *     HTTP/1.1 204 No Content
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/trip/history', function (req, res) {

        let sid = req.params.id;
        let num = 3 || req.params.num;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.user.getTripHistory(sid, num);
        }).then(items => {
            if (items.length === 0) {
                res.status(204).json({success: true, message: 'User does not have previous set trips'});
            } else {
                res.status(200).json({
                    success: true,
                    message: 'Successfully retrieved user\'s trip history',
                    data: {trips: items}
                });
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/lift/history Get lift history
     * @apiName GetLiftHistory
     * @apiGroup Active
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {Number} [num=3] The number of items to get.
     *
     * @apiDescription Get a list of lifts that the user has request in the past.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the lift history items were retrieved.
     * @apiSuccess {Object} data
     * @apiSuccess {Object} [lifts] List of lift settings.
     *
     * @apiSuccessExample Success-Response 200:
     *     HTTP/1.1 200 OK
     *        {
     *            "success": true,
     *            "message": "Successfully retrieved user's lift history",
     *            "data": {
     *                "lifts": [
     *                    {
     *                        "destination": {
     *                            "lat": -33.933589,
     *                            "lon": 18.86227
     *                        },
     *                        "origin": {
     *                            "lat": -33.930313,
     *                            "lon": 18.863897
     *                        },
     *                        "startTime": "2017-09-23T06:45:17.567Z",
     *                        "endTime": "2017-09-23T09:25:17.567Z",
     *                        "timestamp": 1506178593114
     *                    }
     *                ]
     *            }
     *        }
     *
     * @apiSuccessExample Success-Response 204:
     *     HTTP/1.1 204 No Content
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/lift/history', function (req, res) {

        let sid = req.params.id;
        let num = 3 || req.params.num;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.user.getLiftHistory(sid, num);
        }).then(items => {
            if (items.length === 0) {
                res.status(204).json({success: true, message: 'User does not have previous set lifts'});
            } else {
                res.status(200).json({
                    success: true,
                    message: 'Successfully retrieved user\'s lift history',
                    data: {lifts: items}
                });
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });
};
