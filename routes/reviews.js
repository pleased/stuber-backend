/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let checks = require('../util/checks.js');

module.exports = function(router) {

    /**
     * @api {post} /review/ Submit a review
     * @apiName AddReview
     * @apiGroup Review
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} reviewer Reviewer's unique Stuber ID.
     * @apiParam {String} reviewed Reviewed's unique Stuber ID.
     * @apiParam {String} message The review message the reviewer gives.
     * @apiParam {Number} rating The rating the reviewer gives.
     *
     * @apiExample {json} Example
     *   {
     *      "reviewer": "H1xR9l_7SZ",
     *      "reviewed": "SJRcx_mSZ",
     *
     *      "message": "This person is really a bad person. Don't ever take a trip with them. BEWARE x|",
     *      "rating": 0
     *   }
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the review successfully submitted.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully submitted review"
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/review', function (req, res) {
        // TODO: Give some karma to the reviewing user
        // TODO: Update the reviewed user's rating
        // Check that reviewer exists

        let json = req.body;

        // Check that valid settings were provided in the request
        let pass = checks.validSubmitReviewSettings(json);
        if (pass !== true) {
            return res.status(400).json({success: false, message: pass});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(json.reviewer);
        }, 'Reviewer does not exist').then(() => {
            return checks.isWrapper(() => {
                return rdb.sidExists(json.reviewed);
            }, 'Reviewed does not exist');
        }).then(() => {
            return mdb.mdbModules.review.addReview(json);
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully submitted review'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /review/:id?num=# Get user reviews
     * @apiName GetUserReviews
     * @apiGroup Review
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {Number} [num=5] The number of reviews to retrieve. De.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the reviews was successfully retrieved.
     * @apiSuccess {Object} data
     * @apiSuccess {[Objects]} data.reviews List of reviews retrieved.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *        "success": true,
     *        "message": "Reviews retrieved",
     *        "data": [
     *            {
     *               "reviewer": "H1xR9l_7SZ",
     *               "reviewed": "SJRcx_mSZ",
     *               "rating": 0,
     *               "message": "This person is really a bad person. Don't ever take a trip with them. BEWARE x|"
     *            }
     *        ]
     *     }
     *
     * @apiError UserHasNoReviews The User has no reviews
     * @apiErrorExample User Has No Reviews:
     *     HTTP/1.1 204 NoContent
     *     {
     *       "success": True,
     *       "message": "No reviews present for the user"
     *     }
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/review/:id', function (req, res) {

        let sid = req.params.id;
        let num = 5 || req.params.num;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
           return mdb.mdbModules.review.getReviews(num, sid);
        }).then(items => {
            if (items.length === 0) {
                res.status(204).json({success: true, message: 'No reviews present for the user'});
            } else {
                res.status(200).json({success: true, message: 'Reviews retrieved', data: items});
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });
};
