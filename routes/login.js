/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let ta = require('../tokenAuth.js');

let rdb = require('../database/rdb.js');
let mdb = require('../database/mdb.js');

let checks = require('../util/checks.js');

module.exports = function(router) {

    /**
     * @api {post} /login Login a user
     * @apiName LoginUser
     * @apiGroup Login
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user was successfully logged in.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.sid Unique Stuber ID for the user.
     * @apiSuccess {String} data.token JWT token user has to use to access preceding requests.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully logged in user",
     *       "data": {
     *          "sid": "ByOFSUmBZ"
     *          "token": "thisisajwttoken56780"
     *       }
     *     }
     *
     * @apiUse UserNotFoundError
     */
    router.post('/login', function(req, res) {

        // Check if user Google ID exists in the database
        let gid = req.body.gid;

        if (gid === undefined) {
            return res.status(400).json({success: false, message: 'No Google ID was supplied'});
        }

        checks.isWrapper(() => {
            return rdb.gidExists(gid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.user.loginUser(gid);
        }).then(sid => {
            let token = ta.generateToken(gid);

            res.status(200).json({
                success: true,
                message: 'Successfully logged in user',
                data: {sid: sid, token: token}
            });
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });
};
