/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
module.exports = function(router){

    // Should always be last two routes (at bottom of file)
    router.all('/', function(req, res) {
        res.status(200).json("Welcome to the Stuber REST API");
    });

    //The 404 Route
    router.all('*', function(req, res){

        res.status(404).json('Error 404: Could not find command');
    });
};
