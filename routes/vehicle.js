/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let io = require('../io.js');
let remove = require('../controller/remove.js');
let logger = require('../logger.js');

let checks = require('../util/checks.js');

let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let multer  = require('multer');

const upload = multer({
    dest: io.PATH_VEHICLES
});

module.exports = function(router) {

    /**
     * @api {post} /vehicle/ Register a vehicle
     * @apiName AddVehicle
     * @apiGroup Vehicle
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} color The color of the vehicle.
     * @apiParam {String} make The make of the vehicle.
     * @apiParam {String} model The model of the vehicle.
     * @apiParam {String} regNumber The registration number of the vehicle.
     * @apiParam {String} sid Owner's unique Stuber ID.
     * @apiParam {Number} seats The number of seats the vehicle has.
     *
     * @apiExample {json} Example
     *    {
     *      "color": "blue",
     *      "make": "Porche",
     *      "model": "GTR3",
     *      "regNumber": "123456789",
     *      "sid": "SJRcx_mSZ",
     *      "seats": 2
     *    }
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the review successfully submitted.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.vid Unique vehicle ID.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully added the vehicle to the system",
     *       "data": {
     *          "vid": "HkZJseO7BW"
     *       }
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/vehicle', function (req, res) {

        let sid = req.body.sid;
        let Vid;

        // Add a new vehicle
        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return checks.isNotWrapper(() => {
                return mdb.mdbModules.util.checkVehicleExists({regNumber: req.body.regNumber});
            }, 'A vehicle with the same registration number already exists');
        }).then(() => {
            return mdb.mdbModules.vehicle.addVehicle(sid, req.body);
        }).then(vid => {
            Vid = vid;
            return rdb.addVid(vid);
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully added the vehicle to the system', data: {vid: Vid}});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /vehicle/:id Retrieve a vehicle
     * @apiName GetVehicle
     * @apiGroup Vehicle
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id Vehicle's unique ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the review successfully submitted.
     * @apiSuccess {Object} data
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully added the vehicle to the system",
     *       "data": {
     *          "color": "blue",
     *          "make": "Porche",
     *          "model": "GTR3",
     *          "regNumber": "123456789",
     *          "seats": 2,
     *          "vid": "SylJjldXHZ",
     *          "picture": "",
     *          "owners": [
     *              "SJRcx_mSZ"
     *          ]
     *       }
     *     }
     *
     * @apiUse VehicleNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/vehicle/:id', function (req, res) {

        let vid = req.params.id;

        checks.isWrapper(() => {
            return rdb.vidExists(vid);
        }, 'Vehicle does not exist').then(() => {
            return mdb.mdbModules.vehicle.getVehicle(vid);
        }).then(item => {
            res.json({success: true, message: 'Successfully retrieved vehicle', data: item});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    router.delete('/vehicle/:id', function (req, res) {

        let vid = req.params.id;

        checks.isWrapper(() => {
            return rdb.vidExists(vid);
        }, 'Vehicle does not exist').then(() => {
            return remove.deleteVehicle(vid);
        }).then(() => {
            res.json({success: true, message: 'Vehicle has been successfully deleted'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /vehicle/:id/image Add a vehicle image
     * @apiName PostVehicleImage
     * @apiGroup Vehicle
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id Vehicle's unique ID.
     * @apiParam {String} file Image file.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the image was successfully uploaded.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully uploaded vehicle image"
     *     }
     * @apiUse VehicleNotFoundError
     * @apiUser NoImageFileSupplied
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/vehicle/:id/image', upload.single('file'), function (req, res) {

        // Upload an image for a vehicle
        let vid = req.params.id;

        if (!req.file) {
            logger.logInfo('No image was supplied in request to upload image for vehicle ' + vid);
            return res.status(400).json({success: false, message: 'No image was supplied'});
        }

        checks.isWrapper(() => {
            return rdb.vidExists(vid);
        }, 'Vehicle does not exist').then(() => {
            io.saveVechileImage(req, function (err, mess) {
                if (err) {
                    return Promise.reject(err);
                } else {
                    // Update image field in the vehicle database
                    return mdb.mdbModules.vehicle.addVehicleImage(vid, mess);
                }
            });
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully uploaded vehicle image'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /vehicle/:id/image Get a vehicle image
     * @apiName GetVehicleImage
     * @apiGroup Vehicle
     *
     * @apiParam {String} id Vehicle's unique ID.
     *
     * @apiSuccess {FileStream} file Filestream containing the image data.
     * @apiError VehicleHasNoImage The Vehicle has no image
     * @apiErrorExample Vehicle Has No Image:
     *     HTTP/1.1 204 NoContent
     *     {
     *       "success": True,
     *       "message": "The vehicle does not have an image"
     *     }
     *
     * @apiUse VehicleNotFoundError
     *
     * @apiError ImageDoesNotExist The image file does not exist
     * @apiErrorExample Image File Doesn't Exist:
     *     HTTP/1.1 400 BadRequest
     *     {
     *       "success": False,
     *       "message": "The image file does not exist"
     *     }
     */
    router.get('/vehicle/:id/image', function (req, res) {

        let vid = req.params.id;

        checks.isWrapper(() => {
            return rdb.vidExists(vid);
        }, 'Vehicle does not exist').then(() => {
            return mdb.mdbModules.vehicle.getVehicleImage(vid);
        }).then(picture => {
            if (picture === '') {
                // TODO: Maybe replace this with some default image later
                res.status(204).json({success: true, message: 'The vehicle does have an image'})
            }
            // Check that the file exists
            else if (io.fileExistsVehicle(picture)) {
                res.status(200).sendFile(io.PATH_VEHICLES + picture);
            } else {
                return Promise.reject('The image file does not exist');
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });
};
