/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let ta = require('../tokenAuth.js');
let remove = require('../controller/remove.js');
let io = require('../io.js');

let checks = require('../util/checks.js');

let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let logger = require('../logger.js');

let multer = require('multer');
let upload = multer({
    dest: io.PATH_USERS
});

module.exports = function (router) {


    /**
     * @api {post} /user Register a user
     * @apiName RegisterUser
     * @apiGroup User
     *
     * @apiParam {String} gid User's Google ID.
     * @apiParam {String} fullname User's name and surname
     * @apiParam {Number} age User's age.
     * @apiParam {String} [email] User's email address.
     * @apiParam {[String]} [likes] Interests that the user may have.
     * @apiParam {[String]} [dislikes] Non-Interests that the user may have.
     * @apiParam {Object} [address]
     * @apiParam {String} [address.home] User's home address
     * @apiParam {String} [address.work] User's work address
     *
     * @apiExample {json} Example
     *   {
     *     "fullname": "George Green",
     *     "gid": "googleId",
     *     "age": "34",
     *     "email": "geogreen@gmail.com
     *   }
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user was registered successfully.
     * @apiSuccess {Object} data
     * @apiSuccess {String} data.sid Unique Stuber ID for the user.
     * @apiSuccess {String} data.token JWT token user has to use to access preceding requests.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "User successfully registered",
     *       "data": {
     *          "sid": "ByOFSUmBZ"
     *          "token": "thisisajwttoken56780"
     *       }
     *     }
     *
     * @apiErrorExample ID already in use:
     *     HTTP/1.1 400 BadRequest
     *     {
     *       "success": False,
     *       "message": "Google ID already in use"
     *     }
     */
    router.post('/user', function (req, res) {

        let gid = req.body.gid;
        let Sid;

        // Check that the google id is not already in use
        checks.isNotWrapper(() => {
            return rdb.gidExists(gid);
        }, 'Google ID already in use').then(() => {
            // Add a new user to the system
            return mdb.mdbModules.user.addUser(req.body);
        }).then(sid => {
            Sid = sid;
            return rdb.addUser(gid, sid);
        }).then(() => {
            let token = ta.generateToken(gid);
            res.status(200).json({
                success: true,
                message: 'User successfully registered',
                data: {sid: Sid, token: token}
            });
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id Retrieve a user
     * @apiName RetrieveUser
     * @apiGroup User
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user's data was successfully retrieved.
     * @apiSuccess {Object} data
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "User successfully registered",
     *       "data": {
     *          "fullname": "George Green",
     *          "age": 34,
     *          "sid": "ByOFSUmBZ",
     *          "picture": "",
     *          "tripHistory": [
     *              {
     *                  "startTime": "2017-07-12T06:30:48.912Z",
     *                  "endTime": "2017-07-12T09:50:48.912Z",
     *                  "origin": {
     *                      "lat": -33.927337,
     *                      "lon": 18.863926
     *                  },
     *                  "destination": {
     *                    "lat": -33.934272,
     *                     "lon": 18.862424
     *                  }
     *              }
     *          ],
     *          "liftHistory": [],
     *          "isDriver": true,
     *          "karma": 0,
     *          "rating": 0,
     *          "dislikes": [],
     *          "likes": [],
     *          "address": {
     *             "work": "",
     *             "home": ""
     *          },
     *          "email": ""
     *       }
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return Promise.all([
                mdb.mdbModules.user.getUser(sid),
                mdb.mdbModules.review.getUserRating(sid)
            ]);
        }).then(item => {
            item[0].rating = item[1];

            res.status(200).json({success: true, message: 'Successfully retrieved user data', data: item[0]});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {put} /user/:id Update user
     * @apiName UpdateUser
     * @apiGroup User
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiDescription Update the details of a user.
     *
     * @apiExample {json} Example
     *    {
     *      "age": 58,
     *      "email": "jeffman@gmail.com",
     *      "address_home": "52 Standard Street",
     *      "likes": ["cake", "The number 55", "jeff"],
     *      "address_work": "Santa Maria Shore Building 77"
     *    }
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user's rating was retrieved
     * @apiSuccess {Object} data The updated user object
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully retrieved user's rating",
     *       "data": {
     *          $updateUserObject
     *       }
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.put('/user/:id', function (req, res) {

        let sid = req.params.id;
        let json = req.body;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            const validFields = ['fullname', 'age', 'email', 'likes', 'dislikes', 'address_home', 'address_work'];

            for (let field in json) {
                // Do the mappings to the correct address field if required
                if (field === 'address_home' || field === 'address_work') {
                    if (!json.address) {
                        json.address = {}
                    }

                    json.address[field.split('_')[1]] = json[field];
                    delete json[field];
                }

                if (!validFields.includes(field)) {
                    delete json[field];
                }
            }

            return mdb.mdbModules.user.updateUser(sid, json);
        }).then(item => {
            // Return the updated user object
            res.status(200).json({success: true, message: 'Successfully retrieved user data', data: item});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {delete} /user/:id Delete a user
     * @apiName DeleteUser
     * @apiGroup User
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user's data was successfully deleted.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "User successfully registered"
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.delete('/user/:id', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return remove.deleteUser(sid);
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully deleted user'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {put} /user/:id/driver Update user's driver status
     * @apiName UpdateUserStatus
     * @apiGroup User
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {Boolean} isDriver Value to set user's driver status to.
     *
     * @apiExample {json} Example
     *   {
     *      "isDriver": True
     *   }

     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user's driver status was successfully updated.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully updated user's driver status"
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.put('/user/:id/driver', function (req, res) {

        let sid = req.params.id;

        if (req.body.isDriver === undefined) {
            return res.status(400).json({success: false, message: 'Invalid values supplied for request'});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.user.setAsDriver(sid, req.body.isDriver);
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully updated user\'s driver status'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/vehicles Retrieve user vehicles
     * @apiName GetUserVehicles
     * @apiGroup User
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user's vehicles have successfully been retrieved.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully retrieved user's vehicles",
     *       "data": {
     *          $vehicle1,
     *          $vehicle2
     *       }
     *
     * @apiError UserHasNoVehicles The User has no registered vehicles
     * @apiErrorExample User Has No Vehicles:
     *     HTTP/1.1 204 NoContent
     *     {
     *       "success": True,
     *       "message": "User does not have any vehicles registered"
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/vehicles', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.vehicle.getVehiclesOfOwner(sid);
        }).then(vehicles => {
            if (vehicles.length === 0) {
                res.status(204).json({success: true, message: 'User does not have any vehicles registered'});

            } else {
                res.status(200).json({success: true, message: 'Successfully retrieved user\'s vehicles', data: vehicles});
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/image Get a user profile image
     * @apiName GetUserImage
     * @apiGroup User
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {FileStream} file Filestream containing the image data.
     * @apiError UserHasNoImage The User has no image
     * @apiErrorExample User Has No Image:
     *     HTTP/1.1 204 NoContent
     *     {
     *       "success": True,
     *       "message": "The user does not have an image"
     *     }
     *
     * @apiUse UserNotFoundError
     *
     * @apiError ImageDoesNotExist The image file does not exist
     * @apiErrorExample Image File Doesn't Exist:
     *     HTTP/1.1 400 BadRequest
     *     {
     *       "success": False,
     *       "message": "The image file does not exist"
     *     }
     */
    router.get('/user/:id/image', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.user.getUserImage(sid);
        }).then(picture => {
            if (picture === '') {
                // TODO: Maybe replace this with some default image later
                res.status(204).json({success: true, message: 'The user does not have an image'})
            }
            // Check that the file exists
            else if (io.fileExistsUser(picture)) {
                res.status(200).sendFile(io.PATH_USERS + picture);
            } else {
                return Promise.reject('The image file does not exist');
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {post} /user/:id/image Add a user profile image
     * @apiName PostUserImage
     * @apiGroup User
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     * @apiParam {String} file Image file.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the image was successfully uploaded.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully uploaded user image"
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUser NoImageFileSupplied
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.post('/user/:id/image', upload.single('file'), function (req, res) {

        let sid = req.params.id;

        if (!req.file) {
            logger.logInfo('No image was supplied in request to upload image for user ' + sid);
            return res.status(400).json({success: false, message: 'No image was supplied'});
        }

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            io.saveUserImage(req, function (err, mess) {
                if (err) {
                    return Promise.reject(err);
                } else {
                    // Update image field in the user database
                    return mdb.mdbModules.user.addUserImage(sid, mess);
                }
            });
        }).then(() => {
            res.status(200).json({success: true, message: 'Successfully uploaded user image'});
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/inbox Get user inbox messages
     * @apiName GetUserInbox
     * @apiGroup User
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user's inbox was successfully retrieved.
     * @apiSuccess {Object} data
     * @apiSuccess {[Objects]} data.messages List of messages retrieved.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully retrieved user's inbox messages",
     *       "data": [
     *          {
     *              "text": "You will be notified when you have been assigned to a trip",
     *              "timestamp": 1499853002527
     *          }
     *       ]
     *     }
     *
     * @apiError UserHasNoMessages The User has no messages in their inbox
     * @apiErrorExample User Has No Image:
     *     HTTP/1.1 204 NoContent
     *     {
     *       "success": True,
     *       "message": "No messages currently in the inbox"
     *     }
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/inbox', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.inbox.getMessages(sid);
        }).then(items => {
            if (items.length > 0) {
                res.status(200).json({
                    success: true,
                    message: 'Successfully retrieved user\'s inbox messages',
                    data: items
                });
            } else {
                res.status(204).json({success: true, message: 'No messages currently in the inbox'});
            }
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });

    /**
     * @api {get} /user/:id/rating Get user rating
     * @apiName UserRating
     * @apiGroup User
     * @apiHeader {String} x-access-token JWT token used for authentication
     *
     * @apiParam {String} id User's unique Stuber ID.
     *
     * @apiDescription Get the rating of a user. This rating is the average score of the reviews submitted of the user.
     * If the user has no raviews agains them they will get a default score of 0.
     *
     * @apiSuccess {Boolean} success Indicates that the request was a success.
     * @apiSuccess {String} message Message saying that the user's rating was retrieved
     * @apiSuccess {Object} data
     * @apiSuccess {Number} rating The rating of the user
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "success": True,
     *       "message": "Successfully retrieved user's rating",
     *       "data": {
     *          "rating": 3.75
     *       }
     *     }
     *
     * @apiUse UserNotFoundError
     * @apiUse AuthTokenInvalid
     * @apiUse AuthTokenExpired
     * @apiUse AuthTokenNotSupplied
     */
    router.get('/user/:id/rating', function (req, res) {

        let sid = req.params.id;

        checks.isWrapper(() => {
            return rdb.sidExists(sid);
        }, 'User does not exist').then(() => {
            return mdb.mdbModules.review.getUserRating(sid);
        }).then(rating => {
            res.status(200).json({
                success: true,
                message: 'Successfully retrieved user\'s rating',
                data: {
                    rating: rating
                }
            });
        }).catch(err => {
            res.status(400).json({success: false, message: err});
        });
    });
};
