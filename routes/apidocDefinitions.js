/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// Contains a bunch of definitions for apidoc
/**
 * @apiDefine UserNotFoundError
 *
 * @apiError UserNotFound The id of the User was not found.
 *
 * @apiErrorExample User Not Found:
 *     HTTP/1.1 400 BadRequest
 *     {
 *       "success": False,
 *       "message": "Use does not exist"
 *     }
 */

/**
 * @apiDefine VehicleNotFoundError
 *
 * @apiError VehicleNotFound The id of the Vehicle was not found.
 *
 * @apiErrorExample Vehicle Not Found:
 *     HTTP/1.1 400 BadRequest
 *     {
 *       "success": False,
 *       "message": "Vehicle does not exist"
 *     }
 */

 /**
 * @apiDefine NoImageFileSupplied
 *
 * @apiError NoImageFileSupplied No image file was supplied
 *
 * @apiErrorExample Vehicle Not Found:
 *     HTTP/1.1 400 BadRequest
 *     {
 *       "success": False,
 *       "message": "No image was supplied"
 *     }
 */

 /**
 * @apiDefine AuthTokenInvalid
 *
 * @apiError InvalidTokenSupplied JWT Token Invalid
 *
 * @apiErrorExample JWT Invalid:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "success": False,
 *       "message": "Authentication failed - Invalid token"
 *     }
 */

/**
 * @apiDefine AuthTokenExpired
 *
 * @apiError ExpiredTokenSupplied JWT Token Expired
 *
 * @apiErrorExample JWT Expired:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "success": False,
 *       "message": "Authentication failed - Token has expired"
 *     }
 */

/**
 * @apiDefine AuthTokenNotSupplied
 *
 * @apiError NoTokenSupplied JWT Token Supplied
 *
 * @apiErrorExample No JWT Supplied:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "success": False,
 *       "message": "Authentication failed - Request has no token in headers"
 *     }
 */

/**
 * @apiDefine UserNotOnTrip
 *
 * @apiError UserNotOnTrip User is not on a trip
 *
 * @apiErrorExample User Not On Trip:
 *     HTTP/1.1 400 BadRequest
 *     {
 *       "success": False,
 *       "message": "The user is not currently on a trip"
 *     }
 */

/**
 * @apiDefine ActiveDoesNotExist
 *
 * @apiError ActiveDoesNotExist The active specified does not exist
 *
 * @apiErrorExample Active Does Not Exist
 *     HTTP/1.1 400 BadRequest
 *     {
 *       "success": False,
 *       "message": "Trip does not exist"
 *     }
 */

/**
 * @apiDefine PassengersPresent
 *
 * @apiError PassengersPresent There are still passengers on the trip
 *
 * @apiErrorExample Passengers Present
 *     HTTP/1.1 400 BadRequest
 *     {
 *       "success": False,
 *       "message": "Trip could not be set as complete as it still has pickups/passengers in it"
 *     }
 */

/**
 * @apiDefine ScheduledLiftDoesNotExist
 *
 * @apiError ScheduledLiftDoesNotExist The scheduled lift specified does not exist
 *
 * @apiErrorExample Scheduled Lift Does Not Exist
 *     HTTP/1.1 400 BadRequest
 *     {
 *       "success": False,
 *       "message": "Lift with specified ID does not exist"
 *     }
 */

/**
 * @apiDefine ScheduledTripDoesNotExist
 *
 * @apiError ScheduledTripDoesNotExist The scheduled trip specified does not exist
 *
 * @apiErrorExample Scheduled Trip Does Not Exist
 *     HTTP/1.1 400 BadRequest
 *     {
 *       "success": False,
 *       "message": "Trip with specified ID does not exist"
 *     }
 */
