/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../logger.js');
let compare = require('../util/compare.js');

let consts = require('../consts.js');

module.exports = {

    /**
     * Solve the travelling salesman problem but with a few adaptions.
     * Before a coordinate can be added to the route it must first be checked if the passenger associated with it has
     * been picked up by the driver before actually dropping the passenger off as their desired destination. Pickup and dropoff times are also considered.
     * If the driver can reach a pickup location in a given time frame, +- x minutes, then it is OK. If the driver can reach the dropoff
     * any amount of time early or x minutes after it is OK.
     *
     * This iteration is only used for scheduled trips as no additional checks have to be made in order
     * to calculate the correct route.
     *
     * NOTE: This assumes that no two origins/destinations will have the exact same gps coordinates. If it does then sorry some dude
     * @param oto A list of all possible combinations between pairs of coordinates with values of distance
     * @param passengers The passengers that are on the trip and the new one that wants to be added
     * @param driver The driver of the trip
     */
    solve: function (oto, driver, passengers) {

        return new Promise((resolve, reject) => {
            logger.logInfo(`(salesman.solve) Received a set of size ${oto.length}`);

            try {
                let endSet;
                let route = [];
                let pickedUp = [];
                let totDist = driver.traveled || 0.0;

                let date = new Date(driver.startTime);
                let currentTime = ((date.getUTCHours() * 60) + date.getUTCMinutes()) * 60; // In seconds

                // Remove any lanes that lead to the driver's origin. They are useless as the driver's origin is the first spot
                oto = oto.filter(item => !(driver.origin.lon === item.destination.lon && driver.origin.lat === item.destination.lat));
                // Remove any lanes that start at the driver's destination. They are useless as the driver's destination is the final spot
                oto = oto.filter(item => !(driver.destination.lon === item.origin.lon && driver.destination.lat === item.origin.lat));

                // Get the driver's destination lanes. These only have to be used at the end
                endSet = oto.filter(item => item.destination.lon === driver.destination.lon && item.destination.lat === driver.destination.lat);
                // Remove the driver's destination lanes. These only have to be used at the end
                oto = oto.filter(item => !(item.destination.lon === driver.destination.lon && item.destination.lat === driver.destination.lat));

                // The drivers origin
                route.push({lon: driver.origin.lon, lat: driver.origin.lat});

                // Now iterate over the remaining ones and find the closet one each time
                while (oto.length > 0) {
                    let startParts = route[route.length - 1];

                    // Get all lanes that have source of the last entry in the route currently
                    let possibles = oto.filter(item => startParts.lon === item.origin.lon && startParts.lat === item.origin.lat);

                    if (possibles.length === 0) {
                        logger.logWarn('(salesman.solve) The list of possibles should not have been empty');
                        return reject('The list of possibles should not have been empty');
                    } else {
                        // Sort the possibles by travel distance
                        possibles = possibles.sort((a, b) => a.distance - b.distance);

                        // Remove all lanes with the same origin as that of the last entry in the route array
                        oto = oto.filter(item => !(startParts.lon === item.origin.lon && startParts.lat === item.origin.lat));

                        // Find the first possibility where the passenger has been picked up if the coordinates is a destination
                        let decision = null;
                        for (let i = 0; i < possibles.length; i = i + 1) {
                            // Get only passenger where the possibility is the origin
                            let possibleDestination = possibles[i].destination;
                            let origins = passengers.filter(item => possibleDestination.lon === item.origin.lon && possibleDestination.lat === item.origin.lat);

                            // If origin has length greater than 0 than this possibility is the
                            if (origins.length > 0) {
                                currentTime += possibles[0].duration;

                                let date = new Date(origins[0].startTime);
                                if (compare.timeCompareBetween(currentTime, date.getUTCHours(), date.getUTCMinutes(), consts.TIME_FRAMES.PICKUP_BEFORE, consts.TIME_FRAMES.PICKUP_AFTER)) {
                                    pickedUp.push(origins[0].sid);
                                    decision = possibles[i];

                                    break;
                                } else {
                                    // Signifies time constraints could not be met
                                    logger.logInfo('(salesman.solve) Could not pickup passenger at their desired time');
                                    return resolve(-420);
                                }
                            }
                            // If there are no origins then this means that the possibility is the destination of some passenger
                            // Check that the passenger has been picked up before adding this to the route
                            else {
                                // Get only passenger where the possibility is the destination
                                let destinations = passengers.filter(item => possibleDestination.lon === item.destination.lon && possibleDestination.lat === item.destination.lat);
                                if (!destinations || destinations.length === 0) {
                                    logger.logWarn('(salesman.solve) This should not have happened');
                                    return Promise.reject('This should not have happened');
                                } else if (pickedUp.includes(destinations[0].sid)) {
                                    currentTime += possibles[0].duration;

                                    let date = new Date(destinations[0].endTime);
                                    if (compare.timeCompareBetween2(currentTime, date.getUTCHours(), date.getUTCMinutes(), consts.TIME_FRAMES.DROPOFF_AFTER)) {
                                        decision = possibles[i];

                                        break;
                                    } else {
                                        // Signifies time constraints could not be met
                                        logger.logInfo('(salesman.solve) Could not dropoff passenger at their desired time');
                                        return resolve(-420);
                                    }
                                }
                            }
                        }

                        if (decision === null) {
                            logger.logWarn('(salesman.solve) This should also not have happened. Decision is null');
                            return reject('This should also not have happened');
                        }

                        // Now add the next point in the route
                        totDist += decision.distance;
                        route.push(decision.destination);

                        // Remove all lanes with the same destination as that of the last entry in the route array
                        oto = oto.filter(item => !(decision.destination.lon === item.destination.lon && decision.destination.lat === item.destination.lat));
                    }
                }

                // Now add the final lane to the driver's destination
                let startParts = route[route.length - 1];
                let end = endSet.filter(item => startParts.lon === item.origin.lon && startParts.lat === item.origin.lat)[0];
                date = new Date(driver.endTime);

                totDist += end.distance;
                route.push(end.destination);

                currentTime += end.duration;
                if (!compare.timeCompareBetween2(currentTime, date.getUTCHours(), date.getUTCMinutes(), consts.TIME_FRAMES.DROPOFF_AFTER)) {
                    // Signifies time constraints could not be met
                    logger.logInfo('(salesman.solve) Driver could not reach their destination on time');
                    return resolve(-420);
                }

                logger.logDebug('(salesman.solve) The final route is ');
                logger.logDebug('salesman.solve) ' + JSON.stringify(route, null, 2));
                logger.logDebug('salesman.solve) With a total distance of ' + totDist + 'm');

                // Resolve the route and the distance of the route
                resolve([route, totDist]);
            } catch (e) {
                console.log(e);
                reject('The salesman decided to skip some work. Time to fire him');
            }
        });
    },

    /**
     * Solve the travelling salesman problem but with a few adaptions.
     * Before a coordinate can be added to the route it must first be checked if the passenger associated with it has
     * been picked up by the driver before actually dropping the passenger off as their desired destination. Pickup and dropoff times are also considered.
     * If the driver can reach a pickup location in a given time frame, +- x minutes, then it is OK. If the driver can reach the dropoff
     * any amount of time early or x minutes after it is OK.
     *
     * This iteration is only used for currently active trips as a few additional checks have to be made to
     * calculate the correct route.
     *
     * NOTE: This assumes that no two origins/destinations will have the exact same gps coordinates. If it does then sorry some dude
     * @param oto A list of all possible combinations between pairs of coordinates with values of distance
     * @param passengers The passengers that are on the trip and the new one that wants to be added
     * @param driver The driver of the trip
     */
    solveRealtime: function (oto, driver, passengers) {

        return new Promise((resolve, reject) => {
            logger.logInfo(`(salesman.solveRealtime) Received a set of size ${oto.length}`);

            try {
                let endSet;
                let route = [];
                let pickedUp = [];
                let totDist = driver.traveled;

                let date = new Date(driver.startTime);
                let currentTime = ((date.getUTCHours() * 60) + date.getUTCMinutes()) * 60; // In seconds

                // Remove any lanes that lead to the driver's origin. They are useless as the driver's origin is the first spot
                oto = oto.filter(item => !(driver.currLoc.lon === item.destination.lon && driver.currLoc.lat === item.destination.lat));
                // Remove any lanes that start at the driver's destination. They are useless as the driver's destination is the final spot
                oto = oto.filter(item => !(driver.destination.lon === item.origin.lon && driver.destination.lat === item.origin.lat));

                // Get the driver's destination lanes. These only have to be used at the end
                endSet = oto.filter(item => item.destination.lon === driver.destination.lon && item.destination.lat === driver.destination.lat);
                // Remove the driver's destination lanes. These only have to be used at the end
                oto = oto.filter(item => !(item.destination.lon === driver.destination.lon && item.destination.lat === driver.destination.lat));

                // The drivers origin
                route.push({lon: driver.currLoc.lon, lat: driver.currLoc.lat});

                // Now iterate over the remaining ones and find the closet one each time
                while (oto.length > 0) {
                    // Get all lanes that has origin of the destination of the last entry in the route
                    let startParts = route[route.length - 1];
                    let possibles = oto.filter(item => startParts.lon === item.origin.lon && startParts.lat === item.origin.lat);

                    if (possibles.length === 0) {
                        logger.logWarn('(salesman.solveRealtime) The list of possibles should not have been empty');
                        return reject('The list of possibles should not have been empty');
                    } else {
                        // Sort the possibles by travel distance
                        possibles = possibles.sort((a, b) => a.distance - b.distance);

                        // Find the first possibility where the passenger has been picked up if the coordinates is a destination
                        let decision = null;
                        let skip = false;
                        for (let i = 0; i < possibles.length; i = i + 1) {
                            // Get only passengers where the possibility is the origin
                            let possibleDestination = possibles[i].destination;
                            let origins = passengers.filter(item => possibleDestination.lon === item.origin.lon && possibleDestination.lat === item.origin.lat);

                            // If origin has length greater than 0 than this possibility is the
                            if (origins.length > 0) {
                                // Check if the passenger has not already been added to the pickedUp list
                                // This will only be the case for a passenger if they have been added due to
                                // have been picked up in real life
                                if (pickedUp.includes(origins[0].sid)) {
                                    // Since the passenger has been marked as being picked up continue to the next iteration
                                    continue;
                                }

                                // Check if this passenger has not already been picked up by the driver.
                                // This check is not if the passenger has been picked up in the algorithm but rather
                                // if the passenger has been picked up in real life.
                                let realLifePickup = driver.passengers.filter(item => item.sid === origins[0].sid);
                                if (realLifePickup.length === 1) {
                                    // They have been picked up in real life they should simply be skipped.
                                    // They distance has already been taken into account in initial driver.traveled value

                                    logger.logInfo('(salesman.solveRealtime) Passenger has already been picked up in real life... Skipping');

                                    // The passenger is still added to pickedUp list so algorithm can continue if it
                                    // finds the passenger's dropoff
                                    skip = true;

                                    pickedUp.push(origins[0].sid);
                                    decision = possibles[i];

                                    // Remove this passenger from the oto list
                                    oto = oto.filter(item => !(origins[0].origin.lon === item.destination.lon && origins[0].origin.lat === item.destination.lat));

                                    break;
                                } else {
                                    // Since they have not been picked up in real life they have to be taken into account
                                    // in current iteration of solve()
                                    currentTime += possibles[0].duration;

                                    let date = new Date(origins[0].startTime);
                                    if (compare.timeCompareBetween(currentTime, date.getUTCHours(), date.getUTCMinutes(), consts.TIME_FRAMES.PICKUP_BEFORE, consts.TIME_FRAMES.PICKUP_AFTER)) {
                                        pickedUp.push(origins[0].sid);
                                        decision = possibles[i];

                                        break;
                                    } else {
                                        // Signifies time constraints could not be met
                                        logger.logInfo('(salesman.solveRealtime) Could not pickup passenger at their desired time');
                                        return resolve(-420);
                                    }
                                }
                            }
                            // If there are no origins then this means that the possibility is the destination of some passenger
                            // Check that the passenger has been picked up before adding this to the route
                            else {
                                // Get only passenger where the possibility is the destination
                                let destinations = passengers.filter(item => possibleDestination.lon === item.destination.lon && possibleDestination.lat === item.destination.lat);
                                if (!destinations || destinations.length === 0) {
                                    logger.logWarn('(salesman.solveRealtime) This should not have happened');
                                    return Promise.reject('This should not have happened');
                                } else if (pickedUp.includes(destinations[0].sid)) {
                                    currentTime += possibles[0].duration;

                                    let date = new Date(destinations[0].endTime);
                                    if (compare.timeCompareBetween2(currentTime, date.getUTCHours(), date.getUTCMinutes(), consts.TIME_FRAMES.DROPOFF_AFTER)) {
                                        decision = possibles[i];

                                        break;
                                    } else {
                                        // Signifies time constraints could not be met
                                        logger.logInfo('(salesman.solveRealtime) Could not dropoff passenger at their desired time');
                                        return resolve(-420);
                                    }
                                }
                            }
                        }

                        if (decision === null) {
                            logger.logWarn('(salesman.solveRealtime) This should also not have happened. Decision is null');
                            return reject('This should also not have happened');
                        }

                        if (skip === false) {
                            // First remove all lanes with the same origin as that of the last entry in the route array
                            oto = oto.filter(item => !(startParts.lon === item.origin.lon && startParts.lat === item.origin.lat));

                            // Now add the next point in the route
                            totDist += decision.distance;
                            route.push(decision.destination);

                            // Remove all lanes with the same destination as that of the decision
                            oto = oto.filter(item => !(decision.destination.lon === item.destination.lon && decision.destination.lat === item.destination.lat));
                        }
                    }
                }

                // Now add the final lane to the driver's destination
                let startParts = route[route.length - 1];
                let end = endSet.filter(item => startParts.lon === item.origin.lon && startParts.lat === item.origin.lat)[0];
                date = new Date(driver.endTime);

                totDist += end.distance;
                route.push(end.destination);

                currentTime += end.duration;
                if (!compare.timeCompareBetween2(currentTime, date.getUTCHours(), date.getUTCMinutes(), consts.TIME_FRAMES.DROPOFF_AFTER)) {
                    // Signifies time constraints could not be met
                    logger.logInfo('(salesman.solveRealtime) Driver could not reach their destination on time');
                    return resolve(-420);
                }

                logger.logDebug('(salesman.solveRealtime) The final route is ');
                logger.logDebug('salesman.solveRealtime) ' + JSON.stringify(route, null, 2));
                logger.logDebug('salesman.solveRealtime) With a total distance of ' + totDist + 'm');

                // Resolve the route and the distance of the route
                resolve([route, totDist]);
            } catch (e) {
                console.log(e);
                reject('The salesman decided to skip some work. Time to fire him');
            }
        });
    }
};
