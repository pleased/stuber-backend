/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

let consts = require('../../consts.js');

let mdb = require('../../database/mdb.js');
let rdb = require('../../database/rdb.js');

let script = require('../../controller/script.js');

module.exports = {

    /**
     *
     * @param job
     * @param done
     */
    scheduleSingleLift: function (job, done) {

        let DATA = job.data;

        // First check if the lift is not marked for deletion
        rdb.isIdMarkedForDeletion(DATA.lid).then(is => {
            if (is === true) {
                // Since the lift has been marked for deletion this job can be cancelled
                done('This job is being failed as the lift has been marked for deletion or is already deleted');
            } else {
                logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                script.getFittingScheduledTrip(DATA.lid).then(data => {
                    let trip = data[0];
                    let route = data[1];

                    // Add the passenger to the trip lifts
                    return mdb.mdbModules.schedule.addLiftToTripSingleDate(trip.tid, route, DATA.lid).then(() => {
                        // Set the driver of the lift
                        return mdb.mdbModules.schedule.setLiftSingleDateDriver(trip.tid, DATA.lid).then(() => {
                            return mdb.mdbModules.inbox.addMessage(DATA.sid, `You were scheduled to trip ${trip.tid} for lift ${DATA.lid}`, consts.MESSAGE_TYPES.SCHEDULE).catch(() => {
                                return Promise.resolve();
                            });
                        });
                    });
                }).then(() => {
                    logger.logInfo('All stuff completed');
                    done();
                }).catch(err => {
                    logger.logWarn('(kueTasks.scheduleSingleLift) Something went wrong with script
' + err);
                    done(err);
                });
            }
        }).catch(err => {
            done(err);
        });
    },

    /**
     *
     * @param job
     * @param done
     */
    scheduleSingleTrip: function (job, done) {

        let DATA = job.data;

        // First check if the trip is not marked for deletion
        rdb.isIdMarkedForDeletion(DATA.tid).then(is => {
            if (is === true) {
                // Since the trip has been marked for deletion this job can be cancelled
                done('This job is being failed as the trip has been marked for deletion or is already deleted');
            } else {
                logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                script.getFittingScheduledLifts(DATA.tid).then(data => {
                    // data = [newPassengers, route]
                    let lifts = data[0];
                    let lids = lifts.map(item => item.lid);
                    let sids = lifts.map(item => item.sid);
                    let route = data[1];

                    return mdb.mdbModules.schedule.addMultiLiftsToTripSingleDate(DATA.tid, route, lids).then(() => {
                        let calls = [];

                        // Notify all the passengers that have been assigned
                        for (let i = 0; i < lifts.length; i = i + 1) {
                            calls.push(mdb.mdbModules.schedule.setLiftSingleDateDriver(DATA.tid, lids[i]).then(() => {
                                return mdb.mdbModules.inbox.addMessage(sids[i], `You were scheduled to ${DATA.tid} for ${lids[i]}`, consts.MESSAGE_TYPES.SCHEDULE).catch(() => {
                                    return Promise.resolve();
                                });
                            }));
                        }

                        return Promise.all(calls);
                    });
                }).then(() => {
                    logger.logInfo('All stuff completed');
                    done();
                }).catch(err => {
                    logger.logWarn('(kueTasks.scheduleSingleTrip) Something went wrong with script
' + err);
                    done(err);
                });
            }
        }).catch(err => {
            done(err);
        });
    }
};
