/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

let mdb = require('../../database/mdb.js');
let rdb = require('../../database/rdb.js');

module.exports = function (kue) {

    return {
        /**
         *
         * @param job
         * @param done
         */
        removeScheduledTrip: function (job, done) {

            let DATA = job.data;

            logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

            rdb.removeTid(DATA.tid).then(() => {
                return mdb.mdbModules.schedule.removeScheduledTrip(DATA.tid);
            }).then(item => {

                // Check if weekly or singleDate trip. This will dictate which kue job will be created
                if (item.singleDate === true) {
                    // Go through all the lifts that were assigned and create jobs to go and remove the driver from them
                    let calls = [];
                    for (let i = 0; i < item.singleDatePassengers.length; i = i + 1) {
                        calls.push(kue.addRemoveLiftDriverSingleDateJob({
                            tid: DATA.tid, // Driver
                            lid: item.singleDatePassengers[i] // Lift
                        }));
                    }

                    return Promise.all(calls);
                } else if (item.weekRepeat === true) {

                    let lifts = [];

                    // Get all the unique lifts from all the days
                    for (let i = 0; i < item.passengers.monday.length; i = i + 1) lifts.includes(item.passengers.monday[i]) === false ? lifts.push(item.passengers.monday[i]) : logger.logDebug(item.passengers.monday[i]);
                    for (let i = 0; i < item.passengers.tuesday.length; i = i + 1) lifts.includes(item.passengers.tuesday[i]) === false ? lifts.push(item.passengers.tuesday[i]) : logger.logDebug(item.passengers.tuesday[i]);
                    for (let i = 0; i < item.passengers.wednesday.length; i = i + 1) lifts.includes(item.passengers.wednesday[i]) === false ? lifts.push(item.passengers.wednesday[i]) : logger.logDebug(item.passengers.wednesday[i]);
                    for (let i = 0; i < item.passengers.thursday.length; i = i + 1) lifts.includes(item.passengers.thursday[i]) === false ? lifts.push(item.passengers.thursday[i]) : logger.logDebug(item.passengers.thursday[i]);
                    for (let i = 0; i < item.passengers.friday.length; i = i + 1) lifts.includes(item.passengers.friday[i]) === false ? lifts.push(item.passengers.friday[i]) : logger.logDebug(item.passengers.friday[i]);
                    for (let i = 0; i < item.passengers.saturday.length; i = i + 1) lifts.includes(item.passengers.saturday[i]) === false ? lifts.push(item.passengers.saturday[i]) : logger.logDebug(item.passengers.saturday[i]);
                    for (let i = 0; i < item.passengers.sunday.length; i = i + 1) lifts.includes(item.passengers.sunday[i]) === false ? lifts.push(item.passengers.sunday[i]) : logger.logDebug(item.passengers.sunday[i]);

                    // Go through all the lifts that were assigned and create jobs to go and remove the driver from them
                    let calls = [];
                    for (let i = 0; i < lifts.length; i = i + 1) {
                        calls.push(kue.addRemoveLiftDriverWeeklyJob({
                            tid: DATA.tid, // Driver
                            lid: lifts[i] // Lift
                        }));
                    }
                } else {
                    // Should never get here
                    logger.logFatal('What the hell do we have here');
                    done('What the hell do we have here');
                }
            }).then(() => {
                done();
            }).catch(err => {
                done(err);
            });
        },

        /**
         *
         * @param job
         * @param done
         */
        removeLiftDriverSingleDate: function (job, done) {

            let DATA = job.data;
            let sid;

            // First check if the lift is not marked for deletion
            rdb.isIdMarkedForDeletion(DATA.lid).then(is => {
                if (is === true) {
                    // Since the lift has been marked for deletion this job can be cancelled
                    return done('This job is being failed as the lift has been marked for deletion or is already deleted');
                } else {
                    logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                    // Clear the driver of the lift
                    return mdb.mdbModules.schedule.getScheduledLift(DATA.lid);
                }
            }).then(item => {
                if (item.length !== 0) {
                    sid = item[0].sid;

                    return mdb.mdbModules.schedule.setLiftSingleDateDriver('', DATA.lid);
                } else {
                    return done('Could not get scheduled lift with LID ' + DATA.lid);
                }
            }).then(() => {
                // Schedule a job so that this lift might get a driver again
                return kue.addLiftSingleDateJob({
                    lid: DATA.lid,
                    sid: sid
                });
            }).then(() => {
                done();
            }).catch(err => {
                done(err);
            });
        },

        /**
         *
         * @param job
         * @param done
         */
        removeLiftDriverWeekRepeat: function (job, done) {

            let DATA = job.data;
            let sid;

            // First check if the lift is not marked for deletion
            rdb.isIdMarkedForDeletion(DATA.lid).then(is => {
                if (is === true) {
                    // Since the lift has been marked for deletion this job can be cancelled
                    return done('This job is being failed as the lift has been marked for deletion or is already deleted');
                } else {
                    logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                    // Clear the driver of the lift
                    return mdb.mdbModules.schedule.getScheduledLift(DATA.lid);
                }
            }).then(item => {
                if (item.length === 0) {
                    return done('Could not get scheduled lift with LID ' + DATA.lid);
                } else {
                    item = item[0];
                    sid = item.sid;
                }

                // Check which days have to be cleared
                let calls = [];

                item.driver.monday === DATA.tid ? calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver('', DATA.lid, 'monday')) : logger.logDebug('Monday is OK');
                item.driver.tuesday === DATA.tid ? calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver('', DATA.lid, 'tuesday')) : logger.logDebug('Tuesday is OK');
                item.driver.wednesday === DATA.tid ? calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver('', DATA.lid, 'wednesday')) : logger.logDebug('Wednesday is OK');
                item.driver.thursday === DATA.tid ? calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver('', DATA.lid, 'thursday')) : logger.logDebug('Thursday is OK');
                item.driver.friday === DATA.tid ? calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver('', DATA.lid, 'friday')) : logger.logDebug('Friday is OK');
                item.driver.saturday === DATA.tid ? calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver('', DATA.lid, 'saturday')) : logger.logDebug('Saturday is OK');
                item.driver.sunday === DATA.tid ? calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver('', DATA.lid, 'sunday')) : logger.logDebug('Sunday is OK');

                return Promise.all(calls);
            }).then(() => {
                // Schedule a job so that this lift might get a driver again
                return kue.addLiftWeeklyJob({
                    lid: DATA.lid,
                    sid: sid
                });
            }).then(() => {
                done();
            }).catch(err => {
                done(err);
            });
        },

        /**
         *
         * @param job
         * @param done
         */
        removeScheduledLift: function (job, done) {

            let DATA = job.data;

            logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

            rdb.removeLid(DATA.lid).then(() => {
                return mdb.mdbModules.schedule.removeScheduledLift(DATA.lid);
            }).then(item => {

                // Check if weekly or singleDate trip. This will dictate which kue job will be created
                if (item.singleDate === true) {
                    if (item.singleDateDriver === '') {
                        return done();
                    } else {
                        return kue.addRemoveTripPassengerSingleDateJob({
                            tid: item.singleDateDriver,
                            lid: DATA.lid
                        });
                    }
                } else if (item.weekRepeat === true) {

                    let calls = [];
                    let days = Object.keys(item.driver);

                    // TODO: Improve this to one call or something. Each one of these jobs recalculates the route which is stupid
                    // Check which days of which drivers have to be cleared
                    for (let i = 0; i < days.length; i = i + 1) {
                        if (item.driver[days[i]] !== '') {
                            calls.push(kue.addRemoveTripPassengerWeeklyJob({
                                tid: item.driver[days[i]],
                                lid: DATA.lid,
                                day: days[i]
                            }));
                        } else {
                            logger.logDebug(days[i] + ' is OK');
                        }
                    }

                    return Promise.all(calls);
                } else {
                    // Should never get here
                    logger.logFatal('What the hell do we have here');
                    done('What the hell do we have here');
                }
            }).then(() => {
                done();
            }).catch(err => {
                done(err);
            });
        },

        /**
         *
         * @param job
         * @param done
         */
        removeTripPassengerSingleDate: function (job, done) {

            let DATA = job.data;

            // First check if the trip is not marked for deletion
            rdb.isIdMarkedForDeletion(DATA.tid).then(is => {
                if (is === true) {
                    // Since the trip has been marked for deletion this job can be cancelled
                    return done('This job is being failed as the trip has been marked for deletion or is already deleted');
                } else {
                    logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                    // Remove the passenger from the trip
                    return mdb.mdbModules.schedule.removeSingleDatePassenger(DATA.tid, DATA.lid);
                }
            }).then(() => {
                // Schedule a job that will recalculate route with only present passengers
                return kue.addRefreshTripSingleDateJob({
                    tid: DATA.tid
                });
            }).then(() => {
                done();
            }).catch(err => {
                done(err);
            });
        },

        /**
         *
         * @param job
         * @param done
         */
        removeTripPassengerWeekly: function (job, done) {

            let DATA = job.data;

            // First check if the trip is not marked for deletion
            rdb.isIdMarkedForDeletion(DATA.tid).then(is => {
                if (is === true) {
                    // Since the trip has been marked for deletion this job can be cancelled
                    return done('This job is being failed as the trip has been marked for deletion or is already deleted');
                } else {
                    logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                    // Remove the passenger from the trip
                    return mdb.mdbModules.schedule.removeWeeklyPassenger(DATA.tid, DATA.lid, DATA.day);
                }
            }).then(() => {
                // Schedule a job that will recalculate route with only present passengers
                return kue.addRefreshTripWeeklyJob({
                    tid: DATA.tid
                });
            }).then(() => {
                done();
            }).catch(err => {
                done(err);
            });
        }
    }
};
