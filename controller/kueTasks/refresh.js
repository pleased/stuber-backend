/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

let mdb = require('../../database/mdb.js');
let rdb = require('../../database/rdb.js');

let script = require('../../controller/script.js');

module.exports = function (kue) {

    return {

        /**
         *
         * @param job
         * @param done
         */
        refreshSingleDateTrip: function (job, done) {

            let DATA = job.data;

            // First check if the trip is not marked for deletion
            rdb.isIdMarkedForDeletion(DATA.tid).then(is => {
                if (is === true) {
                    // Since the trip has been marked for deletion this job can be cancelled
                    return done('This job is being failed as the trip has been marked for deletion or is already deleted');
                } else {
                    logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                    // Recalculate the route
                    return script.recalculateScheduledTrip(DATA.tid);
                }
            }).then(route => {
                return mdb.mdbModules.schedule.setScheduledTripSingleDateRoute(DATA.tid, route);
            }).then(() => {
                return mdb.mdbModules.schedule.getScheduledTrip(DATA.tid);
            }).then(item => {

                if (item.length === 0) {
                    return done('Could not get scheduled trip with TID ' + DATA.tid);
                } else {
                    // Schedule a job so that this trip might get new passengers
                    return kue.addTripSingleDateJob({
                        tid: DATA.tid,
                        sid: item[0].sid
                    });
                }
            }).then(() => {
                done();
            }).catch(err => {
                done(err);
            });
        },

        /**
         *
         * @param job
         * @param done
         */
        refreshWeeklyTrip: function (job, done) {

            let DATA = job.data;

            // First check if the trip is not marked for deletion
            rdb.isIdMarkedForDeletion(DATA.tid).then(is => {
                if (is === true) {
                    // Since the trip has been marked for deletion this job can be cancelled
                    return done('This job is being failed as the trip has been marked for deletion or is already deleted');
                } else {
                    logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                    // Recalculate the route
                    return script.recalculateScheduledTrip(DATA.tid);
                }
            }).then(route => {
                return mdb.mdbModules.schedule.setScheduledTripWeeklyRoute(DATA.tid, route);
            }).then(() => {
                return mdb.mdbModules.schedule.getScheduledTrip(DATA.tid);
            }).then(item => {

                if (item.length === 0) {
                    return done('Could not get scheduled trip with TID ' + DATA.tid);
                } else {
                    // Schedule a job so that this trip might get new passengers
                    return kue.addTripWeeklyJob({
                        tid: DATA.tid,
                        sid: item[0].sid
                    });
                }
            }).then(() => {
                done();
            }).catch(err => {
                done(err);
            });
        }
    }
};
