/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

let consts = require('../../consts.js');

let mdb = require('../../database/mdb.js');
let rdb = require('../../database/rdb.js');

let script = require('../../controller/script.js');

module.exports = {

    /**
     *
     * @param job
     * @param done
     */
    scheduleWeeklyLift: function (job, done) {

        let DATA = job.data;

        // First check if the lift is not marked for deletion
        rdb.isIdMarkedForDeletion(DATA.lid).then(is => {
            if (is === true) {
                // Since the lift has been marked for deletion this job can be cancelled
                done('This job is being failed as the lift has been marked for deletion or is already deleted');
            } else {
                logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                script.getFittingScheduledTrip(DATA.lid).then(data => {
                    // Save the weekRepeat Settings
                    let calls = [];
                    let days = Object.keys(data);

                    // Save the settings for the lift
                    for (let d = 0; d < days.length; d = d + 1) {
                        let day = days[d];
                        let dayValue = data[day];

                        if (dayValue !== undefined) {
                            calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver(dayValue.driver, DATA.lid, day).then(() => {
                                return mdb.mdbModules.inbox.addMessage(DATA.sid, `You were scheduled to trip ${dayValue.tid} for lift ${DATA.lid} on ${day}`).catch(() => {
                                    return Promise.resolve();
                                });
                            }));

                            // TODO: Maybe check about making this like the schedule trip one. There multiple values are updated with a single query to the database
                            calls.push(mdb.mdbModules.schedule.addLiftToTripWeekRepeat(dayValue.driver, dayValue.route, DATA.lid, day));
                            // TODO: Consider adding this I guess. If the above TODO is done then this will be possible we will have the sids
                            // calls.push(mdb.addMessage(sid, `You were scheduled to trip ${data[i]} for lift ${lid} on ${Object.keys(data)[i]}`));
                        }
                    }

                    return Promise.all(calls);
                }).then(() => {
                    logger.logInfo('All stuff completed');
                    done();
                }).catch(err => {
                    logger.logWarn('(kueTasks.scheduleWeeklyLift) Something went wrong with script
' + err);
                    done(err);
                });
            }
        }).catch(err => {
            done(err);
        });
    },

    /**
     *
     * @param job
     * @param done
     */
    scheduleWeeklyTrip: function (job, done) {

        let DATA = job.data;

        // First check if the trip is not marked for deletion
        rdb.isIdMarkedForDeletion(DATA.tid).then(is => {
            if (is === true) {
                // Since the trip has been marked for deletion this job can be cancelled
                done('This job is being failed as the trip has been marked for deletion or is already deleted');
            } else {
                logger.logInfo('Starting job #' + job.id + ' of type ' + job.type + ' and with priority ' + job._priority);

                script.getFittingScheduledLifts(DATA.tid).then(data => {
                    // Add the tip to the lifts and send them messages
                    let calls = [];
                    for (let i = 0; i < Object.keys(data).length; i = i + 1) {
                        let day = Object.keys(data)[i];
                        for (let j = 0; j < data[day].passengers.length; j = j + 1) {
                            // Add the trip to the lift and notify the passenger
                            calls.push(mdb.mdbModules.schedule.setLiftWeekRepeatDriver(DATA.tid, data[day].passengers[j].lid, day).then(() => {
                                return mdb.mdbModules.inbox.addMessage(data[day].passengers[j].sid, `You were scheduled to ${DATA.tid} for ${data[day].passengers[j].lid} on day ${day}`, consts.MESSAGE_TYPES.SCHEDULE).catch(() => {
                                    return Promise.resolve();
                                });
                            }));

                            // Add the lift to the trip and notify the driver
                            calls.push(mdb.mdbModules.schedule.addLiftToTripWeekRepeat(DATA.tid, data[day].route, data[day].passengers[j].lid, day).then(() => {
                                return mdb.mdbModules.inbox.addMessage(DATA.sid, `${data[day].passengers[j].lid} were scheduled to your ${DATA.tid} on day ${day}`, consts.MESSAGE_TYPES.SCHEDULE).catch(() => {
                                    return Promise.resolve();
                                });
                            }));
                        }
                    }

                    return Promise.all(calls);
                }).then(() => {
                    logger.logInfo('All stuff completed');
                    done();
                }).catch(err => {
                    logger.logWarn('(kueTasks.scheduleWeeklyTrip) Something went wrong with script
' + err);
                    done(err);
                });
            }
        }).catch(err => {
            done(err);
        });
    }
};
