/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let Promise = require('promise');
let async = require('async');

let config = require('../config.js');

let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let logger = require('../logger.js');
let adapter = require('../adapters/osm.js');
let salesman = require('./salesman.js');

const THRESHOLD = 0.2;

const dateEqual = function (d1, d2) {
    return d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();
};

function calcCrow(lat1, lon1, lat2, lon2) {
    
    /**
     * Converts numeric degrees to radians
     * @param value
     * @returns {number}
     */
    let toRad = function (value) {
        return value * Math.PI / 180.0;
    };

    let R = 6371; // km
    let dLat = toRad(lat2 - lat1);
    let dLon = toRad(lon2 - lon1);
    lat1 = toRad(lat1);
    lat2 = toRad(lat2);

    let a = Math.sin(dLat / 2.0) * Math.sin(dLat / 2.0) + Math.sin(dLon / 2.0) * Math.sin(dLon / 2.0) * Math.cos(lat1) * Math.cos(lat2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return R * c;
}

/**
 * Search for a trip that is on single day and meets the lift's requirements
 * @param lift
 * @param trips
 * @param passengers
 */
function singleDateSearchTrips(lift, trips, passengers) {

    const myFilter = function (lift, trip) {
        return trip.singleDate && dateEqual(lift.date, trip.date);
    };

    let drivers = trips.filter(trip => myFilter(lift, trip));
    drivers = drivers.filter(item => item.singleDateOpenSeats > 0);

    if (drivers.length === 0) {
        return Promise.reject('No drivers to assign');
    } else {
        for (let i = 0; i < drivers.length; i = i + 1) {
            drivers[i].passengers = passengers[drivers[i].sid];
            drivers[i].route = drivers[i].singleDateRoute;
        }

        // Resolves a list of drivers, so no need to flatten like in 'singleDateSearchLifts'
        return minifyDriverSet(lift, drivers).then(results => {
            // Remove the drivers that failed
            let filtered = results.filter(item => item !== false);
            return Promise.resolve(filtered);
        });
    }
}

/**
 * Check for lifts that match the settings of a trip
 * @param lifts
 * @param driver
 * @param passengers
 * @returns {*|Promise}
 */
function singleDateSearchLifts(lifts, driver, passengers) {

    const myFilter = function (lift, trip) {
        return lift.singleDateDriver === '' && lift.singleDate && dateEqual(lift.date, trip.date);
    };

    lifts = lifts.filter(lift => myFilter(lift, driver));

    if (lifts.length === 0) {
        return Promise.reject();
    } else {

        // Sort the passengers according to the singleDate. Older schedules will be given higher priority
        lifts = lifts.sort((a, b) => a.singleDate - b.singleDate);

        return new Promise((resolve, reject) => {

            let hasAtLeastSingleAssign = false;
            let bestRoute = driver.singleDateRoute;
            let newPassengers = [];

            driver.passengers = passengers;
            driver.route = driver.singleDateRoute;

            // Check each passenger one at a time.
            async.eachOfSeries(lifts, (lift, i, callback) => {

                if (driver.singleDateOpenSeats > 0) {
                    minifyDriverSet(lift, [driver]).then(result => {
                        if (result[0] !== false) {
                            hasAtLeastSingleAssign = true;

                            // result = [[driver, route]]. It's nested because minifyDriverSet returns from a Promise.all
                            bestRoute = result[0][1];

                            newPassengers.push(lift);
                            driver.passengers.push(lift);
                            driver.route = bestRoute;
                            driver.singleDateOpenSeats -= 1;
                        }

                        callback();
                    }).catch(err => {
                        logger.logInfo('(scripts.singleDateSearchLifts) Error was thrown but we are ignoring it.
' + err);
                        callback();
                    });
                } else {
                    // Since there is no more space open just skip this lift
                    console.log('Skipping lift as no open seats left');
                    callback();
                }
            }, err => {
                if (err) {
                    logger.logInfo(`(script.singleDateSearchLifts) No assignments could be made due to an error
${err}`);
                    reject(err);
                } else if (hasAtLeastSingleAssign) {
                    // At least a single assignment was made so that is good
                    resolve([newPassengers, bestRoute]);
                } else {
                    // No assignments were made so the allocation was unsuccessful
                    logger.logInfo('(script.singleDateSearchLifts) No assignments were made so the allocation was unsuccessful');
                    reject('No matches to make assignment');
                }
            });
        });
    }
}

/**
 * Search for a trip that is on a weekly schedule that meets the lift's requirements
 * @param lift
 * @param trips
 * @param passengers
 * @returns {*|Promise}
 */
function weeklySearchTrips(lift, trips, passengers) {

    const myFilter = function (lift, trip) {

        // Check that the lift and trip has `days` that can be compared
        if (lift.days === undefined || lift.days === null || trip.days === undefined || trip.days === null) {
            return false;
        } else {
            // NOTE: undefined === true is false
            let monday = lift.days.monday === true && trip.days.monday === true;
            let tuesday = lift.days.tuesday === true && trip.days.tuesday === true;
            let wednesday = lift.days.wednesday === true && trip.days.wednesday === true;
            let thursday = lift.days.thursday === true && trip.days.thursday === true;
            let friday = lift.days.friday === true && trip.days.friday === true;
            let saturday = lift.days.saturday === true && trip.days.saturday === true;
            let sunday = lift.days.sunday === true && trip.days.sunday === true;

            return monday || tuesday || wednesday || thursday || friday || saturday || sunday;
        }
    };

    const daysEqual = function (d1, d2) {
        return d1 === true && d2 === true;
    };

    let drivers = trips.filter(trip => myFilter(lift, trip));

    if (drivers.length === 0) {
        logger.logInfo('(script.weeklySearchTrips) No assignments could be made due to there being no drivers after filter');
        return Promise.reject('No drivers to assign');
    } else {
        return new Promise((resolve, reject) => {
            let sizedUp = {};

            let hasAtLeastSingleAssign = false;
            let newAssigned = {};
            let assigned = {
                monday: lift.driver.monday,
                tuesday: lift.driver.tuesday,
                wednesday: lift.driver.wednesday,
                thursday: lift.driver.thursday,
                friday: lift.driver.friday,
                saturday: lift.driver.saturday,
                sunday: lift.driver.sunday
            };

            // Sort the trips by the amount of days that match the days of the lift
            for (let i in drivers) {
                let count = 0;

                sizedUp[drivers[i].tid] = {
                    // The number of days that match
                    count: count,
                    // The days that match as a boolean array. Monday[0] -> Sunday[6]
                    days: [
                        daysEqual(drivers[i].days.monday, lift.days.monday),
                        daysEqual(drivers[i].days.tuesday, lift.days.tuesday),
                        daysEqual(drivers[i].days.wednesday, lift.days.wednesday),
                        daysEqual(drivers[i].days.thursday, lift.days.thursday),
                        daysEqual(drivers[i].days.friday, lift.days.friday),
                        daysEqual(drivers[i].days.saturday, lift.days.saturday),
                        daysEqual(drivers[i].days.sunday, lift.days.sunday)
                    ]
                };

                count = sizedUp[drivers[i].tid].days[0] ? count + 1 : count;
                count = sizedUp[drivers[i].tid].days[1] ? count + 1 : count;
                count = sizedUp[drivers[i].tid].days[2] ? count + 1 : count;
                count = sizedUp[drivers[i].tid].days[3] ? count + 1 : count;
                count = sizedUp[drivers[i].tid].days[4] ? count + 1 : count;
                count = sizedUp[drivers[i].tid].days[5] ? count + 1 : count;
                count = sizedUp[drivers[i].tid].days[6] ? count + 1 : count;

                logger.logDebug(`${count} days matched between the trip ${drivers[i].tid} and lift`);

                // Set the new processed count
                sizedUp[drivers[i].tid].count = count;
            }

            drivers.sort((a, b) => sizedUp[b.tid].count - sizedUp[a.tid].count);

            // Go through all drivers and check which ones match
            async.eachOfSeries(drivers, (driver, i, callback) => {

                // For each driver compare each day.
                let calls = [];
                let days = Object.keys(assigned);
                for (let k = 0; k < days.length; k = k + 1) {
                    // If the days equal and the driver has an open seat then see if fit
                    if (sizedUp[driver.tid].days[k] === true && assigned[days[k]] === '' && driver.weekRepeatOpenSeats[days[k]] > 0) {
                        // Get only the passengers that have a lift on this particular day
                        let dayPassengers = passengers[driver.sid].filter(item => driver.passengers[days[k]].includes(item.lid));

                        // Copy the driver object. As multiple versions of the driver object will be used in following query
                        let localDriver = JSON.parse(JSON.stringify(driver));
                        localDriver.passengers = dayPassengers;
                        localDriver.route = driver.weekRepeatRoute[days[k]];

                        calls.push(minifyDriverSet(lift, [localDriver]));
                    // Otherwise just resolve false as if a fail
                    } else {
                        // Resolve as [false] to fit with format of minifyDriverSet fail resolve
                        calls.push(Promise.resolve([false]));
                    }
                }

                Promise.all(calls).then(result => {
                    let days = Object.keys(assigned);

                    // NOTE: result will always have length 7
                    for (let j = 0; j < result.length; j = j + 1) {
                        // A false indicates that the match won't work
                        if (result[j][0] !== false && assigned[days[j]] === '' && sizedUp[driver.tid].days[j] === true) {
                            hasAtLeastSingleAssign = true;

                            assigned[days[j]] = driver.tid;
                            newAssigned[days[j]] = {
                                driver: driver.tid,
                                route: result[j][0][1]
                            };
                        }
                    }

                    callback();
                }).catch(err => {
                    return callback(err);
                });
            }, err => {
                if (err) {
                    logger.logInfo(`(script.weeklySearchTrips) No assignments could be made due to an error
${err}`);
                    reject(err);
                } else if (hasAtLeastSingleAssign) {
                    // At least a single assignment was made so that is good
                    resolve(newAssigned);
                } else {
                    // No assignments were made so the allocation was unsuccessful
                    logger.logInfo('(script.weeklySearchTrips) No assignments were made so the allocation was unsuccessful');
                    reject('No matches to make assignment');
                }
            });
        });
    }
}

/**
 *
 * @param lifts
 * @param trip
 * @param passengers
 * @returns {*|Promise}
 */
function weeklySearchLifts(lifts, trip, passengers) {

    const myFilter = function (lift, trip) {

        // Check that the lift and trip has `days` that can be compared
        if (lift.days === undefined || lift.days === null || trip.days === undefined || trip.days === null) {
            return false;
        } else {
            // NOTE: undefined === true is false
            let monday = lift.days.monday === true && trip.days.monday === true && lift.driver.monday === '';
            let tuesday = lift.days.tuesday === true && trip.days.tuesday === true && lift.driver.tuesday === '';
            let wednesday = lift.days.wednesday === true && trip.days.wednesday === true && lift.driver.wednesday === '';
            let thursday = lift.days.thursday === true && trip.days.thursday === true && lift.driver.thursday === '';
            let friday = lift.days.friday === true && trip.days.friday === true && lift.driver.friday === '';
            let saturday = lift.days.saturday === true && trip.days.saturday === true && lift.driver.saturday === '';
            let sunday = lift.days.sunday === true && trip.days.sunday === true && lift.driver.sunday === '';

            return monday || tuesday || wednesday || thursday || friday || saturday || sunday;
        }
    };

    const daysEqual = function (d1, d2) {
        return d1 === true && d2 === true;
    };

    // Will only give lifts that do not already have all days assigned
    lifts = lifts.filter(lift => myFilter(lift, trip));

    if (lifts.length === 0) {
        logger.logInfo('(script.weeklySearchLifts) No assignments could be made due to there being no passengers after filter');
        return Promise.reject('No passengers to assign');
    } else {
        return new Promise((resolve, reject) => {
            // What new lifts are assigned to the trip on specific days
            let hasAtLeastSingleAssign = false;
            let newAssigned = {};
            let sizedUp = {};
            let assigns = {
                monday: trip.passengers.monday,
                tuesday: trip.passengers.tuesday,
                wednesday: trip.passengers.wednesday,
                thursday: trip.passengers.thursday,
                friday: trip.passengers.friday,
                saturday: trip.passengers.saturday,
                sunday: trip.passengers.sunday
            };

            for (let i = 0; i < lifts.length; i = i + 1) {
                let count = 0;

                sizedUp[lifts[i].lid] = {
                    count : 0,
                    days : [
                        daysEqual(trip.days.monday, lifts[i].days.monday) && lifts[i].driver.monday === '',
                        daysEqual(trip.days.tuesday, lifts[i].days.tuesday) && lifts[i].driver.tuesday === '',
                        daysEqual(trip.days.wednesday, lifts[i].days.wednesday) && lifts[i].driver.wednesday === '',
                        daysEqual(trip.days.thursday, lifts[i].days.thursday) && lifts[i].driver.thursday === '',
                        daysEqual(trip.days.friday, lifts[i].days.friday) && lifts[i].driver.friday === '',
                        daysEqual(trip.days.saturday, lifts[i].days.saturday) && lifts[i].driver.saturday === '',
                        daysEqual(trip.days.sunday, lifts[i].days.sunday) && lifts[i].driver.sunday === ''
                    ]
                };


                // Get the number of days that the trip and lift match
                count = sizedUp[lifts[i].lid].days[0] ? count + 1 : count;
                count = sizedUp[lifts[i].lid].days[1] ? count + 1 : count;
                count = sizedUp[lifts[i].lid].days[2] ? count + 1 : count;
                count = sizedUp[lifts[i].lid].days[3] ? count + 1 : count;
                count = sizedUp[lifts[i].lid].days[4] ? count + 1 : count;
                count = sizedUp[lifts[i].lid].days[5] ? count + 1 : count;
                count = sizedUp[lifts[i].lid].days[6] ? count + 1 : count;

                logger.logDebug(`${count} days matched between the trip and lift ${lifts[i].lid}`);

                // Set the new processed count
                sizedUp[lifts[i].lid].count = count;
            }

            // Sort the passengers by the number of days that match with the trip
            lifts.sort((a, b) => sizedUp[b.lid].count - sizedUp[a.lid].count);

            async.eachOfSeries(lifts, (passenger, i, callback) => {
                // For each driver compare each day.
                let calls = [];
                let days = Object.keys(assigns);
                for (let k = 0; k < days.length; k = k + 1) {
                    // If the days equal and there are open seats then see if fit
                    if (sizedUp[passenger.lid].days[k] === true && trip.weekRepeatOpenSeats[days[k]] > 0) {
                        let dayPassengers = assigns[days[k]].length === 0 ? [] : passengers.filter(item => assigns[days[k]].includes(item.lid));

                        // Copy the driver object. As multiple versions of the driver object will be used in following query
                        let localDriver = JSON.parse(JSON.stringify(trip));
                        localDriver.passengers = dayPassengers;
                        if (typeof newAssigned[days[k]] !== 'undefined') {
                            localDriver.route = newAssigned[days[k]].route;
                        } else {
                            localDriver.route = trip.weekRepeatRoute[days[k]];
                        }

                        calls.push(minifyDriverSet(passenger, [localDriver]));
                    }
                    // Otherwise just resolve false as if a fail
                    else {
                        // Resolve as [false] to fit with format of minifyDriverSet fail resolve
                        calls.push(Promise.resolve([false]));
                    }
                }

                Promise.all(calls).then(result => {
                    let days = Object.keys(assigns);

                    // NOTE: result will always have length 7
                    for (let j = 0; j < result.length; j = j + 1) {
                        // A false indicates that the match won't work
                        if (result[j][0] !== false && sizedUp[passenger.lid].days[j] === true) {
                            hasAtLeastSingleAssign = true;

                            assigns[days[j]].push(passenger.lid);
                            passengers.push(passenger);
                            trip.weekRepeatOpenSeats[days[j]] -= 1;

                            if (typeof newAssigned[days[j]] !== 'undefined') {
                                newAssigned[days[j]].passengers.push({lid: passenger.lid, sid: passenger.sid});
                                newAssigned[days[j]].route = result[j][0][1];
                            } else {
                                newAssigned[days[j]] = {
                                    passengers: [{lid: passenger.lid, sid: passenger.sid}],
                                    route: result[j][0][1]
                                };
                            }
                        }
                    }

                    callback();
                }).catch(err => {
                    return callback(err);
                });
            }, err => {
                if (err) {
                    logger.logWarn(`(script.weeklySearchLifts) No assignments could be made due to an error
${err}`);
                    reject(err);
                } else if (hasAtLeastSingleAssign) {
                    // At least a single assignment was made so that is good
                    resolve(newAssigned);
                } else {
                    // No assignments were made so the allocation was unsuccessful
                    logger.logInfo('(script.weeklySearchLifts) No assignments were made so the allocation was unsuccessful');
                    reject('No matches to make assignment');
                }
            });
        });
    }
}

/**
 * Recalculate the settings for a singleDate trip
 * @param passengers
 * @param driver
 */
function recalculateSingleDateTrip(passengers, driver) {

    let coordinates = [driver.origin, driver.destination];
    for (let i = 0; i < passengers.length; i = i + 1) {
        coordinates.push(passengers[i].origin, passengers[i].destination);
    }

    if (coordinates.length === 2) {
        return Promise.resolve([driver.destination]);
    }

    return adapter.calcOto(coordinates).then(oto => {
        return salesman.solve(oto, driver, passengers);
    }).then(result => {
        return Promise.resolve(result[0]);
    });
}

/**
 * Recalculate the settings for a weekRepeat trip
 * @param passengers
 * @param driver
 */
function recalculateWeeklyTrip(passengers, driver) {

    return new Promise((resolve, reject) => {

        let days = Object.keys(driver.passengers);
        let dailyRoutes = {};

        // Calculate the route for each day of the week. All of these will happen in parallel
        async.forEachOf(days, (day, i, callback) => {

            if (driver.days[day] === false) {
                dailyRoutes[day] = [];
                callback();
            } else {
                let coordinates = [driver.origin, driver.destination];
                let localPassengers = passengers.filter(item => driver.passengers[day].includes(item.lid));

                if (localPassengers.length === 0) {
                    // If no passengers for this day just set the route to empty
                    dailyRoutes[day] = [driver.destination];
                    callback();
                } else {
                    for (let i = 0; i < localPassengers.length; i = i + 1) {
                        coordinates.push(localPassengers[i].origin, localPassengers[i].destination);
                    }

                    adapter.calcOto(coordinates).then(oto => {
                        return salesman.solve(oto, driver, localPassengers);
                    }).then(result => {
                        dailyRoutes[day] = result[0];
                        callback();
                    }).catch(err => {
                        callback(err);
                    });
                }
            }
        }, err => {
            if (err) {
                logger.logInfo(`(script.recalculateWeeklyTrip) No assignments could be made due to an error
${err}`);
                reject(err);
            } else {
                logger.logInfo('(script.singleDateSearchLifts) Successfully recalculated routes');
                resolve(dailyRoutes);
            }
        });
    });
}

/**
 * Filter out drivers that are further away than the distance threshold with direct distance
 * @param passenger
 * @param drivers - An array of drivers
 * @param REALTIME Use the real-time allocation algorithm if set to TRUE
 */
function minifyDriverSet(passenger, drivers, REALTIME=false) {

    // For now just find the first trip that is valid
    let possibles = [];

    for (let driver of drivers) {
        let dist = 0.0;
        let origDist = calcCrow(driver.origin.lat, driver.origin.lon, driver.destination.lat, driver.destination.lon);

        dist += calcCrow(driver.origin.lat, driver.origin.lon, passenger.origin.lat, passenger.origin.lon);
        dist += calcCrow(passenger.origin.lat, passenger.origin.lon, passenger.destination.lat, passenger.destination.lon);
        dist += calcCrow(passenger.destination.lat, passenger.destination.lon, driver.destination.lat, driver.destination.lon);

        logger.logDebug('(script.minifyDriverSet) Original distance to travel = ' + origDist);
        logger.logDebug('(script.minifyDriverSet) Distance to travel with passenger = ' + dist);
        logger.logDebug('(script.minifyDriverSet) Percentage ' + (dist / origDist) + ' [MUST BE <= ' + (1.0 + THRESHOLD) + ' TO BE VALID]');
        if (dist / origDist <= 1.0 + THRESHOLD) {
            possibles.push(driver);
        }
    }

    if (possibles.length <= 0) {
        // No match was found so reject
        logger.logInfo('(script.minifyDriverSet) No possible matches found');
        return Promise.reject('No possible matches found');
    } else {
        logger.logDebug('(script.minifyDriverSet) ONWARDS TO MAPS');
        if (REALTIME) {
            return calcExactDriverRealtime(passenger, possibles).then(drivers => {
                // TODO: Don't just give the first driver. Process them
                return Promise.resolve(drivers);
            }).catch(err => {
                logger.logInfo('(script.minifyDriverSet) Rejected with error: ' + err);
                return Promise.reject(err);
            });
        } else {
            return calcExactDriver(passenger, possibles).then(drivers => {
                // TODO: Don't just give the first driver. Process them
                return Promise.resolve(drivers);
            }).catch(err => {
                logger.logInfo('(script.minifyDriverSet) Rejected with error: ' + err);
                return Promise.reject(err);
            });
        }
    }
}

/**
 * Find an exact driver with map service data
 * @param passenger
 * @param drivers
 * @returns {Promise}
 */
function calcExactDriver(passenger, drivers) {

    let calls = [];
    let func = function (driver) {

        let dist;

        // Calculate the original distance of the driver's trip
        return adapter.calcDist([driver.origin, driver.destination]).then(origDist => {
            dist = origDist[1];
            logger.logDebug('Driver\'s Original dist from Google Maps = ' + dist);

            // Use the driver's current location. Calculations must be done from the current location of the driver
            let coordinates = [driver.origin, driver.destination, passenger.origin, passenger.destination];

            // Remove the driver's destination from the goingTo array. If the array is only length 1 then that is only what is in it so skip
            if (driver.route.length > 1) {
                let tmpRoute = driver.route.slice(0, driver.route.length - 1);
                coordinates = coordinates.concat(tmpRoute);
            }

            return adapter.calcOto(coordinates);
        }).then(oto => {
            let passengers = driver.passengers;
            passengers.push(passenger);

            return salesman.solve(oto, driver, passengers);
        }).then(result => {
            // A user could not reach their location on time
            if (result === -420) {
                logger.logInfo('(script.calcExactDriver) A user could not reach their location on time. FAIL');
                return Promise.resolve(false);
            }

            // The first element in the route is the driver's current location which we don't want it
            let route = result[0].slice(1);
            let realDist = result[1];

            logger.logDebug('(script.calcExactDriver) Original distance = ' + dist);
            logger.logDebug('(script.calcExactDriver) Real distance from Maps = ' + realDist);

            if (realDist / dist <= 1.0 + THRESHOLD) {
                // Resolve the driver and the route that will be taken
                return Promise.resolve([driver, route]);
            } else {
                // 'Driver is not suitable as they will have to drive to far'
                return Promise.resolve(false);
            }
        }).catch(err => {
            logger.logInfo('(script.calcExactDriver) Error ' + err);
            return Promise.reject(err);
        });
    };

    for (let i = 0; i < drivers.length; i = i + 1) {
        calls.push(func(drivers[i]));
    }

    return Promise.all(calls);
}

/**
 * Find an exact driver with map service data
 * @param passenger
 * @param drivers
 * @returns {Promise}
 */
function calcExactDriverRealtime(passenger, drivers) {

    let func = function (driver) {

        let dist;

        // Calculate the original distance of the driver's trip
        return adapter.calcDist([driver.origin, driver.destination]).then(origDist => {
            dist = origDist[1];

            // Use the driver's current location. Calculations must be done from the current location of the driver
            let coordinates = [driver.currLoc, driver.destination, passenger.origin, passenger.destination];

            // Add all the passengers that still have to be dropped off
            for (let i = 0; i < driver.passengers.length; i = i + 1) {
                coordinates.push(driver.passengers[i].origin);
                coordinates.push(driver.passengers[i].destination);
            }

            // Add all the passengers that still have to be picked up
            for (let i = 0; i < driver.pickups.length; i = i + 1) {
                coordinates.push(driver.pickups[i].origin);
                coordinates.push(driver.pickups[i].destination);
            }

            return adapter.calcOto(coordinates);
        }).then(oto => {
            // Add all the passengers into one list
            let passengers = driver.pickups || [];
            passengers = passengers.concat(driver.passengers);
            passengers.push(passenger);

            return salesman.solveRealtime(oto, driver, passengers);
        }).then(result => {
            // A user could not reach their location on time
            if (result === -420) {
                logger.logInfo('(script.calcExactDriverRealtime) A user could not reach their location on time. FAIL');
                return Promise.resolve(false);
            }

            // The first element in the route is the driver's current location which we don't want it
            let route = result[0].slice(1);
            let realDist = result[1];

            logger.logDebug('(script.calcExactDriverRealtime) Driver\'s original distance = ' + dist);
            logger.logDebug('(script.calcExactDriverRealtime) Driver\'s distance with passengers = ' + realDist);

            if (realDist / dist <= 1.0 + THRESHOLD) {
                // Resolve the driver and the route that will be taken
                return Promise.resolve([driver, route]);
            } else {
                // Driver is not suitable as they will have to drive to far
                return Promise.resolve(false);
            }
        }).catch(err => {
            logger.logInfo('(script.calcExactDriverRealtime) Error ' + err);
            return Promise.reject(err);
        });
    };

    let calls = [];
    for (let i = 0; i < drivers.length; i = i + 1) {
        calls.push(func(drivers[i]));
    }

    return Promise.all(calls);
}

module.exports = {

    /**
     * Get a driver for a user looking for a lift (Real-time)
     */
    getFittingTrips: function (passenger) {

        let Drivers;

        return mdb.mdbModules.active.getTripsWithOpenSeats().then(drivers => {

            Drivers = drivers;
            if (Drivers.length === 0) {
                return Promise.resolve(204);
            }

            let calls = [];
            for (let i = 0; i < drivers.length; i = i + 1) {
                calls.push(rdb.getActiveRelease(drivers[i].aid));
            }

            return Promise.all(calls);
        }).then(timestamps => {
            let now = new Date().getTime();
            // Only use the drivers that aren't locked or who's locks have expired
            Drivers = Drivers.filter((item, index) => timestamps[index] === null || (now - Number(timestamps[index])) > config.ACTIVE_LOCK_TIME);

            if (Drivers.length === 0) {
                return Promise.resolve(204);
            } else {
                return minifyDriverSet(passenger, Drivers, true).then(results => {
                    results = results.filter(item => item !== false);
                    if (results.length > 0) {
                        return Promise.resolve(results);
                    } else {
                        return Promise.resolve(204);
                    }
                });
            }
        });
    },

    /**
     * Get a scheduled trip that meets the requirements of a scheduled lift
     * @param lid
     * @returns {*|Promise}
     */
    getFittingScheduledTrip: function (lid) {

        let Lift;
        let Trips;

        return mdb.mdbModules.schedule.getScheduledLift(lid).then(lift => {
            if (lift.length === 0) {
                return Promise.reject();
            } else {
                Lift = lift[0];

                return mdb.mdbModules.schedule.getAllScheduledTrips(lift[0].singleDate);
            }
        }).then(trips => {
            // Remove any trips that have the same user as that of the lift.
            // Prevents trips being assigned to lifts where the driver is the passenger - This would not make sense
            Trips = trips.filter(item => item.sid !== Lift.sid);

            if (Trips.length === 0) {
                logger.logInfo('(script.getFittingScheduledTrip) There are no scheduled trips in the system to match to the lift');
                return Promise.reject('No scheduled trips in system');
            } else {
                let calls = [];
                for (let trip of Trips) {
                    calls.push(rdb.isIdMarkedForDeletion(trip.tid));
                }

                return Promise.all(calls);
            }
        }).then(v => {
            // Remove any trips that have been marked for deletion
            Trips = Trips.filter((item, index) => v[index] === false);

            if (Trips.length === 0) {
                logger.logInfo('(script.getFittingScheduledTrip) There are no scheduled trips in the system to match to the lift');
                return Promise.reject('No scheduled trips in system');
            } else {
                // If single day schedule
                if (Lift.singleDate) {

                    // TODO: Remove all trips that don't have any seats left. Not really needed here as will still fail later on. Just more performance efficient

                    if (Lift.singleDateDriver !== '') {
                        return Promise.reject('Lift already has a singleDate driver');
                    }

                    // Get all the user lift schedules that belong to each of the drivers
                    let singleDateDrivers = {};
                    let calls = [];
                    for (let trip of Trips) {
                        singleDateDrivers[trip.sid] = [];

                        if (trip.singleDatePassengers.length === 0) {
                            calls.push(Promise.resolve([]));
                        } else {
                            calls.push(mdb.mdbModules.schedule.getMultiScheduledLifts(trip.singleDatePassengers));
                        }
                    }

                    return Promise.all(calls).then(values => {
                        let keys = Object.keys(singleDateDrivers);
                        for (let i = 0; i < keys.length; i = i + 1) {
                            // Set the user lift data to their assigned driver for the day
                            singleDateDrivers[keys[i]] = values[i];
                        }

                        // Give the lift, the trips, and mappings of passengers to drivers with the passengers' info
                        return singleDateSearchTrips(Lift, Trips, singleDateDrivers);
                    }).then(drivers => {
                        if (drivers.length === 0) {
                            return Promise.reject('The are no drivers suitable for this trip');
                        } else {
                            // TODO: For now just return the first driver in the list
                            return Promise.resolve(drivers[0]);
                        }
                    });
                }
                // If weekly schedule
                else if (Lift.weekRepeat) {

                    // TODO: Remove all trips that don't have any seats left. Not really needed here as will still fail later on. Just more performance efficient?

                    // Get all the user lift that belong to each of the drivers
                    let weekRepeatDrivers = {};
                    let calls = [];
                    for (let i = 0; i < Trips.length; i = i + 1) {
                        weekRepeatDrivers[Trips[i].sid] = [];

                        let passengers = [];
                        let days = Object.keys(Trips[i].passengers); // Days of the week
                        for (let j = 0; j < days.length; j = j + 1) {
                            passengers = passengers.concat(Trips[i].passengers[days[j]]);
                        }

                        calls.push(mdb.mdbModules.schedule.getMultiScheduledLifts(passengers));
                    }

                    return Promise.all(calls).then(values => {
                        let keys = Object.keys(weekRepeatDrivers);
                        for (let i = 0; i < keys.length; i = i + 1) {
                            // Set the user lift data to their assigned driver for the day
                            weekRepeatDrivers[keys[i]] = values[i];
                        }

                        // Give the lift, the trips, and mappings of passengers to drivers with the passengers' info
                        return weeklySearchTrips(Lift, Trips, weekRepeatDrivers)
                    }).then(res => {
                        logger.logInfo(`(script.getFittingScheduledTrip) Weekly scheduled trips are assigned as follows to scheduled lift ${Lift.lid}`);
                        logger.logInfo(`(script.getFittingScheduledTrip) ${JSON.stringify(res, null, 4)}`);

                        return Promise.resolve(res);
                    });
                } else {
                    return Promise.reject('Invalid schedule type was given (singleDate/weekRepeat)');
                }
            }
        });
    },

    /**
     * Check if there are any scheduled lifts that could be added to the scheduled trip
     * @param tid
     * @returns {*|Promise}
     */
    getFittingScheduledLifts: function (tid) {

        let Trip;
        let Lifts;

        return mdb.mdbModules.schedule.getScheduledTrip(tid).then(trip => {
            if (trip.length === 0) {
                return Promise.reject();
            } else {
                Trip = trip[0];

                return mdb.mdbModules.schedule.getAllScheduledLifts(trip[0].singleDate);
            }
        }).then(lifts => {
            // Remove any lifts that have the same user as that of the trip.
            // Prevents lifts being assigned to trips where the driver is the passenger - This would not make sense
            Lifts = lifts.filter(item => item.sid !== Trip.sid);

            if (Lifts.length === 0) {
                logger.logInfo('(script.getFittingScheduledLifts) There are no scheduled lifts in the system to match to the trip)');
                return Promise.reject('No scheduled lifts in system');
            } else {
                let calls = [];
                for (let lift of Lifts) {
                    calls.push(rdb.isIdMarkedForDeletion(lift.lid));
                }

                return Promise.all(calls);
            }
        }).then(v => {
            // Remove any trips that have been marked for deletion
            Lifts = Lifts.filter((item, index) => v[index] === false);

            if (Lifts.length === 0) {
                logger.logInfo('(script.getFittingScheduledTrip) There are no scheduled trips in the system to match to the lift');
                return Promise.reject('No scheduled trips in system');
            } else {
                // If single day schedule
                if (Trip.singleDate) {

                    return mdb.mdbModules.schedule.getMultiScheduledLifts(Trip.singleDatePassengers).then(passengers => {
                        return singleDateSearchLifts(Lifts, Trip, passengers);
                    }).then(lifts => {
                        if (lifts.length === 0) {
                            return Promise.reject('No assignments were made');
                        } else {
                            return Promise.resolve(lifts);
                        }
                    });
                }
                // If weekly schedule
                else if (Trip.weekRepeat) {
                    let lids = [];
                    let days = Object.keys(Trip.days);
                    for (let i = 0; i <days.length; i = i + 1) {
                        // If the trip is active this day then get the lift info
                        if (Trip.days[days[i]] === true) {
                            lids = lids.concat(Trip.passengers[days[i]]);
                        }
                    }

                    // Get only unique lids: O(n^2)
                    lids = lids.filter((v, i, a) => a.indexOf(v) === i);
                    return mdb.mdbModules.schedule.getMultiScheduledLifts(lids).then(passengers => {
                        return weeklySearchLifts(Lifts, Trip, passengers);
                    }).then(matches => {
                        logger.logInfo(`(script.getFittingScheduledTrip) Weekly scheduled lifts are assigned as follows to scheduled trip ${Trip.tid}`);
                        logger.logInfo(`(script.getFittingScheduledTrip) ${Lifts}`);

                        return Promise.resolve(matches);
                    });
                } else {
                    return Promise.reject('Have no idea what you are trying to say to me ;p');
                }
            }
        });
    },

    /**
     * Recalculate the route of a scheduled trip. This is for when a lift is removed from the trip and the route has to change
     * @param tid
     */
    recalculateScheduledTrip: function(tid) {

        return mdb.mdbModules.schedule.getScheduledTrip(tid).then(trip => {
            if (trip.length === 0) {
                return Promise.reject();
            } else {
                trip = trip[0];

                if (trip.singleDate) {
                    return mdb.mdbModules.schedule.getMultiScheduledLifts(trip.singleDatePassengers).then(passengers => {
                        return recalculateSingleDateTrip(passengers, trip);
                    });
                } else if (trip.weekRepeat) {
                    let lids = [];
                    let days = Object.keys(trip.days);
                    for (let i = 0; i < days.length; i = i + 1) {
                        // If the trip is active this day then get the lift info
                        if (trip.days[days[i]] === true) {
                            lids = lids.concat(trip.passengers[days[i]]);
                        }
                    }

                    // Get only unique lids: O(n^2)
                    lids = lids.filter((v, i, a) => a.indexOf(v) === i);
                    return mdb.mdbModules.schedule.getMultiScheduledLifts(lids).then(passengers => {
                        return recalculateWeeklyTrip(passengers, trip);
                    });
                } else {
                    return Promise.reject('Have no idea what you are trying to say to me ;p');
                }
            }
        });
    }
};
