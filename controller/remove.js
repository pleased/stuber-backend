/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../logger.js');

let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let kue = require('./kue.js');

module.exports = {

    /**
     * Delete the specified user
     * @param sid
     */
    deleteUser: function (sid) {

        // First check that the user is not on an active trip
        return mdb.mdbModules.active.getTripWithSid(sid).then(item => {
            if (item.length > 0) {
                return Promise.reject('User is on an active trip. Please complete it first');
            } else {
                return mdb.mdbModules.vehicle.getVehiclesOfOwner(sid);
            }
        }).then(items => {
            // TODO: This deletion assumes that each vehicle will have one owner
            // Delete the vehicles if there are any
            if (items.length > 0) {
                let calls = [];
                for (let i = 0; i < items.length; i = i + 1) {
                    calls.push(mdb.mdbModules.vehicle.removeVehicle(items[i].vid));
                }

                return Promise.all(calls);
            } else {
                return Promise.resolve();
            }
        }).then(() => {
            // Get all the trips and lifts the user has scheduled
            return Promise.all([
                mdb.mdbModules.schedule.getScheduledTrips(sid),
                mdb.mdbModules.schedule.getScheduledLifts(sid)
            ]);
        }).then(v => {
            let trips = v[0];
            let lifts = v[1];

            let calls = [];

            // Mark each trip as being deleted and then delete it
            for (let i = 0; i < trips.length; i = i + 1) {
                calls.push(rdb.addMarkedForDeletionId(trips[i].tid));
                calls.push(kue.addRemoveScheduledTrip({
                    tid: trips[i].tid
                }));
            }

            // Mark each lift as being deleted and then delete it
            for (let i = 0; i < lifts.length; i = i + 1) {
                calls.push(rdb.addMarkedForDeletionId(lifts[i].lid));
                calls.push(kue.addRemoveScheduledLift({
                    lid: lifts[i].lid
                }));
            }

            return Promise.all(calls);
        }).then(() => {
            // Finally delete the user and their inbox
            return Promise.all([
                mdb.mdbModules.user.removeUser(sid),
                mdb.mdbModules.inbox.removeInbox(sid)
            ]);
        }).then(v => {
            return rdb.removeUser(v[0].gid, sid);
        }).then(() => {
            logger.logInfo('Successfully deleted user ' + sid);
            return Promise.resolve('Successfully deleted the user');
        });
    },

    /**
     * Delete the specified vehicle
     * @param vid
     */
    deleteVehicle: function (vid) {

        // First check that no trips are using this vehicle before attempting deletion
        return mdb.mdbModules.active.getTripWithVehicle(vid).then(item => {
            if (item) {
                return Promise.reject('Vehicle is being used in an active trip. Please complete it first');
            } else {
                return mdb.mdbModules.schedule.getAllScheduledTripsWithVehicle(vid);
            }
        }).then(items => {
            if (items.length > 0) {
                return Promise.reject('Vehicle is being used in scheduled trips. Please delete them first');
            } else {
                return rdb.removeVid(vid);
            }
        }).then(() => {
            return mdb.mdbModules.vehicle.removeVehicle(vid);
        }).then(() => {
            logger.logInfo('Successfully deleted vehicle ' + vid);
            return Promise.resolve('Successfully deleted the vehicle');
        });
    }
};
