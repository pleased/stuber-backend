/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let kue = require('kue');

let config = require('../config.js');
let logger = require('../logger.js');

let singleTask = require('./kueTasks/scheduleSingle.js');
let weeklyTask = require('./kueTasks/scheduleWeekly.js');

let refreshTask;
let removeTask;

// The queues
let weeklyQueue;
let singleDateQueue;
let removeQueue;

// TODO: Rather replace the comparisons with some enum value instead in the future
const decideDirection = function (job, done) {

    // For weekly events
    if (job.data.direction === 'liftWeeklyJob') {
        weeklyTask.scheduleWeeklyLift(job, done);
    } else if (job.data.direction === 'tripWeeklyJob') {
        weeklyTask.scheduleWeeklyTrip(job, done);
    } else if (job.data.direction === 'removeLiftDriverWeeklyJob') {
        removeTask.removeLiftDriverWeekRepeat(job, done);
    } else if (job.data.direction === 'removeTripPassengerWeeklyJob') {
        removeTask.removeTripPassengerWeekly(job, done);
    } else if (job.data.direction === 'refreshTripWeeklyJob') {
        refreshTask.refreshWeeklyTrip(job, done);
    }
    // For single day events
    else if (job.data.direction === 'liftSingleDateJob') {
        singleTask.scheduleSingleLift(job, done);
    } else if (job.data.direction === 'tripSingleDateJob') {
        singleTask.scheduleSingleTrip(job, done);
    } else if (job.data.direction === 'removeDriverLiftSingleDateJob') {
        removeTask.removeLiftDriverSingleDate(job, done);
    } else if (job.data.direction === 'removeTripPassengerSingleDateJob') {
        removeTask.removeTripPassengerSingleDate(job, done);
    } else if (job.data.direction === 'refreshTripSingleDateJob') {
        refreshTask.refreshSingleDateTrip(job, done);
    }
    // For remove events
    else if (job.data.direction === 'removeScheduledLiftJob') {
        removeTask.removeScheduledLift(job, done);
    } else if (job.data.direction === 'removeScheduledTripJob') {
        removeTask.removeScheduledTrip(job, done);
    } else {
        done('You don\'t belong here. Be gone foul beast ->', job);
    }
};

module.exports = {

    /**
     *
     * @param isMaster
     * @returns {Promise}
     */
    init: function (isMaster=false) {

        // The object is passed as argument due to circular dependency, if kue.js is not completely
        // initialized when these modules requires it an error is thrown. This assigns the local ones
        // to this object being initialized. The require happens here to make sure kue.js has been initialized
        // passing it on
        refreshTask = require('./kueTasks/refresh.js')(this);
        removeTask = require('./kueTasks/remove.js')(this);

        return new Promise((resolve, reject) => {

            // TODO: Check if queues were created successfully
            // Create the queues
            weeklyQueue = kue.createQueue();
            singleDateQueue = kue.createQueue();
            removeQueue = kue.createQueue();

            // If master worker make it process the jobs
            if (isMaster) {
                logger.logInfo('(kue.init) Master worker is being assigned processing of jobs');

                // Set the tasks for each job type in the queues
                // Weekly
                weeklyQueue.process('weeklyJob', 1, (job, done) => {
                    decideDirection(job, done);
                });

                // SingleDate
                singleDateQueue.process('singleDateJob', 1, (job, done) => {
                    decideDirection(job, done);
                });

                // Remove
                removeQueue.process('removeJob', 1, (job, done) => {
                    decideDirection(job, done);
                });
            }

            resolve();
        });
    },

    // ******************************
    // LIFT
    // ******************************

    /**
     *
     * @param data
     */
    addLiftWeeklyJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'liftWeeklyJob';

            weeklyQueue.create('weeklyJob', data)
                .removeOnComplete(false)
                .save((err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     *
     * @param data
     */
    addLiftSingleDateJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'liftSingleDateJob';

            singleDateQueue.create('singleDateJob', data)
                .removeOnComplete(false)
                .save((err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     * Remove a weekly driver from the lift
     * This job will have the highest kue priority (-15)
     * @param data
     * @returns {Promise}
     */
    addRemoveLiftDriverWeeklyJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'removeLiftDriverWeeklyJob';

            singleDateQueue.create('weeklyJob', data)
                .removeOnComplete(false)
                .priority(-15)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     * Remove a singleDate driver from the lift
     * This job will have the highest kue priority (-15)
     * @param data
     * @returns {Promise}
     */
    addRemoveLiftDriverSingleDateJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'removeDriverLiftSingleDateJob';

            singleDateQueue.create('singleDateJob', data)
                .removeOnComplete(false)
                .priority(-15)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     * Remove a scheduled lift
     * @param data
     */
    addRemoveScheduledLift: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'removeScheduledLiftJob';

            removeQueue.create('removeJob', data)
                .removeOnComplete(false)
                .delay(config.KUE_DELAY)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    // ******************************
    // TRIP
    // ******************************

    /**
     *
     * @param data
     */
    addTripWeeklyJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'tripWeeklyJob';

            weeklyQueue.create('weeklyJob', data)
                .removeOnComplete(false)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     *
     * @param data
     */
    addTripSingleDateJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'tripSingleDateJob';

            singleDateQueue.create('singleDateJob', data)
                .removeOnComplete(false)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     * Recalculate a weekly trip's route with only passengers already assigned to it.
     * This job will have the second highest kue priority (-14)
     * @param data
     * @returns {Promise}
     */
    addRefreshTripWeeklyJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'refreshTripWeeklyJob';

            singleDateQueue.create('weeklyJob', data)
                .removeOnComplete(false)
                .priority(-14)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     * Recalculate a singleDate trip's route with only passengers already assigned to it.
     * This job will have the second highest kue priority (-14)
     * @param data
     * @returns {Promise}
     */
    addRefreshTripSingleDateJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'refreshTripSingleDateJob';

            singleDateQueue.create('singleDateJob', data)
                .removeOnComplete(false)
                .priority(-14)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     * Remove a weekly passenger from a trip
     * This job will have the highest kue priority (-15)
     * @param data
     * @returns {Promise}
     */
    addRemoveTripPassengerWeeklyJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'removeTripPassengerWeeklyJob';

            singleDateQueue.create('weeklyJob', data)
                .removeOnComplete(false)
                .priority(-15)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     * Remove a singleDate passenger from a trip
     * This job will have the highest kue priority (-15)
     * @param data
     * @returns {Promise}
     */
    addRemoveTripPassengerSingleDateJob: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'removeTripPassengerSingleDateJob';

            singleDateQueue.create('singleDateJob', data)
                .removeOnComplete(false)
                .priority(-15)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    },

    /**
     * Remove a scheduled trip
     * @param data
     */
    addRemoveScheduledTrip: function (data) {

        return new Promise((resolve, reject) => {
            data.direction = 'removeScheduledTripJob';

            removeQueue.create('removeJob', data)
                .removeOnComplete(false)
                .delay(config.KUE_DELAY)
                .save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    }
};
