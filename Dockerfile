FROM node:7

WORKDIR /app

COPY . /app/
RUN npm install

# Application's default port
EXPOSE 3000
CMD ["npm", "start"]
