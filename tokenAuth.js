/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('./logger.js');
let config = require('./config.js');

let jwt = require('jsonwebtoken');

logger.logWarn('(tokenAuth) Token authentication BYPASS = ' + config.BYPASS_AUTH);

function validateToken(token, callback) {

    if (config.BYPASS_AUTH) {
        logger.logWarn('(tokenAuth.validateToken) ** Authentication Bypassed **');
        callback(200, '** Authentication Bypassed **');
    } else if (!token) {
        logger.logInfo("(tokenAuth.validateToken) Request has no token in headers");
        callback(401, "Authentication failed - Request has no token in headers");
    } else {
        jwt.verify(token, config.TOKEN, function(err, decoded) {
            if (decoded) {
                logger.logInfo("(tokenAuth.validateToken) Decoded token successfully");
                logger.logDebug("(tokenAuth.validateToken) Decoded token value = " + decoded.data);

                callback(200, decoded.data);
            } else {
                logger.logDebug('(tokenAuth.validateToken) Error message = ' + err.message);
                if (err.name === 'TokenExpiredError') {
                    logger.logInfo("(tokenAuth.validateToken) Token has expired");
                    callback(401, 'Authentication failed - Token has expired');
                } else {
                    logger.logInfo("(tokenAuth.validateToken) Invalid token");
                    callback(401, 'Authentication failed - Invalid token');
                }
            }
        });
    }
}

function generateToken(gid) {

    // Create token
    return jwt.sign({
        data: gid
    }, config.TOKEN, { expiresIn: '1d' });
}

module.exports = {
    validateToken: validateToken,
    generateToken: generateToken
};
