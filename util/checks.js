/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */

let logger = require('../logger.js');

const STRING = 'string';
const NUMBER = 'number';
const BOOLEAN = 'boolean';

const checks = function (json) {
    // Check that the starTime and endTime were provided
    if (!json.startTime || !json.endTime) {
        logger.logInfo('(util.checks.checks) Neither endTime and startTime were provided');
        return 'Neither endTime and startTime were provided';
    } else if (!json.startTime.hour || !json.startTime.minute || !json.endTime.hour || !json.endTime.minute) {
        logger.logInfo('(util.checks.checks) endTime/startTime hour or minute were not provided');
        return 'endTime/startTime hour or minute were not provided';
    } else if (typeof json.startTime.hour !== NUMBER || typeof json.startTime.minute !== NUMBER || typeof json.endTime.hour !== NUMBER || typeof json.endTime.minute !== NUMBER) {
        logger.logInfo('(util.checks.checks) endTime/startTime hour or minute are not of type number');
        return 'endTime/startTime hour or minute are not of type number';
    }
    // Check that the destination and origin were provided
    else if (!json.destination || !json.origin) {
        logger.logInfo('(util.checks.checks) destination/origin not provided');
        return 'destination/origin not provided';
    } else if (!json.destination.lat || !json.destination.lon || !json.origin.lat || !json.origin.lon) {
        logger.logInfo('(util.checks.checks) destination/origin lat or lon not provided');
        return 'destination/origin latitude or longitude not provided';
    } else if (typeof json.destination.lat !== NUMBER || typeof json.destination.lon !== NUMBER || typeof json.origin.lat !== NUMBER || typeof json.origin.lon !== NUMBER) {
        logger.logInfo('(util.checks.checks) destination/origin latitude or longitude are not of type number');
        return 'destination/origin latitude or longitude are not of type number';
    } else {
        return true;
    }
};

const checksSchedule = function json(json) {

    // Check that at least weekly or single day have been specified
    if (typeof json.singleDate === 'undefined' && typeof json.weekRepeat === 'undefined') {
        logger.logInfo('(util.checks.ckecksSchedule) Neither singleDate or weekRepeat were specified');
        return 'Neither singleDate or weekRepeat were specified';
    }
    // Check that singleDate and weekRepeat are of type boolean
    else if (typeof json.singleDate !== BOOLEAN || typeof json.weekRepeat !== BOOLEAN) {
        logger.logInfo('(util.checks.ckecksSchedule) singleDate/weekRepeat are not of type boolean');
        return 'singleDate/weekRepeat are not of type boolean';
    }
    // Check if weekRepeat was specified that at least one day of the week is marked
    else if (json.weekRepeat && (!json.days || (!json.days.monday && !json.days.tuesday && !json.days.wednesday && !json.days.thursday && !json.days.friday && !json.days.saturday && !json.days.sunday))) {
        logger.logInfo('(util.checks.ckecksSchedule) weekRepeat specified but no days indicated');
        return 'weekRepeat specified but no days indicated';
    }
    // Check if weekRepeat was specified that the types of the days provide are boolean
    else if (json.weekRepeat && (
        (json.days.monday && typeof json.days.monday !== BOOLEAN) ||
        (json.days.tuesday && typeof json.days.tuesday !== BOOLEAN) ||
        (json.days.wednesday && typeof json.days.wednesday !== BOOLEAN) ||
        (json.days.thursday && typeof json.days.thursday !== BOOLEAN) ||
        (json.days.friday && typeof json.days.friday !== BOOLEAN) ||
        (json.days.saturday && typeof json.days.saturday !== BOOLEAN) ||
        (json.days.sunday && typeof json.days.sunday !== BOOLEAN))) {
        logger.logInfo('(util.checks.ckecksSchedule) One of the specified days are not of type boolean');
        return 'One of the specified days are not of type boolean';
    }
    // Check if singleDate was specified that a date was indicated
    else if (json.singleDate && !json.date) {
        logger.logInfo('(util.checks.ckecksSchedule) singleDate specified but no date indicated');
        return 'singleDate specified but no date indicated';
    }
    // TODO: Figure out what the correct type of the date should be
    // Check if singleDate was specified that the date is of type ??????
    // else if (json.singleDate && (typeof json.date !== NUMBER || typeof json.date !== STRING)) {
    //     return 'Date supplied is of invalid type'
    // }
    else {
        return checks(json);
    }
};

module.exports = {

    /**
     * Check if val is True and if it is resolve else reject with value string. This uses promises to easily be chain with existing promise chains
     * @param val
     * @param string
     * @returns {*}
     */
    is: function (val, string) {
        if (val !== false) {
            return Promise.resolve();
        } else {
            return Promise.reject(string);
        }
    },

    /**
     * Calls function func and applies is() on the result
     * @param func Function that must return a promise
     * @param string
     */
    isWrapper: function (func, string) {

        return func().then(val => {
            return this.is(val, string);
        });
    },

    /**
     * Check if val is False and if it is resolve else reject with value string. This uses promises to easily be chain with existing promise chains
     * @param val
     * @param string
     * @returns {*}
     */
    isNot: function (val, string) {
        if (val === false) {
            return Promise.resolve();
        } else {
            return Promise.reject(string);
        }
    },

    /**
     * Calls function func and applies isNot() on the result
     * @param func Function that must be return promise
     * @param string
     */
    isNotWrapper: function (func, string) {

        return func().then(val => {
            return this.isNot(val, string);
        });
    },

    /**
     *
     * @param json
     */
    validLiftScheduleSettings: function (json) {

        return checksSchedule(json);
    },

    /**
     * Check if the json has the required fields for a lift
     * @param json
     */
    validLiftSettings: function (json) {

        return checks(json);
    },

    /**
     * Check if the json has the required fields for a lift selected
     * @param json
     * @returns {string || boolean}
     */
    validLiftSelectedSettings: function (json) {

        if (!json.aid) {
            logger.logInfo('(util.checks.validLiftSelectedSettings) No AID was provided');
            return 'No AID was provided';
        } else if (typeof json.aid !== STRING) {
            logger.logInfo('(util.checks.validLiftSelectedSettings) AID is not of type string');
            return 'AID is not of type string';
        } else if (json.aid.length === 0) {
            logger.logInfo('(util.checks.validLiftSelectedSettings) The provided AID is of length 0');
            return 'The provided AID is of length 0';
        } else {
            return true;
        }
    },

    /**
     * Check if the json has the required fields for a lift decided
     * @param json
     * @returns {string || boolean}
     */
    validLiftDecidedSettings: function (json) {

        if (!json.passenger) {
            logger.logInfo('(util.checks.validLiftDecidedSettings) No passenger SID was provided');
            return 'No passenger SID was provided';
        } else if (typeof json.passenger !== STRING) {
            logger.logInfo('(util.checks.validLiftDecidedSettings) Passenger SID is not of type string');
            return 'Passenger SID is not of type string';
        } else if (json.passenger.length === 0) {
            logger.logInfo('(util.checks.validLiftDecidedSettings) The provided passenger SID is of length 0');
            return 'The provided passenger SID is of length 0';
        } else if (typeof json.decided === 'undefined') {
            logger.logInfo('(util.checks.validLiftDecidedSettings) No passenger SID was provided');
            return 'No passenger SID was provided';
        } else if (typeof json.decided !== BOOLEAN) {
            logger.logInfo('(util.checks.validLiftDecidedSettings) Passenger SID is not of type string');
            return 'Passenger SID is not of type string';
        } else {
            return true;
        }
    },

    /**
     *
     * @param json
     * @returns {string || boolean}
     */
    validTripScheduleSettings: function (json) {

        if (!json.vid) {
            logger.logInfo('(util.checks.validTripScheduleSettings) No vehicle VID was provided');
            return 'No vehicle VID was provided';
        } else if (typeof json.vid !== STRING) {
            logger.logInfo('(util.checks.validTripScheduleSettings) Vehicle VID is not of type string');
            return 'Vehicle VID is not of type string';
        } else if (json.vid.length === 0) {
            logger.logInfo('(util.checks.validTripScheduleSettings) The provided vehicle VID is of length 0');
            return 'The provided vehicle VID is of length 0';
        }

        return checksSchedule(json);
    },

    /**
     * Check if the json has the required fields for a trip
     * @param json
     */
    validTripSettings: function (json) {

        if (!json.vid) {
            logger.logInfo('(util.checks.validTripSettings) No vehicle VID was provided');
            return 'No vehicle VID was provided';
        } else if (typeof json.vid !== STRING) {
            logger.logInfo('(util.checks.validTripSettings) Vehicle VID is not of type string');
            return 'Vehicle VID is not of type string';
        } else if (json.vid.length === 0) {
            logger.logInfo('(util.checks.validTripSettings) The provided vehicle VID is of length 0');
            return 'The provided vehicle VID is of length 0';
        }

        return checks(json);
    },

    /**
     * Check if the json has the required fields for a GPS update
     * @param json
     * @returns {string || boolean}
     */
    validGPSUpdateSettings: function (json) {

        if (!json.lat || !json.lon) {
            logger.logInfo('(util.checks.validGPSUpdateSettings) No latitude/longitude was provided');
            return 'No latitude/longitude was provided';
        } else if (typeof json.lat !== NUMBER || typeof json.lon !== NUMBER) {
            logger.logInfo('(util.checks.validGPSUpdateSettings) Latitude/Longitude is not of type number');
            return 'Latitude/Longitude is not of type number';
        } else {
            return true;
        }
    },

    /**
     * Check if the json has the required fields for submitting a new review
     * @param json
     */
    validSubmitReviewSettings: function (json) {

        if (!json.reviewer || !json.reviewed) {
            logger.logInfo('(util.checks.validSubmitReviewSettings) Reviewer/Reviewed SID was not provided');
            return 'Reviewer/Reviewed SID was not provided';
        } else if (typeof json.reviewer !== STRING || typeof json.reviewed !== STRING) {
            logger.logInfo('(util.checks.validSubmitReviewSettings) Reviewer/Reviewed SID is not of type string');
            return 'Reviewer/Reviewed SID is not of type string';
        } else if (json.reviewer.length === 0 || json.reviewed.length === 0) {
            logger.logInfo('(util.checks.validSubmitReviewSettings) Reviewer/Reviewed SID is of length 0');
            return 'Reviewer/Reviewed SID is of length 0';
        } else if (json.rating === undefined) {
            logger.logInfo('(util.checks.validSubmitReviewSettings) Rating was not provided');
            return 'Rating was not provided';
        } else if (typeof json.rating !== NUMBER) {
            logger.logInfo('(util.checks.validSubmitReviewSettings) Rating was not of type number');
            return 'Rating was not of type number';
        } else {
            return true;
        }
    },

    /**
     *
     * @param json
     * @param vehicleSeats
     */
    validScheduleOpenSeats: function (json, vehicleSeats) {

        if (json.singleDate === true && json.singleDateOpenSeats > vehicleSeats) {
            logger.logInfo('(util.checks.validScheduleOpenSeats) Number of single date open seats specified is more than the vehicle supports');
            return 'Number of single date open seats specified is more than the vehicle supports';
        } else if (json.weekRepeat === true) {
            let days = Object.keys(json.days);
            for (let i = 0; i < days.length; i = i + 1) {
                let day = days[i];
                if (json.days[day] && json.days[day] === true && !json.weekRepeatOpenSeats[day]) {
                    logger.logInfo(`(util.checks.validScheduleOpenSeats) ${day} specified but number of open seats for day not`);
                    return `${day} specified but number of open seats for day not`;
                } else if (json.weekRepeatOpenSeats[day] <= 0) {
                    logger.logInfo(`(util.checks.validScheduleOpenSeats) Number of open seats specified for ${day} is zero or negative`);
                    return `Number of open seats specified for ${day} is zero or negative`;
                } else if (json.weekRepeatOpenSeats[day] > vehicleSeats) {
                    logger.logInfo(`(util.checks.validScheduleOpenSeats) Number of open seats specified for ${day} is more than the vehicle supports`);
                    return `Number of open seats specified for ${day} is more than the vehicle supports`;
                }
            }
        }

        return true;
    }
};
