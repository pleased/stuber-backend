/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
module.exports = {

    /**
     *
     * @param current
     * @param expectedHour
     * @param expectedMinute
     * @param deltaBefore
     * @param deltaAfter
     */
    timeCompareBetween: function (current, expectedHour, expectedMinute, deltaBefore, deltaAfter) {
        let expectedSeconds = ((expectedHour * 60) + expectedMinute) * 60;

        return current >= expectedSeconds - deltaBefore && current <= expectedSeconds + deltaAfter;
    },

    /**
     *
     * @param current
     * @param expectedHour
     * @param expectedMinute
     * @param deltaAfter
     * @returns {boolean}
     */
    timeCompareBetween2: function (current, expectedHour, expectedMinute, deltaAfter) {
        let expectedSeconds = ((expectedHour * 60) + expectedMinute) * 60;

        return current <= expectedSeconds + deltaAfter;
    },

    /**
     *
     * @param date1
     * @param date2
     */
    dateCompare: function (date1, date2) {
        // TODO:
    }
};
