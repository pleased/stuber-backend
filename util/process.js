/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */

module.exports = {

    /**
     * Convert the startTime[hour/minute] and endTime[hour/minute] into proper Date objects
     * @param json
     */
    time: function (json) {

        // Only applicable to schedules
        if (json.singleDate) {
            json.date = new Date().setTime(json.date);
        }

        let startTime = new Date();
        let endTime = new Date();

        startTime.setHours(json.startTime.hour);
        startTime.setMinutes(json.startTime.minute);

        endTime.setHours(json.endTime.hour);
        endTime.setMinutes(json.endTime.minute);

        json.startTime = startTime;
        json.endTime = endTime;
    }
};
