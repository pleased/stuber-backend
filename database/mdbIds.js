/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// A MongoDB helper class that retrieves all IDs from the database. Only used for Redis database setup

let logger = require('../logger.js');

// ----- Models -----
let User = require('../models/user.js').User;
let Vehicle = require('../models/vehicle.js').Vehicle;

let Active = require('../models/active.js').Active;
let TripSchedule = require('../models/tripSchedule.js').TripSchedule;
let LiftSchedule = require('../models/liftSchedule.js').LiftSchedule;

let WaitingList = require('../models/waitingList.js').WaitingList;

module.exports = {

    /**
     *
     * @returns {Promise}
     */
    getGids: function() {
        return User.find({}, {_id: false, gid: true}).lean();
    },

    /**
     *
     * @returns {Promise}
     */
    getSids: function() {
        return User.find({}, {_id: false, sid: true}).lean();
    },

    /**
     *
     * @returns {Promise}
     */
    getVids: function() {
        return Vehicle.find({}, {_id: false, vid: true}).lean();
    },

    /**
     *
     * @returns {Promise}
     */
    getAids: function() {
        return Active.find({}, {_id: false, aid: true}).lean();
    },

    /**
     *
     * @returns {Promise}
     */
    getTids: function() {
        return TripSchedule.find({}, {_id: false, tid: true}).lean();
    },

    /**
     *
     * @returns {Promise}
     */
    getLids: function() {
        return LiftSchedule.find({}, {_id: false, lid: true}).lean();
    },

    /**
     *
     * @returns {Promise}
     */
    getWaitingList: function () {
        return WaitingList.find({}, {_id: false, sid: true}).lean();
    }
};
