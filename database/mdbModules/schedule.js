/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let shortid = require("shortid");

let logger = require('../../logger.js');
let buildErrorMessage = require("./util.js").buildErrorMessage;

// ----- Models -----
let TripSchedule = require('../../models/tripSchedule.js').TripSchedule;
let LiftSchedule = require('../../models/liftSchedule.js').LiftSchedule;

module.exports = {

    /**
     * Save a trip schedule
     * @param sid
     * @param json
     * @returns {Promise}
     */
    scheduleTrip: function (sid, json) {

        // TODO: Consider getting the amount of seats in the vehicle and save them in here

        json.sid = sid;
        json.tid = shortid.generate();

        if (json.singleDate) {
            // Set the singleDate initial route
            json.singleDateRoute = [json.destination];
        } else {
            // Set the weekly initial routes
            json.weekRepeatRoute = {};

            json.weekRepeatRoute.monday = json.days.monday ? [json.destination] : [];
            json.weekRepeatRoute.tuesday = json.days.tuesday ? [json.destination] : [];
            json.weekRepeatRoute.wednesday = json.days.wednesday ? [json.destination] : [];
            json.weekRepeatRoute.thursday = json.days.thursday ? [json.destination] : [];
            json.weekRepeatRoute.friday = json.days.friday ? [json.destination] : [];
            json.weekRepeatRoute.saturday = json.days.saturday ? [json.destination] : [];
            json.weekRepeatRoute.sunday = json.days.sunday ? [json.destination] : [];
        }

        // Finally save the trip
        return TripSchedule(json).save().then(() => {
            logger.logInfo(`(mdbModules.schedule.scheduleTrip) Successfully saved scheduled trip ${json.tid}`);
            return Promise.resolve(json.tid);
        }).catch(err => {
            logger.logInfo('(mdbModules.schedule.scheduleTrip) Could not save trip schedule
' + err);
            return Promise.reject(buildErrorMessage(err));
        });
    },

    /**
     * Get the scheduled trips of a single user
     * @param sid
     * @returns {Promise}
     */
    getScheduledTrips: function (sid) {

        return TripSchedule.find({sid: sid}, {__v: false, _id: false}).lean().then(items => {
            logger.logInfo(`(mdbModules.schedule.getScheduledTrips) Successfully retrieved saved scheduled trips for ${sid}`);
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.getScheduledTrips) Could not retrieve scheduled trips for ${sid}
` + err);
            return Promise.reject('Could not retrieve scheduled trips');
        });
    },

    /**
     * Get a specific trip schedule with a tid
     * @param tid
     * @returns {Promise}
     */
    getScheduledTrip: function (tid) {

        return TripSchedule.find({tid: tid}, {__v: false, _id: false}).lean().then(item => {
            logger.logInfo(`(mdbModules.schedule.getScheduledTrip) Successfully retrieved saved scheduled trip with TID ${tid}`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.getScheduledTrip) Could not retrieve scheduled trip with TID ${tid}
` + err);
            return Promise.reject('Could not retrieve the specified scheduled trip');
        });
    },

    /**
     * Save a lift schedule
     * @param sid
     * @param json
     * @returns {Promise}
     */
    scheduleLift: function(sid, json) {

        json.sid = sid;
        json.lid = shortid.generate();

        return LiftSchedule(json).save().then(() => {
            logger.logInfo(`(mdbModules.schedule.scheduleLift) Successfully saved scheduled lift ${json.lid}`);
            return Promise.resolve(json.lid);
        }).catch(err => {
            logger.logInfo('(mdbModules.schedule.scheduleLift) Could not save lift schedule
' + err);
            return Promise.reject('Could not save lift schedule');
        });
    },

    /**
     * Get the scheduled lifts of a single user
     * @param sid
     */
    getScheduledLifts: function(sid) {

        return LiftSchedule.find({sid: sid}, {__v: false, _id: false}).lean().then(items => {
            logger.logInfo(`(mdbModules.schedule.getScheduledLifts) Successfully retrieved saved scheduled lifts for ${sid}`);
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.getScheduledLifts) Could not retrieve scheduled lifts for ${sid}
` + err);
            return Promise.reject('Could not retrieve scheduled lifts');
        });
    },

    /**
     * Get a specific scheduled lift with a lid
     * @param lid
     * @returns {Promise}
     */
    getScheduledLift: function (lid) {

        return LiftSchedule.find({lid: lid}, {__v: false, _id: false}, {multi: false}).lean().then(item => {
            logger.logInfo(`(mdbModules.schedule.getScheduledLift) Successfully retrieved saved scheduled lift with LID ${lid}`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.getScheduledLift) Could not retrieve scheduled lift with LID ${lid}
` + err);
            return Promise.reject('Could not retrieve the specified scheduled lift');
        });

    },

    /**
     * Get multiple lifts at once
     * @param lids An array of lift ids that must be retrieved
     */
    getMultiScheduledLifts: function (lids) {

        return LiftSchedule.find({lid: {$in: lids}}, {_id: false}).lean().then(items => {
            logger.logInfo(`(mdbModules.schedule.getMultiScheduledLifts) Successfully retrieved saved scheduled lifts with LIDs ${lids}`);
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.getMultiScheduledLifts) Could not retrieve the scheduled lifts with LIDs ${lids}
` + err);
            return Promise.reject('Could not retrieve the lifts with the specified lids');
        });
    },

    /**
     * Get all trips that have been scheduled
     * @param singleDate Find only trips with singleDate set to this value (true/false)
     * @returns {Promise}
     */
    getAllScheduledTrips: function (singleDate = false) {

        return TripSchedule.find({singleDate: singleDate}, {_id: false, __v: false}).lean().then(items => {
            logger.logInfo(`(mdbModules.schedule.getAllScheduledTrips) Successfully retrieved all scheduled trips. SingleDate = ${singleDate}`);
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.getAllScheduledTrips) Could not retrieve all scheduled trips. SingleDate = ${singleDate}
` + err);
            return Promise.reject('Could not retrieve all scheduled trips');
        });
    },

    /**
     * Get all the lifts that have been scheduled
     * @param singleDate Find only lifts with singleDate set to this value (true/false)
     * @returns {Promise}
     */
    getAllScheduledLifts: function (singleDate = false) {

        return LiftSchedule.find({singleDate: singleDate}, {_id: false, __v: false}).lean().then(items => {
            logger.logInfo(`(mdbModules.schedule.getAllScheduledLifts) Successfully retrieved all scheduled lifts. SingleDate = ${singleDate}`);
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.getAllScheduledLifts) Could not retrieve all scheduled lifts. SingleDate = ${singleDate}
` + err);
            return Promise.reject('Could not retrieve all scheduled lifts');
        });
    },

    /**
     * Add the scheduled lift ID to the scheduled trip in the singleDatePassengers. Also decrements the number of open seats
     * @param tid
     * @param route The new route for when the new passenger is added
     * @param lid
     */
    addLiftToTripSingleDate: function (tid, route, lid) {

        return TripSchedule.update({tid: tid}, {$push: {singleDatePassengers: lid}, $set: {singleDateRoute: route}, $inc: {singleDateOpenSeats: -1}}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.schedule.addScheduledLiftToScheduledTripSingleDate) Successfully added the lift ${lid} to the trip schedule ${tid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.addScheduledLiftToScheduledTripSingleDate) Could not add the lift ${lid} to the trip schedule ${tid}
` + err);
            return Promise.reject('Could not add the lift to the trip schedule');
        });
    },

    /**
     * Add multiple passengers to the scheduled trip. Also decreases the number of open seats
     * @param tid
     * @param route
     * @param lids
     */
    addMultiLiftsToTripSingleDate: function (tid, route, lids) {

        let seats = lids.length * -1;

        return TripSchedule.update({tid: tid}, {$pushAll: {singleDatePassengers: lids}, $set: {singleDateRoute: route}, $inc: {singleDateOpenSeats: seats}}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.schedule.addMultiLiftsToTripSingleDate) Successfully added lift array ${lids} to the trip schedule ${tid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.addMultiLiftsToTripSingleDate) Could not add the lift array ${lids} to the trip schedule ${tid}
` + err);
            return Promise.reject('Could not add the lift to the trip schedule');
        });
    },

    /**
     * Add the scheduled trip ID to the scheduled lift in the singleDateDriver
     * @param tid
     * @param lid
     */
    setLiftSingleDateDriver: function (tid, lid) {

        return LiftSchedule.update({lid: lid}, {$set: {singleDateDriver: tid}}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.schedule.setLiftSingleDateDriver) Successfully added the trip ${tid} to the lift schedule ${lid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.setLiftSingleDateDriver) Could not add the trip ${tid} to the lift schedule ${lid}
` + err);
            return Promise.reject('Could not add the trip to the lift schedule');
        });
    },

    /**
     * Add the scheduled lift ID to the scheduled trip in the appropriate weekRepeat day
     * @param tid
     * @param route
     * @param lid
     * @param day
     */
    addLiftToTripWeekRepeat: function (tid, route, lid, day) {

        let place = 'passengers.' + day;
        let place2 = 'weekRepeatRoute.' + day;
        let place3 = 'weekRepeatOpenSeats.' + day;

        let updatePush = {};
        updatePush[place] = lid;
        let updateSet = {};
        updateSet[place2] = route;
        let updateInc = {};
        updateInc[place3] = -1;

        return TripSchedule.update({tid: tid}, {$push: updatePush, $set: updateSet, $inc: updateInc}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.schedule.addScheduledLiftToScheduledTripWeekRepeat) Successfully added the lift ${lid} to the trip schedule ${tid} for ${day}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.addScheduledLiftToScheduledTripWeekRepeat) Could not add the lift ${lid} to the trip schedule ${tid} for ${day}
` + err);
            return Promise.reject('Could not add the lift to the trip schedule for the specified day');
        });

    },

    /**
     * Add multiple scheduled lift ID to the scheduled trip in the appropriate weekRepeat day
     * @param tid
     * @param day_lid
     */
    addLiftsToTripWeekRepeat: function (tid, day_lid) {

        let update = {};

        // Go through all the days and build the update query
        // day_lid has format {monday: [[lid, sid]], tuesday: [[lid, sid], [lid, sid]], ....}. See script.weeklySearchLifts assigns variable
        for (let i = 0; i < Object.keys(day_lid).length; i = i + 1) {
            let day = Object.keys(day_lid)[i];
            if (day_lid[day].length > 0) {
                let index = 'passengers.' + day;
                let values = [];

                for (let j = 0; j < day_lid[day].length; j = j + 1) {
                    values.push(day_lid[day][j][0]);
                }

                update[index] = {
                    $each: values
                }
            }
        }

        return TripSchedule.update({tid: tid}, {$push: update}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.schedule.addScheduledLiftsToScheduledTripWeekRepeat) Successfully added the lifts to the trip schedule ${tid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.addScheduledLiftsToScheduledTripWeekRepeat) Could not add the lifts to the trip schedule ${tid}
` + err);
            return Promise.reject('Could not add the lift to the trips schedule');
        });
    },

    /**
     * Add the scheduled trip ID to the scheduled lift in the appropriate weekRepeat day
     * @param tid
     * @param lid
     * @param day
     */
    setLiftWeekRepeatDriver: function (tid, lid, day) {

        let place = 'driver.' + day;
        let update = {};
        update[place] = tid;

        return LiftSchedule.update({lid: lid}, {$set: update}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.schedule.setLiftWeekRepeatDriver) Successfully added the trip ${tid} to the lift schedule ${lid} for ${day}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.setLiftWeekRepeatDriver) Could not add the trip ${tid} to the lift schedule ${lid} for ${day}
` + err);
            return Promise.reject('Could not add the trip to the lift schedule for the specified day');
        });
    },

    /**
     * Remove the specified scheduled trip. The removed document is also returned
     * @param tid
     * @returns {Promise.<T>|Promise}
     */
    removeScheduledTrip: function (tid) {

        return TripSchedule.findOneAndRemove({tid: tid}).lean().then(item => {
            logger.logInfo(`(mdbModules.schedule.removeScheduledTrip) Successfully removed trip ${tid}`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.removeScheduledTrip) Could not remove trip ${tid}
` + err);
            return Promise.reject('Could not remove the trip');
        });
    },

    /**
     * Remove the specified scheduled lift. The removed document is also returned
     * @param lid
     * @returns {Promise.<T>|Promise}
     */
    removeScheduledLift: function (lid) {

        return LiftSchedule.findOneAndRemove({lid: lid}).lean().then(item => {
            logger.logInfo(`(mdbModules.schedule.removeScheduledLift) Successfully removed lift ${lid}`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.removeScheduledLift) Could not remove lift ${lid}
` + err);
            return Promise.reject('Could not remove the lift');
        });
    },

    /**
     * Remove the specified singleDate passenger from the trip. Also increment number of open seats
     * @param tid
     * @param lid
     * @returns {Promise.<T>|Promise}
     */
    removeSingleDatePassenger: function (tid, lid) {

        return TripSchedule.updateOne({tid: tid}, {$pull: {singleDatePassengers: lid}, $inc: {singleDateOpenSeats: 1}}).lean().then(() => {
            logger.logInfo(`(mdbModules.schedule.removeSingleDatePassenger) Successfully marked user ${tid} as being pickup on trip ${tid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.removeSingleDatePassenger) Could not marked user ${tid} as being pickup on trip ${tid}
${err}`);
            return Promise.reject('Could not marked user as being picked up on trip');
        });
    },

    /**
     * Remove the specified weekly passenger from the trip on a specific day
     * @param tid
     * @param lid
     * @param day
     * @returns {Promise.<T>|Promise}
     */
    removeWeeklyPassenger: function (tid, lid, day) {

        let place = 'passengers.' + day;
        let place2 = 'weekRepeatOpenSeats.' + day;
        let update = {};
        update[place] = lid;
        let incUpdate = {};
        incUpdate[place2] = 1;

        return TripSchedule.updateOne({tid: tid}, {$pull: update, $inc: incUpdate}).lean().then(() => {
            logger.logInfo(`(mdbModules.schedule.removeWeeklyPassenger) Successfully marked user ${tid} as being pickup on trip ${tid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.removeWeeklyPassenger) Could not marked user ${tid} as being pickup on trip ${tid}
${err}`);
            return Promise.reject('Could not marked user as being picked up on trip');
        });
    },

    /**
     * Set the route of a singleDate trip
     * @param tid
     * @param route
     * @returns {Promise.<T>|Promise}
     */
    setScheduledTripSingleDateRoute: function (tid, route) {

        return TripSchedule.updateOne({tid: tid}, {$set: {singleDateRoute: route}}).lean().then(() => {
            logger.logInfo(`(mdbModules.schedule.setScheduledTripSingleDateRoute) Successfully set route for trip ${tid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.setScheduledTripSingleDateRoute) Could not set route for trip ${tid}
${err}`);
            return Promise.reject('Could not set route for trip');
        });
    },

    /**
     * Set the routes of each day of a weekly trip
     * @param tid
     * @param route
     * @returns {Promise.<T>|Promise}
     */
    setScheduledTripWeeklyRoute: function (tid, route) {

        return TripSchedule.updateOne({tid: tid}, {$set: {weekRepeatRoute: route}}).lean().then(() => {
            logger.logInfo(`(mdbModules.schedule.setScheduledTripWeeklyRoute) Successfully set route for trip ${tid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.setScheduledTripWeeklyRoute) Could not set route for trip ${tid}
${err}`);
            return Promise.reject('Could not set route for trip');
        });
    },

    /**
     * Get all TIDs of trips that is using the specified vehicle
     * @param vid
     * @returns {Promise.<T>|Promise}
     */
    getAllScheduledTripsWithVehicle: function (vid) {

        return TripSchedule.find({vid: vid}, 'tid').lean().then(items => {
            logger.logInfo(`(mdbModules.schedule.getAllScheduledTripsWithVehicle) Successfully retrieved all trips that use vehicle ${vid}`);
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.schedule.getAllScheduledTripsWithVehicle) Could not retrieve all trips that use vehicle ${vid}
${err}`);
            return Promise.reject('Could not retrieve all trips that use vehicle');
        });
    }
};
