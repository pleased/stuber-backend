/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

let consts = require('../../consts.js');

// ----- Models -----
let Inbox = require('../../models/inbox.js').Inbox;

module.exports = {

    /**
     * Add a message to the user's inbox
     * @param sid
     * @param message
     * @param type
     */
    addMessage: function (sid, message, type=consts.MESSAGE_TYPES.STANDARD) {

        let timestamp = (new Date()).getTime();
        return Inbox.update({sid: sid}, {$push: {messages: {timestamp: timestamp, text: message, type: type}}}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.inbox.addMessage) Successfully added message to user ${sid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.inbox.addMessage) Could not add message to user ${sid}
${err}`);
            return Promise.reject('Could not add message to user\'s inbox');
        });
    },

    /**
     * Get all the messages from the user's inbox after which the inbox is cleared
     * @param sid
     */
    getMessages: function (sid) {

        return Inbox.findOneAndUpdate({sid: sid}, {$set: {messages: []}}, {new: false}).lean().then(doc => {
            logger.logInfo(`(mdbModules.inbox.getMessages) Successfully retrieved inbox of user ${sid}`);
            return Promise.resolve(doc.messages);
        }).catch(err => {
            logger.logInfo(`(mdbModules.inbox.getMessages) Could not retrieve inbox of user ${sid}
${err}`);
            return Promise.reject('Could not retrieve user\' messages');
        });
    },

    /**
     * Delete an inbox
     * @param sid
     */
    removeInbox: function (sid) {

        return Inbox.remove({sid: sid}).then(() => {
            logger.logInfo(`(mdbModules.inbox.removeInbox) Successfully deleted inbox of user ${sid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.inbox.removeInbox) Could not delete inbox of user ${sid}
${err}`);
            return Promise.reject('Could not delete the inbox of user');
        });
    }
};
