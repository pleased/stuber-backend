/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

// ----- Models -----
let WaitingList = require('../../models/waitingList.js').WaitingList;

module.exports = {

    /**
     * Set the driver the passenger might be assigned to
     * @param sid
     * @param aid
     */
    addDriverToPassenger: function (sid, aid) {

        return WaitingList.update({sid: sid}, {$set: {aid: aid}}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.waitingList.addDriverToPassenger) Successfully set the driver for passenger`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.waitingList.addDriverToPassenger) Could not set the driver ${aid} for the passenger ${sid} in the waiting list
${err}`);
            return Promise.reject(`Could not set the driver ${aid} for the passenger ${sid} in the waiting list`);
        });
    },

    /**
     * Add a passenger to the waiting list
     * @param sid
     * @param json
     * @param possibles
     */
    addPassenger: function (sid, json, possibles) {

        let drivers = [];
        for (let i = 0; i < possibles.length; i = i + 1) {
            let driver = possibles[i][0];
            let route = possibles[i][1];

            let newEntry = {
                aid: driver.aid,
                route: route
            };

            drivers.push(newEntry);
        }

        json.sid = sid;
        json.drivers = drivers;

        return WaitingList(json).save().then(() => {
            logger.logInfo(`(mdbModules.waitingList.addPassenger) Successfully added passenger`);
            return Promise.resolve('Successfully added passenger to waiting list');
        }).catch(err => {
            logger.logInfo(`(mdbModules.waitingList.addPassenger) Could not add passenger ${sid}
${err}`);
            return Promise.reject('Could not add passenger to waiting list');
        });
    },

    /**
     * Retrieve passenger from waiting list and remove the record from the list
     * @param sid
     */
    getAndRemovePassenger: function (sid) {

        return WaitingList.findOneAndRemove({sid: sid}).lean().then(item => {
            logger.logInfo(`(mdbModules.waitingList.getAndRemovePassenger) Successfully retrieved and removed passenger`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.waitingList.getAndRemovePassenger) Could not retrieve and remove passenger ${sid}
${err}`);
            return Promise.reject('Could not retrieve the passenger from waiting list');
        });
    },

    /**
     * Remove passenger from waiting list
     * @param sid
     */
    removePassenger: function (sid) {

        return WaitingList.remove({sid: sid}).then(() => {
            logger.logInfo(`(mdbModules.waitingList.removePassenger) Successfully removed passenger`);
            return Promise.resolve('Successfully removed passenger from waiting list');
        }).catch(err => {
            logger.logInfo(`(mdbModules.waitingList.removePassenger) Could not remove passenger ${sid}
${err}`);
            return Promise.reject('Could not remove the passenger from waiting list');
        });
    },

    /**
     * Check if the aid is in the list of possible drivers that this user has been assigned to
     * @param sid The user
     * @param aid The aid to check for
     */
    checkAidInDrivers: function (sid, aid) {

        return WaitingList.findOne({sid: sid}).lean().then(item => {
            logger.logInfo(`(mdbModules.waitingList.checkAidInDrivers) Successfully checked if driver in passenger's list`);
            let driver = item.drivers.filter(item => item.aid === aid);
            if (driver.length === 0) {
                return Promise.resolve(false);
            } else {
                return Promise.resolve(true);
            }
        }).catch(err => {
            logger.logInfo(`(mdbModules.waitingList.checkAidInDrivers) Could not check if driver ${aid} in passenger ${sid}'s list
${err}`);
            return Promise.reject('Could not check if the driver is in the passenger\'s list');
        });
    },

    /**
     * Check that the user has not already made a selection on the driver that they want
     * @param sid The user
     */
    checkSelectedDriverEmpty: function (sid) {

        return WaitingList.findOne({sid: sid}, 'aid').lean().then(item => {
            logger.logInfo(`(mdbModules.waitingList.checkSelectedDriverEmpty) Successfully checked if that passenger does not have a driver assigned`);
            if (item.aid === '') {
                return Promise.resolve(true);
            } else {
                return Promise.resolve(false);
            }
        }).catch(err => {
            logger.logInfo(`(mdbModules.waitingList.checkSelectedDriverEmpty) Could not check if passenger ${sid} already has a driver assigned
${err}`);
            return Promise.reject('Could not check if the user already selected a driver')
        });
    },

    /**
     * Get the users that are waiting for a answer from the driver
     * @param aid
     */
    getDriverWaitingList: function (aid) {

        return WaitingList.findOne({aid: aid}, {_id: false, __v: false, drivers: false, aid: false}).lean().then(item => {
            logger.logInfo(`(mdbModules.waitingList.getDriverWaitingList) Successfully retrieved the waiting list for the driver`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.waitingList.getDriverWaitingList) Could not retrieve the waiting list for driver ${aid}
${err}`);
            return Promise.reject('Could not retrieve the the waiting list for the driver')
        });
    }
};
