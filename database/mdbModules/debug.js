/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

// ----- Models -----
let User = require('../../models/user.js').User;
let History = require('../../models/user.js').History;

let Inbox = require('../../models/inbox.js').Inbox;
let Review = require('../../models/review.js').Review;

let Vehicle = require('../../models/vehicle.js').Vehicle;

let Passenger = require('../../models/passenger').Passenger;
let Active = require('../../models/active.js').Active;

let TripSchedule = require('../../models/tripSchedule.js').TripSchedule;
let LiftSchedule = require('../../models/liftSchedule.js').LiftSchedule;

module.exports = {

    /**
     *
     * @param callback
     */
    getAllUsers: function (callback) {

        User.find({}, {'_id': false, '__v': false, 'gid': false}).then(function (items) {
            callback(false, 'Successfully retrieved all users', items);
        }, function (err) {
            callback(true, 'Could not retrieve all users');
        });
    }
};
