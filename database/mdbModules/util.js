/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

// ----- Models -----
let User = require('../../models/user.js').User;
let History = require('../../models/user.js').History;

let Inbox = require('../../models/inbox.js').Inbox;
let Review = require('../../models/review.js').Review;

let Vehicle = require('../../models/vehicle.js').Vehicle;

let Passenger = require('../../models/passenger').Passenger;
let Active = require('../../models/active.js').Active;

let TripSchedule = require('../../models/tripSchedule.js').TripSchedule;
let LiftSchedule = require('../../models/liftSchedule.js').LiftSchedule;

module.exports = {

    /**
     * Build an error message from an error returned by a failed Mongo query
     * @param err
     * @returns {string}
     */
    buildErrorMessage: function (err) {
        let error = '';

        // Build error message with all errors
        if (err.errors) {
            for (let errMessage in err.errors) {
                error += (err.errors[errMessage].message + '
');
            }
        } else if (err.errmsg) {
            error = err.errmsg;
        }

        return error;
    },

    /**
     * Check if a Active entry exists with condition q. If the active document exists then resolve with the aid corresponding to it. Otherwise resolve false
     * @param q - A Mongo query
     * @returns {*}
     */
    checkActiveExists: function (q) {

        logger.logInfo('Checking if Active document exists for ' + JSON.stringify(q));

        return Active.find(q, 'aid', {_id: false, multi: false}).lean().then(item => {
            if (item && item.length === 0) {
                logger.logInfo('Active document for ' + JSON.stringify(q) + ' does not exist');
                return Promise.resolve(false);
            } else {
                logger.logInfo('Active document for ' + JSON.stringify(q) + ' does exist');
                return Promise.resolve(item[0].aid);
            }
        }).catch(err => {
            return Promise.reject('Could not retrieve the active document');
        });
    },

    /**
     * Check if a User entry exists with condition q
     * @param q - A Mongo query
     * @returns {*}
     */
    checkUserExists: function (q) {

        logger.logInfo('Checking if User document exists for ' + JSON.stringify(q));

        return User.count(q).then(count => {
            if (count === 0) {
                logger.logInfo('User document for ' + JSON.stringify(q) + ' does not exist');
                return Promise.resolve(false);
            } else {
                logger.logInfo('User document for ' + JSON.stringify(q) + ' does exist');
                return Promise.resolve(true);
            }
        }).catch(err => {
            return Promise.reject('Could not retrieve the user document');
        });
    },

    /**
     * Check if a Vehicle entry exists with condition q
     * @param q - A Mongo query
     * @returns {*}
     */
    checkVehicleExists: function (q) {

        logger.logInfo('Checking if Vehicle document exists for ' + JSON.stringify(q));

        return Vehicle.count(q).then(count => {
            if (count === 0) {
                logger.logInfo('Vehicle document for ' + JSON.stringify(q) + ' does not exist');
                return Promise.resolve(false);
            } else {
                logger.logInfo('Vehicle document for ' + JSON.stringify(q) + ' does exist');
                return Promise.resolve(true);
            }
        }).catch(err => {
            return Promise.reject('Could not retrieve the vehicle document');
        });
    }
};
