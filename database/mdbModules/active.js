/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let shortid = require("shortid");

let logger = require('../../logger.js');

let buildErrorMessage = require('./util.js').buildErrorMessage;

// ----- Models -----
let Active = require('../../models/active.js').Active;
let Passenger = require('../../models/passenger').Passenger;

module.exports = {

    getDriver: function (aid) {

        return Active.findOne({aid: aid}, 'sid', {_id: false}).lean().then(item => {
            logger.logInfo(`(active.getDriver) Successfully retrieved active ${aid} driver\'s SID`);
            return Promise.resolve(item.sid);
        }).catch(err => {
            logger.logInfo(`(active.getDriver) Could not retrieve active ${aid} driver\'s SID
${err}`);
            return Promise.reject('Could not retrieve active driver\'s SID');
        });
    },

    /**
     *
     * @param sid
     * @param lat
     * @param lon
     */
    setLatLong: function (sid, lat, lon) {

        let currLoc = {
            lat: lat,
            lon: lon
        };

        return Active.update({sid: sid}, {currLoc: currLoc}, {multi: false}).then(() => {
            logger.logInfo('(active.setLatLong) Successfully updated driver\'s position');
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.setLatLong) Could not set driver ${sid}'s position
${err}`);
            return Promise.reject('Could not update driver\'s position');
        });
    },

    /**
     *
     * @param sid
     */
    getLatLong: function (sid) {

        return Active.findOne({sid: sid}, 'currLoc').lean().then(doc => {
            logger.logInfo('(active.getLatLong) Successfully retrieved driver\'s position');
            return Promise.resolve(doc.currLoc);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getLatLong) Could not retrieve driver ${sid}'s position
${err}`);
            return Promise.reject('Could not retrieve driver\'s position');
        });
    },

    /**
     * Get the SID and current location of all current active trips
     */
    getAllLatLong: function () {

        return Active.find({}).select('-_id sid currLoc').lean().then(docs => {
            logger.logInfo('(active.getAllLatLong) Successfully retrieved all the positions of drivers');
            return Promise.resolve(docs);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getAllLatLong) Could not retrieve all positions of drivers
${err}`);
            return Promise.reject('Could not retrieve all the positions of drivers');
        });
    },

    /**
     *
     * @param sid
     * @param json
     */
    setTrip: function (sid, json) {

        json.sid = sid;
        json.aid = shortid.generate();
        json.route = json.destination;

        return Active(json).save().then(() => {
            logger.logInfo(`(mdbModules.active.setTrip) Successfully saved trip ${json.aid} for driver ${sid}`);
            return Promise.resolve(json.aid);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.setTrip) Could not save trip for driver ${sid}
${err}`);
            return Promise.reject(buildErrorMessage(err));
        });
    },

    /**
     * Create a active from a scheduled trips settings
     * @param trip
     * @param passengers
     */
    activateScheduledTrip: function (trip, passengers) {

        // Create the passengers
        let Passengers = [];
        for (let i = 0; i < passengers.length; i = i + 1) {
            Passengers.push(new Passenger({
                sid: passengers[i].sid,

                origin: passengers[i].origin,
                destination: passengers[i].destination,

                startTime: passengers[i].startTime,
                endTime: passengers[i].endTime
            }));
        }

        trip.pickups = Passengers;
        trip.aid = shortid.generate();

        return Active(trip).save().then(() => {
            logger.logInfo(`(mdbModules.active.activateScheduledTrip) Successfully activated trip ${trip.aid} for driver ${trip.sid}`);
            return Promise.resolve(trip.aid);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.activateScheduledTrip) Could not activate trip for driver ${trip.sid}
${err}`);
            return Promise.reject(buildErrorMessage(err));
        });
    },

    /**
     * Get teh trip belonging to a user
     * @param sid
     * @returns {Promise}
     */
    getTripWithSid: function (sid) {

        return Active.find({sid: sid}, {_id: false, __v: false}, {multi: false}).lean().then(item => {
            logger.logInfo(`(mdbModules.active.getTrip) Successfully retrieved driver ${sid}'s trip`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getTrip) Could not retrieve driver ${sid}'s trip
${err}`);
            return Promise.reject('Could not retrieve trip data');
        });
    },

    /**
     * Get teh trip belonging to a user
     * @param aid
     * @returns {Promise}
     */
    getTripWithAid: function (aid) {

        return Active.find({aid: aid}, {_id: false, __v: false}, {multi: false}).then(item => {
            logger.logDebug(`(mdbModules.active.getTrip) Successfully retrieved trip ${aid}`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getTrip) Could not retrieve trip ${aid}
${err}`);
            return Promise.reject('Could not retrieve trip data');
        });
    },

    /**
     * Checks if a trip has any pickups or passengers left in it
     * @param sid
     * @returns {Promise}
     */
    tripHasLifts: function (sid) {

        return Active.find({sid: sid}, 'passengers pickups', {multi: false}).lean().then(item => {
            logger.logInfo(`(mdbModules.active.tripHasLifts) Successfully retrieved driver ${sid}'s trip`);

            if (item[0].passengers.length > 0 || item[0].pickups.length > 0) {
                return Promise.resolve(true);
            } else {
                return Promise.resolve(false);
            }
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.tripHasLifts) Could not retrieve driver ${sid}'s trip
${err}`);
            return Promise.reject('Could not retrieve trip data');
        });
    },

    /**
     * Remove a trip
     * @param sid
     */
    removeTrip: function (sid) {

        return Active.remove({sid: sid}).then(() => {
            logger.logInfo(`(mdbModules.active.tripHasLifts) Successfully removed trip for driver ${sid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.removeLift) Could not remove trip for driver ${sid}
${err}`);
            return Promise.reject('Could not remove trip');
        });
    },

    /**
     * Adds a user as a pickup to the specified trip. Also decrements the number of open seats the trip has
     * @param sid The ID of the passenger
     * @param aid The ID of the active document to add the passenger to
     * @param route The route that the driver will take when this passenger is added
     * @param json The settings of the lift the passenger wants
     */
    addPickupToLift: function (sid, aid, route, json) {

        let pass = new Passenger({
            sid: sid,

            destination: json.destination,
            origin: json.origin,

            // Only the time portion of the date will be used
            startTime: json.startTime,
            endTime: json.endTime
        });

        return Active.update({aid: aid}, {$push: {pickups: pass}, $set: {route: route}, $inc: {openSeats: -1}}, {multi: false}).then(() => {
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.setLift) Could not add user to trip
${err}`);
            return Promise.reject('Could not add user to trip');
        });
    },

    /**
     * Get a lift where a user is in the pickups or passengers list
     * @param sid
     * @returns {Promise}
     */
    getLift: function (sid) {

        return Active.find({$or: [{'passengers.sid': {$in: [sid]}}, {'pickups.sid': {$in: [sid]}}]}, {
            _id: false,
            __v: false
        }, {multi: false}).lean().then(item => {
            logger.logInfo(`(mdbModules.active.getLift) Successfully retrieved the trip user ${sid} is on`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getLift) Could not retrieved the trip user ${sid} is on
${err}`);
            return Promise.reject();
        });
    },

    /**
     * Get a lift where a user is in the pickups list
     * @param sid
     */
    getLiftPickup: function (sid) {

        return Active.find({'pickups.sid': {$in: [sid]}}, {_id: false, __v: false}, {multi: false}).lean().then(item => {
            logger.logInfo(`(mdbModules.active.getLiftPickup) Successfully retrieved the trip user ${sid} is on as a pickup still`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getLiftPickup) Could not retrieve the trip user ${sid} is on as a pickup still
${err}`);
            return Promise.reject('Could not retrieve trip data where user is a pickup');
        });
    },

    /**
     * Get a lift where the user is in the passengers list
     * @param sid
     */
    getLiftPassenger: function (sid) {

        return Active.find({'passengers.sid': {$in: [sid]}}, {_id: false, __v: false}, {multi: false}).lean().then(item => {
            logger.logInfo(`(mdbModules.active.getLiftPassenger) Successfully retrieved the trip user ${sid} is on as a passenger still`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getLiftPassenger) Could not retrieve the trip user ${sid} is on as a passenger still
${err}`);
            return Promise.reject('Could not retrieve trip data where user is a passenger');
        });
    },

    /**
     * Remove a passenger from a trip - The lift has been completed. Also increments number of openSeats of the trip
     * First element from route is also removed (this is the drop-off loc)
     * @param sid The sid of the passenger to remove
     * @param aid - The aid of the trip that the passenger should be removed from
     */
    removeLift: function (sid, aid) {

        return Active.update({aid: aid}, {$pull: {passengers: {sid: sid}}, $pop: {route: -1}, $inc: {openSeats: 1}}, {multi: false}).lean().then(() => {
            logger.logInfo(`(mdbModules.active.removeLift) Successfully removed passenger ${sid} from trip ${aid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.removeLift) Could not remove passenger ${sid} from trip ${aid}
${err}`);
            return Promise.reject('Could not remove passenger from trip');
        });
    },

    /**
     * Move a passenger from the pickup list to the passenger list.
     * First element from route is also removed (this is the pickup loc)
     * @param sid The user to move
     * @param aid The trip to update
     * @returns {Promise}
     */
    pickupLift: function (sid, aid) {

        return Active.findOneAndUpdate({aid: aid}, {$pull: {pickups: {sid: sid}}, $pop: {route: -1}}, {new: false}).lean().then(doc => {
            let removed = doc.pickups.filter(function (item) {
                return [sid].indexOf(item.sid) !== -1;
            });

            return Promise.resolve(removed[0]);
        }).then(removed => {
            return Active.update({aid: aid}, {$push: {passengers: removed}}, {multi: false}).then(() => {
                logger.logInfo(`(mdbModules.active.pickupLift) Successfully marked user ${sid} as being pickup on trip ${aid}`);
                return Promise.resolve();
            });
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.pickupLift) Could not marked user ${sid} as being pickup on trip ${aid}
${err}`);
            return Promise.reject('Could not marked user as being picked up on trip');
        });
    },

    /**
     * Get all active trips
     * @returns {*|Promise}
     */
    getAllTrips: function () {

        return Active.find({}, {_id: false, __v: false}).lean().then(items => {
            logger.logInfo('(mdbModules.active.getAllTrips) Successfully retrieved all trips in the system');
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getAllTrips) Could not retrieve all trips in the system
${err}`);
            return Promise.reject('Could not retrieve all trips');
        });
    },

    /**
     * Get all active trips that have open seats
     * @returns {*|Promise}
     */
    getTripsWithOpenSeats: function () {

        return Active.find({openSeats: {$gt: 0}}, {_id: false, __v: false}).lean().then(items => {
            logger.logInfo('(mdbModules.active.getTripsWithOpenSeats) Successfully retrieved all trips that have open seats in the system');
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getTripsWithOpenSeats) Could not retrieve all trips that have open seats in the system
${err}`);
            return Promise.reject('Could not retrieve all trips');
        });
    },

    /**
     * Find the active trip which is using the specified vehicle
     * @param vid
     * @returns {Promise.<T>|Promise}
     */
    getTripWithVehicle: function (vid) {

        return Active.findOne({vid: vid}).lean().then(item => {
            logger.logInfo(`(mdbModules.active.getTripWithVehicle) Successfully retrieve active trip which is using vehicle ${vid}`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.active.getTripWithVehicle) Could not retrieve active trip which is using vehicle ${vid}
${err}`);
            return Promise.reject('Could not retrieve active trip which is using vehicle');
        });
    }
};
