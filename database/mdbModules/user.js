/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let shortid = require('shortid');

let logger = require('../../logger.js');
let buildErrorMessage = require("./util.js").buildErrorMessage;
// ----- Models -----
let User = require('../../models/user.js').User;
let History = require('../../models/user.js').History;
let Inbox = require("../../models/inbox.js").Inbox;

module.exports = {

    /**
     *
     * @param sid
     * @returns {*|Promise}
     */
    isUserDriver: function (sid) {

        return User.findOne({sid: sid}, 'isDriver').lean().then(item => {
            logger.logInfo(`(mdbModules.user.isUserDriver) Successfully retrieved user's driver status`);
            return Promise.resolve(item.isDriver);
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.isUserDriver) Could not retrieve user ${sid}'s driver status
${err}`);
            return Promise.reject('Could not retrieve user\'s driver status');
        });
    },


    setAsDriver: function (sid, set) {

        return User.update({sid: sid}, {isDriver: set}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.user.setAsDriver) Successfully updated user's driver status`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.setAsDriver) Could not update user ${sid}'s driver status
${err}`);
            return Promise.reject('Could not update user\'s driver status')
        });
    },

    /**
     *
     * @param sid
     * @param image_name The name of the image
     */
    addUserImage: function (sid, image_name) {

        return User.update({sid: sid}, {picture: image_name}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.user.addUserImage) Successfully updated user's profile picture`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.addUserImage) Could not update user ${sid}'s profile picture
${err}`);
            return Promise.reject('Could not update user profile picture')
        });
    },


    /**
     *
     * @param json
     */
    addUser: function (json) {

        // Generate a Stuber ID for the user
        json.sid = shortid.generate();

        return User(json).save().then(() => {
            return Inbox({sid: json.sid}).save();
        }).then(() => {
            logger.logInfo(`(mdbModules.user.addUser) Successfully created user with inbox`);
            return Promise.resolve(json.sid);
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.addUser) Could not create user with inbox
${err}`);
            return Promise.reject(buildErrorMessage(err));
        });
    },

    /**
     *
     * @param sid
     */
    getUser: function (sid) {

        return User.findOne({sid: sid}, {'_id': false, '__v': false, 'gid': false}).lean().then(user => {
            logger.logInfo(`(mdbModules.user.getUser) Successfully retrieved user`);
            return Promise.resolve(user);
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.getUser) Could not retrieve user ${sid}
${err}`);
            return Promise.reject('Could not retrieve the user');
        });
    },

    /**
     *
     * @param gid
     */
    loginUser: function (gid) {

        return User.find({gid: gid}, 'sid', {multi: false}).lean().then(item => {
            if (item.length === 0) {
                logger.logInfo(`(mdbModules.user.loginUser) Could not get SID for logging in user ${gid}`);
                return Promise.reject('Could not login user');
            } else {
                logger.logInfo(`(mdbModules.user.loginUser) Successfully got SID for logging in user ${gid}`);
                return Promise.resolve(item[0].sid);
            }
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.loginUser) Could not get SID for logging in user ${gid}
${err}`);
            reject('Could not login user');
        });
    },

    /**
     *
     * @param sid
     * @returns {Promise}
     */
    getUserImage: function (sid) {

        return User.find({sid: sid}, 'picture', {multi: false}).lean().then(item => {
            if (item.length === 0) {
                logger.logInfo(`(mdbModules.user.getUserImage) Could not get image name of user ${sid}`);
                return Promise.reject('Could not retrieve the image name for the user');
            } else {
                logger.logInfo(`(mdbModules.user.getUserImage) Successfully retrieved image name of user ${sid}`);
                return Promise.resolve(item[0].picture);
            }
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.getUserImage) Could not get image name of user ${sid}
${err}`);
            return Promise.reject('Could not retrieve the image name for the user');
        });
    },

    /**
     * Add a trip setting to the history
     * @param sid
     * @param json
     */
    addTripHistory: function (sid, json) {

        let hist = new History({
            vid: json.vid,

            origin: json.origin,
            destination: json.destination,

            startTime: json.startTime,
            endTime: json.endTime,

            openSeats: json.openSeats,

            timestamp: (new Date()).getTime()
        });

        return User.update({sid: sid}, {$push: {tripHistory: hist}}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.user.addTripHistory) Successfully added trip history for user ${sid}`);
            return Promise.resolve();
        }).catch(err => {
            // Not reject as if this fails it really is not that big of a deal. User only loses one history entry
            logger.logInfo(`(mdbModules.user.addTripHistory) Could not add trip history for user ${sid} [Still resolving]
${err}`);
            return Promise.resolve();
        });
    },

    /**
     * Get the specified number of trip history settings
     * @param sid
     * @param num
     */
    getTripHistory: function (sid, num) {

        return User.findOne({sid: sid}, 'tripHistory', {limit: num}).lean().then(items => {
            logger.logInfo(`(mdbModules.user.getTripHistory) Successfully retrieved user ${sid} trip history`);
            return Promise.resolve(items.tripHistory);
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.getTripHistory) Could not retrieve user ${sid} trip history
${err}`);
            return Promise.reject('Could not retrieve user\'s trip history');
        });
    },

    /**
     * Add a lift setting to the history
     * @param sid
     * @param json
     */
    addLiftHistory: function (sid, json) {

        let hist = new History({
            origin: json.origin,
            destination: json.destination,

            startTime: json.startTime,
            endTime: json.endTime,

            timestamp: (new Date()).getTime()
        });

        return User.update({sid: sid}, {$push: {liftHistory: hist}}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.user.addLiftHistory) Successfully added lift history for user ${sid}`);
            return Promise.resolve();
        }).catch(err => {
            // Not reject as if this fails it really is not that big of a deal. User only loses one history entry
            logger.logInfo(`(mdbModules.user.addLiftHistory) Could not add trip history for user ${sid} [Still resolving]
${err}`);
            return Promise.resolve();
        });
    },

    /**
     * Get the specified number of lift history settings
     * @param sid
     * @param num
     */
    getLiftHistory: function (sid, num) {

        return User.findOne({sid: sid}, 'liftHistory', {limit: num}).lean().then(items => {
            logger.logInfo(`(mdbModules.user.getLiftHistory) Successfully retrieved user ${sid} lift history`);
            return Promise.resolve(items.liftHistory);
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.getLiftHistory) Could not retrieve user ${sid} lift history
${err}`);
            return Promise.reject('Could not retrieve user\'s lift history');
        });
    },

    /**
     * Delete a user and return the deleted document
     * @param sid
     */
    removeUser: function (sid) {

        return User.findOneAndRemove({sid: sid}).then(item => {
            logger.logInfo(`(mdbModules.user.removeUser) Successfully deleted user ${sid}`);
            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.removeUser) Could not delete user ${sid}
${err}`);
            return Promise.reject('Could not delete user');
        });
    },

    /**
     * Update user details
     * @param sid
     * @param json
     */
    updateUser(sid, json) {

        return User.findOneAndUpdate({sid: sid}, json, {new: true}).then(item => {
            logger.logInfo(`(mdbModules.user.updateUser) Successfully updated use r${sid}'s details`);

            delete item._id;
            delete item.__v;
            delete item.gid;

            return Promise.resolve(item);
        }).catch(err => {
            logger.logInfo(`(mdbModules.user.updateUser) Could not update user ${sid}'s details
${err}`);
            return Promise.reject('Could not update the user\'s details');
        });
    }
};
