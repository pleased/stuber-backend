/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../../logger.js');

let buildErrorMessage = require("./util.js").buildErrorMessage;

// ----- Models -----
let Review = require('../../models/review.js').Review;

module.exports = {

    /**
     * Submit a review
     * @param json
     */
    addReview: function (json) {

        return Review(json).save().then(() => {
            logger.logInfo(`(mdbModules.reviews.addReview) Successfully submitted review for user ${json.reviewed}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.reviews.addReview) Could not submit review for user ${json.reviewed}
${err}`);
            return Promise.reject(buildErrorMessage(err));
        });
    },

    /**
     * Retrieved the specified number of reviews for a user
     * @param num
     * @param sid
     */
    getReviews: function (num, sid) {

        return Review.find({reviewed: sid}, {__v: false, _id: false}, {limit: num}).lean().then(items => {
            logger.logInfo(`(mdbModules.reviews.getReviews) Successfully retrieved the reviews for user ${sid}`);
            return Promise.resolve(items);
        }).catch(err => {
            logger.logInfo(`(mdbModules.reviews.getReviews) Could not retrieve the reviews for user ${sid}
${err}`);
            return Promise.reject('Could not retrieve the reviews');
        });
    },

    /**
     * Get the rating of a user. This is the average score of a user's review rating
     * @param sid
     */
    getUserRating: function (sid) {

        return Review.aggregate([
            {
                $match: {reviewed: sid}
            },
            {
                $group: {
                    _id: "$reviewed",
                    rating: {$avg: "$rating"}
                }
            }
        ]).then(item => {
            if (item.length === 0) {
                logger.logInfo(`(mdbModules.reviews.getUserRating) User ${sid} does not have rating yet. Default to 0.0`);
                return Promise.resolve(0.0);
            } else {
                logger.logInfo(`(mdbModules.reviews.getUserRating) Successfully retrieved user ${sid}'s rating`);
                return Promise.resolve(item[0].rating);
            }
        }).catch(err => {
            logger.logInfo(`(mdbModules.reviews.getUserRating) Could not retrieve the user ${sid}'s rating
${err}`);
            return Promise.reject('Could not retrieve user\'s rating');
        });
    }
};
