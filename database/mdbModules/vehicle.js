/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let shortid = require("shortid");

let logger = require('../../logger.js');

let buildErrorMessage = require("./util.js").buildErrorMessage;

// ----- Models -----
let Vehicle = require('../../models/vehicle.js').Vehicle;

module.exports = {

    /**
     *
     * @param sid
     * @param json The vehicle data to be added
     */
    addVehicle: function (sid, json) {

        delete json.sid;

        json.vid = shortid.generate();
        json.owners = [sid];

        return Vehicle(json).save().then(() => {
            logger.logInfo(`(mdbModules.vehicle.addVehicle) Successfully added vehicle ${json.vid} to the system`);
            return Promise.resolve(json.vid);
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.addVehicle) Could not add vehicle ${json.vid} to the system
${err}`);
            return Promise.reject(buildErrorMessage(err));
        });
    },

    /**
     * Get all vehicles who has gid as set as its owner
     * @param vid
     */
    getVehicle: function (vid) {

        return Vehicle.findOne({vid: vid}, {'_id': false, '__v': false}).lean().then(doc => {
            logger.logInfo(`(mdbModules.vehicle.getVehicle) Successfully retrieved vehicle`);
            return Promise.resolve(doc);
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.getVehicle) Could not retrieve vehicle ${vid}
` + err);
            return Promise.reject('Could not retrieve vehicle');
        });
    },

    /**
     *
     * @param sid
     */
    getVehiclesOfOwner: function (sid) {

        return Vehicle.find({'owners': {$in: [sid]}}, {'_id': false, '__v': false}).lean().then(docs => {
            logger.logInfo(`(mdbModules.vehicle.getVehiclesOfOwner) Successfully retrieved vehicles of user`);
            return Promise.resolve(docs);
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.getVehiclesOfOwner) Could not retrieve user ${sid}'s vehicles
${err}`);
            return Promise.reject('Could not retrieve user\'s vehicles');
        });
    },

    /**
     * Get the owners of the specified vehicle
     * @param vid
     * @returns {Promise.<T>|Promise}
     */
    getVehicleOwners: function (vid) {

        return Vehicle.find({vid: vid}, 'owners').lean().then(docs => {
            logger.logInfo(`(mdbModules.vehicle.getVehicleOwners) Successfully retrieved vehicle's owners`);
            return Promise.resolve(docs);
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.getVehicleOwners) Could not retrieve owners of vehicle ${vid}
${err}`);
            return Promise.reject(`Could not retrieve the owners of the vehicle`);
        });
    },

    /**
     * Add an owner to an already existing vehicle
     * @param vid
     * @param sid
     */
    addVehicleOwner: function (vid, sid) {

        // TODO: Check that the $push is correct - Just winged it
        return Vehicle.update({vid: vid}, {$push: {owners: sid}}, {multi: false}).then(() => {
            logger.logInfo(`(mdbModules.vehicle.addVehicleOwner) Successfully added user as vehicle owner`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.addVehicleOwner) Could not add user ${sid} as an owner of vehicle ${vid}
${err}`);
            return Promise.reject('Could not add user as a vehicle owner');
        });
    },


    /**
     *
     * @param sid
     * @param vid
     */
    isUserVehicleOwner: function (sid, vid) {

        return Vehicle.findOne({vid: vid}, 'owners').lean().then(item => {
            logger.logInfo(`(mdbModules.vehicle.isUserVehicleOwner) Successfully checked if user is an owner of vehicle`);
            return Promise.resolve(item.owners.indexOf(sid) > -1);
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.isUserVehicleOwner) Could not check if user ${sid} is an owner of vehicle ${vid}
${err}`);
            return Promise.reject('Could not check if user is an owner of vehicle');
        });
    },

    /**
     *
     * @param vid
     * @param image_name
     */
    addVehicleImage: function (vid, image_name) {

        return Vehicle.update({vid: vid}, {picture: image_name}, {multi: false}).then(function () {
            logger.logInfo(`(mdbModules.vehicle.addVehicleImage) Successfully updated vehicle's image`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.addVehicleImage) Could not update vehicle ${vid}'s image
${err}`);
            return Promise.reject('Could not update vehicle picture');
        });
    },

    /**
     *
     * @param vid
     * @returns {Promise}
     */
    getVehicleImage: function (vid) {

        return Vehicle.find({vid: vid}, 'picture', {multi: false}).lean().then(item => {
            if (item.length === 0) {
                logger.logInfo(`(mdbModules.vehicle.getVehicleImage) Could not get image name of vehicle ${vid}`);
                return Promise.reject();
            } else {
                logger.logInfo(`(mdbModules.vehicle.getVehicleImage) Successfully retrieved image name of vehicle ${vid}`);
                return Promise.resolve(item[0].picture);
            }
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.getVehicleImage) Could not get image name of vehicle ${vid}
${err}`);
            return Promise.reject('Could not retrieve image of the vehicle');
        });
    },

    /**
     * Delete the specified vehicle
     * @param vid
     */
    removeVehicle: function (vid) {

        return Vehicle.remove({vid: vid}).then(() => {
            logger.logInfo(`(mdbModules.vehicle.removeVehicle) Successfully deleted vehicle ${vid}`);
            return Promise.resolve();
        }).catch(err => {
            logger.logInfo(`(mdbModules.vehicle.removeVehicle) Could not delete vehicle ${vid}
${err}`);
            return Promise.reject('Could not delete the vehicle');
        });
    }
};
