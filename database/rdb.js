/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let logger = require('../logger.js');
let config = require('../config.js');

let redis = require("redis");
let client;

const SID = 'sid'; // Stuber ID
const GID = 'gid'; // Google ID
const VID = 'vid'; // Vehicle ID
const AID = 'aid'; // Active ID
const TID = 'tid'; // Trip ID - Schedule
const LID = 'lid'; // Lift ID - Schedule
const WAL = 'wal'; // Waiting List

const DM = 'deletionMarked'; // Marked for deletion

/**
 *
 * @returns {Promise}
 */
function dropCollections () {
    return new Promise((resolve, reject) => {

        client.flushdb(function (err) {
            if (err) {
                logger.logInfo('(rdb.dropCollections) Could not clear Redis database
' + err);
                reject(err);
            } else {
                logger.logInfo('(rdb.dropCollections) Cleared Redis database');
                resolve();
            }
        });
    });
}

module.exports = {
    /**
     * Init the Redis db.
     * @returns {*|Promise}
     */
    init: function (isMaster=false) {

        return new Promise(function (resolve, reject) {

            logger.logInfo('(rdb.init) Trying to connect to Redis database');
            client = redis.createClient();

            client.on("error", err => {
                logger.logFatal('(rdb.init) Could not connect to redis database - ' + err);
                reject(err);
            });

            client.on("connect", () => {
                logger.logInfo('(rdb.init) Successfully connected to redis database');

                if (config.CLEAR_REDIS_COLLECTIONS && isMaster) {
                    logger.logWarn('(rdb.init) CLEAR_REDIS_COLLECTIONS = ' + config.CLEAR_REDIS_COLLECTIONS);
                    dropCollections().then(() => resolve()).catch(err => reject(err));
                } else {
                    resolve();
                }
            });
        });
    },

    // Export dropCollections
    dropCollections: dropCollections,

    /**
     * Load the initial data into the db.
     * @param gid
     * @param sid
     * @param vid
     * @param aid
     * @param tid
     * @param lid
     * @param wal
     * @returns {Promise}
     */
    loadData: function (gid, sid, vid, aid, tid, lid, wal) {
        /**
         * Load the list of ids in to the Redis database into set 'set'. It is assumed that the list of ids are a list of
         * mappings in the form [{index: id1}, {index: id1}, {index: id1}, {index: id1}, ...]
         * @param set Where the ids should be saved
         * @param index The name of the key to access the ids
         * @param ids The list of ids to save
         */
        let load = function (set, index, ids) {
            if (ids !== undefined && ids !== null && ids.length > 0) {
                logger.logInfo(`(rdb.loadData) Loading IDs for set ${set}`);

                for (let i in ids) {
                    client.sadd(set, ids[i][index]);
                }
            } else {
                logger.logWarn(`(rdb.loadData) Supplied IDs for set ${set} is null`);
            }
        };

        return new Promise((resolve, reject) => {
            try {
                load(GID, 'gid', gid);
                load(SID, 'sid', sid);
                load(VID, 'vid', vid);
                load(AID, 'aid', aid);
                load(TID, 'tid', tid);
                load(LID, 'lid', lid);
                load(WAL, 'sid', wal);
            } catch (err) {
                reject(err);
            }

            resolve();
        });


    },

    //**************************
    // Check if exists methods
    //**************************
    /**
     *
     * @param gid
     * @returns {Promise}
     */
    gidExists: function (gid) {

        return new Promise(function (resolve, reject) {
            client.sismember(GID, gid, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.gidExists) Could not check if user ' + gid + ' is exists');
                    reject(err);
                } else {
                    logger.logInfo('(rdb.gidExists) User ' + gid + (res === 1 ? ' exists' : ' does not exist'));
                    resolve(res === 1);
                }
            });
        });
    },

    /**
     *
     * @param sid
     * @returns {Promise}
     */
    sidExists: function (sid) {

        return new Promise(function (resolve, reject) {
            client.sismember(SID, sid, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.sidExists) Could not check if user ' + sid + ' is exists');
                    reject(err);
                } else {
                    logger.logInfo('(rdb.sidExists) User ' + sid + (res === 1 ? ' exists' : ' does not exist'));
                    resolve(res === 1);
                }
            });
        });
    },

    /**
     *
     * @returns {Promise}
     * @param vid
     */
    vidExists: function (vid) {

        return new Promise(function (resolve, reject) {
            client.sismember(VID, vid, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.vidExists) Could not check if vehicle ' + vid + ' is exists');
                    reject(err);
                } else {
                    logger.logInfo('(rdb.vidExists) Vehicle ' + vid + (res === 1 ? ' exists' : ' does not exist'));
                    resolve(res === 1);
                }
            });
        });
    },

    /**
     *
     * @param aid
     * @returns {Promise}
     */
    aidExists: function (aid) {

        return new Promise(function (resolve, reject) {
            client.sismember(AID, aid, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.aidExists) Could not check if active ' + aid + ' is exists');
                    reject(err);
                } else {
                    logger.logInfo('(rdb.aidExists) Active ' + aid + (res === 1 ? ' exists' : ' does not exist'));
                    resolve(res === 1);
                }
            });
        });
    },

    /**
     *
     * @param tid
     * @returns {Promise}
     */
    tidExists: function (tid) {

        return new Promise(function (resolve, reject) {
            client.sismember(TID, tid, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.tidExists) Could not check if trip ' + tid + ' is exists');
                    reject(err);
                } else {
                    logger.logInfo('(rdb.tidExists) Trip ' + tid + (res === 1 ? ' exists' : ' does not exist'));
                    resolve(res === 1);
                }
            });
        });
    },

    /**
     *
     * @param lid
     * @returns {Promise}
     */
    lidExists: function (lid) {

        return new Promise(function (resolve, reject) {
            client.sismember(LID, lid, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.lidExists) Could not check if lift ' + lid + ' is exists');
                    reject(err);
                } else {
                    logger.logInfo('(rdb.lidExists) Lift ' + lid + (res === 1 ? ' exists' : ' does not exist'));
                    resolve(res === 1);
                }
            });
        });
    },

    /**
     *
     * @param sid
     * @returns {Promise}
     */
    isPassengerInWaitingList: function (sid) {

        return new Promise(function (resolve, reject) {
            client.sismember(WAL, sid, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.isPassengerInWaitingList) Could not check if user ' + sid + ' is a passenger');
                    reject(err);
                } else {
                    logger.logInfo('(rdb.isPassengerInWaitingList) User ' + sid + (res === 1 ? ' is a passenger' : ' is not a passenger'));
                    resolve(res === 1);
                }
            });
        });
    },

    //**************************
    // Add new IDs
    //**************************
    /**
     * Add a gid and sid
     * @param gid
     * @param sid
     */
    addUser: function (gid, sid) {

        return new Promise((resolve, reject) => {

            logger.logInfo('(rdb.addUser) Adding new Google ID');
            client.sadd(GID, gid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    logger.logInfo('(rdb.addUser) Adding new Stuber ID');
                    client.sadd(SID, sid, function (err, res) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve();
                        }
                    });
                }
            });

        });
    },

    /**
     *
     * @param gid
     * @returns {Promise}
     */
    addGid: function (gid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addGid) Adding new Google ID');
            client.sadd(GID, gid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    /**
     *
     * @param sid
     * @returns {Promise}
     */
    addSid: function (sid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addSid) Adding new Stuber ID');
            client.sadd(SID, sid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    /**
     *
     * @param vid
     * @returns {Promise}
     */
    addVid: function (vid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addVid) Adding new Vehicle ID');
            client.sadd(VID, vid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    /**
     *
     * @param tid
     * @returns {Promise}
     */
    addTid: function (tid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addTid) Adding new Trip ID');
            client.sadd(TID, tid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    /**
     *
     * @param lid
     * @returns {Promise}
     */
    addLid: function (lid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addLid) Adding new Lift ID');
            client.sadd(LID, lid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    /**
     *
     * @param aid
     * @returns {Promise}
     */
    addAid: function (aid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addAid) Adding new Active ID');
            client.sadd(AID, aid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    /**
     *
     * @param sid
     * @returns {Promise}
     */
    addPassengerToWaitingList: function (sid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addPassengerToWaitingList) Adding new passenger to waiting list');
            client.sadd(WAL, sid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    //**************************
    // Remove new IDs
    //**************************

    removeAid: function (aid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.removeAid) Removing active');
            client.srem(AID, aid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    removeTid: function (tid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.removeTid) Removing scheduled trip');
            client.srem(TID, tid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    removeLid: function (lid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.removeLid) Removing scheduled lift');
            client.srem(LID, lid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    removeVid: function (vid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.removeVid) Removing vehicle');
            client.srem(VID, vid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    removeUser: function (gid, sid) {

        return new Promise((resolve, reject) => {

            logger.logInfo('(rdb.removeUser) Removing Google ID');
            client.srem(GID, gid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    logger.logInfo('(rdb.removeUser) Removing Stuber ID');
                    client.srem(SID, sid, function (err, res) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve();
                        }
                    });
                }
            });

        });
    },

    removePassengerFromWaitingList: function (sid) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.removePassengerFromWaitingList) Removing passenger');
            client.srem(WAL, sid, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    //**************************
    // Marked for deletion IDs
    //**************************

    /**
     *
     * @param id
     * @returns {Promise}
     */
    addMarkedForDeletionId: function (id) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addMarkedForDeletionId) Adding marked for deletion ID');
            client.sadd(DM, id, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    /**
     *
     * @param id
     * @returns {Promise}
     */
    isIdMarkedForDeletion: function (id) {

        return new Promise(function (resolve, reject) {
            client.sismember(DM, id, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.isIdMarkedForDeletion) Could not check if ID ' + id + ' is marked for deletion');
                    reject(err);
                } else {
                    logger.logInfo('(rdb.isIdMarkedForDeletion) ID ' + id + (res === 1 ? ' is marked for deletion' : ' is not marked for deletion'));
                    resolve(res === 1);
                }
            });
        });
    },

    //**************************
    // Active release IDs
    //**************************

    /**
     *
     * @param id
     * @param time
     * @returns {Promise}
     */
    addActiveRelease: function (id, time) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.addActiveRelease) Adding active release');
            client.set(id, time, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    },

    /**
     *
     * @param id
     * @returns {Promise}
     */
    getActiveRelease: function (id) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.getActiveRelease) Adding active release');
            client.get(id, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.getActiveRelease) Could not get active ' + id + ' release timestamp');
                    reject(err);
                } else {
                    logger.logInfo(`(rdb.getActiveRelease) Successfully retrieved active ${id} release timestamp ($${res})`);
                    resolve(res);
                }
            });
        });
    },

    /**
     *
     * @param id
     * @returns {Promise}
     */
    removeActiveRelease: function (id) {

        return new Promise(function (resolve, reject) {
            logger.logInfo('(rdb.removeActiveRelease) Removing active release');
            client.del(id, function (err, res) {
                if (err) {
                    logger.logInfo('(rdb.removeActiveRelease) Could not remove active ' + id + ' release timestamp');
                    reject(err);
                } else {
                    logger.logInfo(`(rdb.removeActiveRelease) Successfully removed active ${id} release timestamp ($${res})`);
                    resolve(res);
                }
            });
        });
    }
};
