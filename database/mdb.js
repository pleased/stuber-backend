/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// File that contains all database access methods
let mongoose = require('mongoose');
let Promise = require('promise');
let bulk = require('bulk-require');

let logger = require('../logger.js');
let config = require('../config.js');

// ----- Models -----
let User = require('../models/user.js').User;
let History = require('../models/user.js').History;

let Inbox = require('../models/inbox.js').Inbox;
let Review = require('../models/review.js').Review;

let Vehicle = require('../models/vehicle.js').Vehicle;

let Passenger = require('../models/passenger').Passenger;
let Active = require('../models/active.js').Active;
let WaitingList = require('../models/waitingList.js').WaitingList;

let TripSchedule = require('../models/tripSchedule.js').TripSchedule;
let LiftSchedule = require('../models/liftSchedule.js').LiftSchedule;
// ------------------

// URL when running db in container
// let dbURL = "mongodb://" + process.env.MONGODB_PORT_27017_TCP_ADDR + ':' + process.env.MONGODB_PORT_27017_TCP_PORT + "/StuberDB";

/**
 * Connect to the database
 */
function init(isMaster=false) {

    logger.logInfo('(mdb.init) Trying to connect to the database @' + config.DB_URL);

    mongoose.Promise = Promise;

    // Connect to the db
    return mongoose.connect(config.DB_URL, {useMongoClient: true}).then(() => {
        logger.logInfo('(mdb.init) Successfully connected to the database');

        if (config.CLEAR_MONGO_COLLECTIONS && isMaster) {
            logger.logWarn('(mdb.init) CLEAR_MONGO_COLLECTIONS = ' + config.CLEAR_MONGO_COLLECTIONS);
            return dropCollections();
        } else {
            return Promise.resolve();
        }
    }, err => {
        logger.logFatal('(mdb.init) Could not connect to the database - ' + err);
        return Promise.reject(err);
    });
}

/**
 *
 */
function dropCollections() {

    let calls = [];

    // Make remove calls to all collections
    calls.push(User.remove({}).then(() => logger.logInfo('(mdb.dropCollections) Cleared User collection')));
    calls.push(Inbox.remove({}).then(() => logger.logInfo('(mdb.dropCollections) Cleared Inbox collection')));
    calls.push(Review.remove({}).then(() => logger.logInfo('(mdb.dropCollections) Cleared Review collection')));
    calls.push(Vehicle.remove({}).then(() => logger.logInfo('(mdb.dropCollections) Cleared Vehicle collection')));
    calls.push(Active.remove({}).then(() => logger.logInfo('(mdb.dropCollections) Cleared Active collection')));
    calls.push(WaitingList.remove({}).then(() => logger.logInfo('(mdb.dropCollections) Cleared WaitingList collection')));
    calls.push(TripSchedule.remove({}).then(() => logger.logInfo('(mdb.dropCollections) Cleared TripSchedule collection')));
    calls.push(LiftSchedule.remove({}).then(() => logger.logInfo('(mdb.dropCollections) Cleared LiftSchedule collection')));

    return Promise.all(calls);
}

module.exports = {
    init: init,
    dropCollections: dropCollections,

    // Import all functions in the mdbModules folder.
    // They can be accessed from the outside as mdb.mdbModules.<filename>.<functionName>
    mdbModules: bulk(__dirname, ['mdbModules/*.js'])['mdbModules']
};
