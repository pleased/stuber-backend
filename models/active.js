/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Passenger = require('./passenger.js').Passenger.schema;

// Create a schema
let activeSchema = new Schema({

    vid: {type: String, required: true, unique: true},
    sid: {type: String, required: true, unique: true},
    aid: {type: String, required: true, unique: true},

    destination: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },
    origin: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },

    // Only the time portion of the date will be used
    startTime: {type: Date, required: true},
    endTime: {type: Date, required: true},

    currLoc: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },

    openSeats: {type: Number, required: true},

    passengers: {type: [Passenger], default: []},
    pickups: {type: [Passenger], default: []},

    route: {type: [{
        _id: false,
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    }], default: []},

    originalTravelDistance: {type: Number, default: 0.0},
    traveled: {type: Number, default: 0.0}
});

// Create a model using schema
let Active = mongoose.model('Active', activeSchema);

module.exports = {
    Active: Active
};
