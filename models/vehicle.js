/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Create a schema
let vehicleSchema = new Schema({

    vid: {type: String, required: true, unique: true},

    model: {type: String, required: true},
    make: {type: String, required: true},
    color: {type: String, required: true},
    regNumber: {type: String, required: true},
    seats: {type: Number, required: true},

    owners: {type: [String], required: true},

    picture: {type: String, default: ''}
});

// Create a model using schema
let Vehicle = mongoose.model('Vehicle', vehicleSchema);

module.exports = {
    Vehicle: Vehicle
};
