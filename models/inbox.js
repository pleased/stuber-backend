/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let SchemaTypes = mongoose.Schema.Types;

// Create a schema
let inboxSchema = new Schema({

    sid: {type: String, required: true, unique: true},
    messages: {type: [{
        timestamp: {type: Number, required: true},
        text: {type: String, required: true},
        type: {type: String, default: 'STANDARD'},
        _id: false
    }], default: []}
});

// Create a model using schema
let Inbox = mongoose.model('Inbox', inboxSchema);

module.exports = {
    Inbox: Inbox
};
