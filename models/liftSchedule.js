/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Create a schema
let liftScheduleSchema = new Schema({
    sid: {type: String, required: true},
    lid: {type: String, required: true, unique: true},

    origin: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },
    destination: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },

    // Only the time portion of the date will be used
    startTime: {type: Date, required: true},
    endTime: {type: Date, required: true},

    date: {type: Date},
    singleDate: {type: Boolean, required: true},
    singleDateDriver: {type: String, default: ''},

    // The days the schedule will be active
    days: {
        monday: {type: Boolean, default: false},
        tuesday: {type: Boolean, default: false},
        wednesday: {type: Boolean, default: false},
        thursday: {type: Boolean, default: false},
        friday: {type: Boolean, default: false},
        saturday: {type: Boolean, default: false},
        sunday: {type: Boolean, default: false}
    },
    weekRepeat: {type: Boolean, required: true},

    // Lift Ids will populate this field
    driver: {
        monday: {type: String, default: ''},
        tuesday: {type: String, default: ''},
        wednesday: {type: String, default: ''},
        thursday: {type: String, default: ''},
        friday: {type: String, default: ''},
        saturday: {type: String, default: ''},
        sunday: {type: String, default: ''}
    }
});

// Create a model using schema
let LiftSchedule = mongoose.model('LiftSchedule', liftScheduleSchema);

module.exports = {
    LiftSchedule: LiftSchedule
};
