/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Create a schema
let HistorySchema = new Schema({

    destination: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },
    origin: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },

    // Only the time portion of the date will be used
    startTime: {type: Date, required: true},
    endTime: {type: Date, required: true},

    timestamp: {type: Number, required: true},

    _id: false,
});

let userSchema = new Schema({
    gid: {type: String, required: true, unique: true},
    sid: {type: String, required: true, unique: true},

    fullname: {type: String, required: true},
    age: {type: Number, required: true},
    email: {type: String, default: ''},
    address: {
        home: {type: String, default: ''},
        work: {type: String, default: ''}
    },
    likes: {type: [String], default: []},
    dislikes: {type: [String], default: []},

    isDriver: {type: Boolean, default: false},

    liftHistory: {type: [HistorySchema], default: []},
    tripHistory: {type: [HistorySchema], default: []},

    picture: {type: String, default: ''}
});

// Create a model using schema
let User = mongoose.model('User', userSchema);
let History = mongoose.model('History', HistorySchema);

module.exports = {
    User: User,
    History: History
};
