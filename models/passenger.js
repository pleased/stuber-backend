/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Create a schema
let passengerSchema = new Schema({

    sid: {type: String, required: true, unique: true},

    destination: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },
    origin: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },

    // Only the time portion of the date will be used
    startTime: {type: Date, required: true},
    endTime: {type: Date, required: true},

    _id: false
});

// Create a model using schema
let Passenger = mongoose.model('Passenger', passengerSchema);

module.exports = {
    Passenger: Passenger
};
