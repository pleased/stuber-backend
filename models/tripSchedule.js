/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Create a schema
let tripScheduleSchema = new Schema({
    sid: {type: String, required: true},
    vid: {type: String, required: true},
    tid: {type: String, required: true, unique: true},

    origin: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },
    destination: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },

    // Only the time portion of the date will be used
    startTime: {type: Date, required: true},
    endTime: {type: Date, required: true},

    date: {type: Date},
    singleDate: {type: Boolean, required: true},
    singleDatePassengers: {type: [String], default: []},
    singleDateOpenSeats: {type: Number, default: 0},

    // The route that this trip schedule will take when finally made active
    singleDateRoute: {type: [{
        _id: false,
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    }], default: []},

    // The days the schedule will be active
    days: {
        monday: {type: Boolean, default: false},
        tuesday: {type: Boolean, default: false},
        wednesday: {type: Boolean, default: false},
        thursday: {type: Boolean, default: false},
        friday: {type: Boolean, default: false},
        saturday: {type: Boolean, default: false},
        sunday: {type: Boolean, default: false}
    },
    weekRepeat: {type: Boolean, required: true},

    // Lift Ids will populate this field where each day will have Ids
    passengers: {
        monday: {type: [String], default: []},
        tuesday: {type: [String], default: []},
        wednesday: {type: [String], default: []},
        thursday: {type: [String], default: []},
        friday: {type: [String], default: []},
        saturday: {type: [String], default: []},
        sunday: {type: [String], default: []}
    },

    // The routes that will be taken for each individual day
    weekRepeatRoute: {
        monday: {type: [{
            _id: false,
            lat: {type: Number, required: true},
            lon: {type: Number, required: true}
        }], default: []},
        tuesday: {type: [{
            _id: false,
            lat: {type: Number, required: true},
            lon: {type: Number, required: true}
        }], default: []},
        wednesday: {type: [{
            _id: false,
            lat: {type: Number, required: true},
            lon: {type: Number, required: true}
        }], default: []},
        thursday: {type: [{
            _id: false,
            lat: {type: Number, required: true},
            lon: {type: Number, required: true}
        }], default: []},
        friday: {type: [{
            _id: false,
            lat: {type: Number, required: true},
            lon: {type: Number, required: true}
        }], default: []},
        saturday: {type: [{
            _id: false,
            lat: {type: Number, required: true},
            lon: {type: Number, required: true}
        }], default: []},
        sunday: {type: [{
            _id: false,
            lat: {type: Number, required: true},
            lon: {type: Number, required: true}
        }], default: []}
    },

    // The number of open seats there are for each of the days
    weekRepeatOpenSeats: {
        monday: {type: Number, default: 0},
        tuesday: {type: Number, default: 0},
        wednesday: {type: Number, default: 0},
        thursday: {type: Number, default: 0},
        friday: {type: Number, default: 0},
        saturday: {type: Number, default: 0},
        sunday: {type: Number, default: 0}
    }
});

// Create a model using schema
let TripSchedule = mongoose.model('TripSchedule', tripScheduleSchema);

module.exports = {
    TripSchedule: TripSchedule
};
