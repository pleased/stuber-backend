/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Create a schema
let reviewSchema = new Schema({

    reviewer: {type: String, required: true},
    reviewed: {type: String, required: true},

    message: {type: String, default: ''},
    rating: {type: Number, required: true}
});

// Create a model using schema
let Review = mongoose.model('Review', reviewSchema);

module.exports = {
    Review: Review
};
