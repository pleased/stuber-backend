/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

// Create a schema
let waitingListSchema = new Schema({

    sid: {type: String, required: true, unique: true},
    aid: {type: String, unique: true, default: ''},

    destination: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },
    origin: {
        lat: {type: Number, required: true},
        lon: {type: Number, required: true}
    },

    // Only the time portion of the date will be used
    startTime: {type: Date, required: true},
    endTime: {type: Date, required: true},

    // The drivers that match and the route that will be taken if a driver is chosen
    drivers: {type: [{
        _id: false,
        aid: {type: String, required: true},
        route: {type: [{
            _id: false,
            lat: {type: Number, required: true},
            lon: {type: Number, required: true}
            }], default: []}
    }], required: true}
});

// Create a model using schema
let WaitingList = mongoose.model('WaitingList', waitingListSchema);

module.exports = {
    WaitingList: WaitingList
};
