/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// Contains methods for image reading and writing
let logger = require('./logger.js');

let fs = require('fs');

module.exports = {

    PATH_USERS: __dirname + '/public/images/users/',
    PATH_VEHICLES: __dirname + '/public/images/vehicles/',

    createImageDirs: function () {
        logger.logInfo('(io.createImageDirs) User image path = ' + this.PATH_USERS);
        logger.logInfo('(io.createImageDirs) Vehicle image path = ' + this.PATH_VEHICLES);

        if (!fs.existsSync('public')) {
            logger.logInfo('(io.createImageDirs) Creating "public" directory');
            fs.mkdirSync('public');
        }

        if (!fs.existsSync('./public/images')) {
            logger.logInfo('(io.createImageDirs) Creating "images" directory');
            fs.mkdirSync('./public/images');
        }

        if (!fs.existsSync('./public/images/users')) {
            logger.logInfo('(io.createImageDirs) Creating "users" directory');
            fs.mkdirSync('./public/images/users');
        }

        if (!fs.existsSync('./public/images/vehicles')) {
            logger.logInfo('(io.createImageDirs) Creating "vehicles" directory');
            fs.mkdirSync('./public/images/vehicles');
        }
    },

    fileExistsUser: function (filename) {
        return filename !== '' && fs.existsSync(this.PATH_USERS + filename);
    },

    fileExistsVehicle: function (filename) {
        return filename !== '' && fs.existsSync(this.PATH_VEHICLES + filename);
    },

    saveVechileImage: function (req, callback) {
        let tmp_path = req.file.path;
        let filename = req.params.id + '.' + req.file.originalname.substr(req.file.originalname.lastIndexOf('.') + 1);
        let target_path = this.PATH_VEHICLES + filename;

        let src = fs.createReadStream(tmp_path);
        let dest = fs.createWriteStream(target_path);
        src.pipe(dest);

        src.on('end', function() {
            // If the file was saved successfully return the filename as the message

            fs.unlinkSync(req.file.path);
            callback(false, filename);
        });

        src.on('error', function(err) {
            callback(true, 'Could not save file');
        });
    },
    
    saveUserImage: function (req, callback) {
        let tmp_path = req.file.path;
        let filename = req.params.id + '.' + req.file.originalname.substr(req.file.originalname.lastIndexOf('.') + 1);
        let target_path = this.PATH_USERS + filename;

        let src = fs.createReadStream(tmp_path);
        let dest = fs.createWriteStream(target_path);
        src.pipe(dest);

        src.on('end', function() {
            // If the file was saved successfully return the filename as the message

            fs.unlinkSync(req.file.path);
            callback(false, filename);
        });

        src.on('error', function(err) {
            callback(true, 'Could not save file');
        });
    }
};
