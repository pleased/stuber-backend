/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let request = require('request');

let config = require('../config.js');
let logger = require('../logger.js');

function generatePairs(points) {

    let pairs = [];

    // Generate pairs
    for (let i = 0; i < points.length; i = i + 1) {
        for (let j = 0; j < points.length; j = j + 1) {
            if (j !== i) {
                pairs.push({
                    origin: {
                        lon: points[i].lon,
                        lat: points[i].lat
                    },
                    destination: {
                        lon: points[j].lon,
                        lat: points[j].lat
                    },
                    distance: 0.0,
                    duration: 0.0
                });
            }
        }
    }

    return pairs;
}

/**
 * Given GPS points in travel order, return the total travel distance and time
 *
 * Returns a list of the form [[[dist1, time1], [dist2, time2], ...], [totalDist, totalTime]]
 */
function calcRoute(points) {

    return new Promise(function (resolve, reject) {

        // Build up the GPS part of the URL
        let pairs = [];
        for (let i = 0; i < points.length; i = i + 1) {
            pairs.push([points[i].lon, points[i].lat].join(','));
        }

        let gpsPart = pairs.join(';');
        let url = config.OSRM_URL + `route/v1/driving/${gpsPart}?overview=false`;
        request.get(url, (error, response, body) => {
            try {
                if (error) {
                    reject(error);
                } else {
                    // Convert the string body to a JSON object
                    body = JSON.parse(body);

                    if (body.code === 'Ok') {
                        let result = [];

                        for (let i = 0; i < body.routes[0].legs.length; i = i + 1) {
                            // Add the metric to the results => [distance, duration]
                            result.push({
                                distance: body.routes[0].legs[i].distance,
                                duration: body.routes[0].legs[i].duration
                            });
                        }

                        // Add the total metrics also
                        result = [result, {
                            totalDistance: body.routes[0].distance,
                            totalDuration: body.routes[0].duration
                        }];

                        resolve(result);
                    } else {
                        reject(body.code);
                    }
                }
            } catch (e) {
                reject(e);
            }
        });
    });
}

/**
 * Given GPS points in travel order, return the total travel time
 * @param points
 * @param total
 */
function calcTime(points, total = true) {

    logger.logInfo('(osm.calcDst) Getting travel time');

    return calcRoute(points).then(r => {
        if (total === true) {
            r[0] = r[0].map(item => item.duration);
            r[1] = r[1].totalDuration;
        } else {
            r = r[0].map(item => item.duration);
        }

        return Promise.resolve(r);
    });
}

/**
 * Given GPS points in travel order, return the total travel distance
 * @param points
 * @param total
 */
function calcDist(points, total = true) {

    logger.logInfo('(osm.calcDst) Getting travel distance');

    return calcRoute(points).then(r => {
        if (total === true) {
            r[0] = r[0].map(item => item.distance);
            r[1] = r[1].totalDistance;
        } else {
            r = r[0].map(item => item.distance);
        }

        return Promise.resolve(r);
    });
}

/**
 * Given GPS points in travel order, return the total travel distance and time
 * @param points
 * @param total
 */
function calcTimeDist(points, total = true) {

    logger.logInfo('(osm.calcTimeDist) Getting travel distance and time');

    return calcRoute(points).then(r => {
        if (total === false) {
            r = r[0].map(item => {
                return {
                    distance: item.distance,
                    duration: item.duration
                };
            });
        }

        return Promise.resolve(r);
    });
}

/**
 * Given GPS points, return the travel distance and time between all pairs
 * @param points
 */
function calcOto(points) {

    let pairs = generatePairs(points);
    let calls = [];

    for (let i = 0; i < pairs.length; i = i + 1) {
        calls.push(calcTimeDist([pairs[i].origin, pairs[i].destination], false).then(v => {
            pairs[i].distance = v[0].distance;
            pairs[i].duration = v[0].duration;

            return Promise.resolve();
        }));
    }

    return Promise.all(calls).then(() => {
        return Promise.resolve(pairs);
    });
}

function test() {

    // calcDist([{lat: -33.9859, lon: 19.2861}, {lat: -33.9321, lon: 18.8602}, {lat: -34.0826, lon: 18.8234}]).then(r => console.log(r)).catch(r => console.log(r));
    // calcTime([{lat: -33.9859, lon: 19.2861}, {lat: -33.9321, lon: 18.8602}, {lat: -34.0826, lon: 18.8234}]).then(r => console.log(r)).catch(r => console.log(r));

    calcOto([{lat: -33.9859, lon: 19.2861}, {lat: -33.9321, lon: 18.8602}, {lat: -34.0826, lon: 18.8234}]);
}

module.exports = {
    calcRoute: calcRoute,
    calcTimeDist: calcTimeDist,
    calcTime: calcTime,
    calcDist: calcDist,
    calcOto: calcOto
};
