/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
let config = require('../config.js');
let logger = require('../logger.js');

let Promise = require('promise');
let client = require('@google/maps').createClient({
    key: config.GMAPS_KEY
});

// TODO: Look at osm.js on how to update this adapter. Currently won't work with system as is

function getResponseMetrics(resp) {

    // Travel distance in meters
    let dist = resp.json.rows[0].elements[0].distance.value;

    // Travel time in seconds
    let time = resp.json.rows[0].elements[0].duration.value;

    return [dist, time];
}

function generatePairs(points) {

    let pairs = [];

    // Generate pairs
    for (let i = 0; i < points.length; i = i + 1) {
        for (let j = i + 1; j < points.length; j = j + 1) {
            pairs.push([points[i], points[j]]);
        }
    }

    return pairs;
}

/**
 * Given GPS points in travel order, return the total travel distance and time
 */
function calcRoute(points) {

    return new Promise(function (resolve, reject) {
        let origins = [];
        let destinations = [];
        let metrics = [0, 0];

        if (points.length <= 1) {
            logger.logInfo('(google.calcRoute) Too few points provided');
            reject();
        }

        for (let i = 0; i < points.length - 1; i = i + 1) {
            origins.push([points[i].lat, points[i].lon]);
            destinations.push([points[i+1].lat, points[i+1].lon]);
        }

        console.log('Origins = ' + origins);
        console.log('Destination = ' + destinations);

        client.distanceMatrix({
            origins: origins,
            destinations: destinations,
            mode: 'driving',
            avoid: ['ferries'],
            traffic_model: 'best_guess',
            departure_time: Date.now()
        }, function (res, status) {
            // Add the results
            // for (let i = 0; i < status.json.rows[0].elements.length; i = i + 1) {
            //     console.log(status.json.rows[0].elements[i]);
            //     metrics[0] += status.json.rows[0].elements[i].distance.value;
            //     metrics[1] += status.json.rows[0].elements[i].duration.value;
            // }
            if (status.json.rows[0].elements[0].status === 'ZERO_RESULTS') {
                logger.logInfo('(google.calcRoute) There is no route between the specified coordinates');
                reject('There is no route between the specified coordinates');
            } else {
                let length = status.json.rows[0].elements.length - 1;
                metrics[0] = status.json.rows[0].elements[length].distance.value;
                metrics[1] = status.json.rows[0].elements[length].duration.value;

                resolve(metrics);
            }
        });
    });
}

/**
 * Given GPS points in travel order, return the total travel time
 */
function calcTime(points) {

    logger.logInfo('(google.calcDst) Getting travel time');

    return calcRoute(points).then(r => {
        return Promise.resolve(r[1]);
    }).catch(err => {
        return Promise.reject(err);
    });
}

/**
 * Given GPS points in travel order, return the total travel distance
 */
function calcDist(points) {

    logger.logInfo('(google.calcDst) Getting travel distance');

    return calcRoute(points).then(r => {
        return Promise.resolve(r[0]);
    }).catch(err => {
        return Promise.reject(err);
    });
}

/**
 * Given GPS points, return the travel distance and time between all pairs
 */
function calcOto(points, callback) {

    let metrics = [];
    let calls = [];
    let pairs = generatePairs(points);

    // Get the metrics for each pair
    pairs.forEach(function (p) {

        calls.push(new Promise(function (resolve, reject) {
            client.distanceMatrix({
                origins: [p[0]],
                destinations: [p[1]],
                mode: 'driving',
                avoid: ['ferries'],
                traffic_model: 'best_guess',
                departure_time: Date.now()
            }, function (res, status) {

                metrics.push({
                    towns: p,
                    metric: getResponseMetrics(status)
                });

                resolve();
            });
        }));
    });

    // Wait for all metrics to have been retrieved before returning results
    return Promise.all(calls).then(() => {
        return Promise.resolve(metrics);
    });
}

function test() {

    calcRoute(['Stellenbosch, South Africa', 'Pretoria, South Africa', 'Cape Town, South Africa', 'Johannesburg, South Africa', 'Hermanus, South Africa'], function (r) {
        console.log(r);
    });
}

module.exports = {
    calcRoute: calcRoute,
    calcTime: calcTime,
    calcDist: calcDist,
    calcOto: calcOto
};
