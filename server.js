/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// Increase the maximum number of event listeners as kue needs 12 in our case
require('events').EventEmitter.prototype._maxListeners = 12;

let cluster = require('cluster');
let express = require('express');
let bodyParser = require('body-parser');
let morgan = require('morgan');
let rfs = require('rotating-file-stream');
let https = require('https');
let fs = require('fs');
let path = require('path');

let config = require('./config.js');
let logger = require('./logger.js');
let pjson = require('./package.json');

if (cluster.isMaster) {
    // Used to split up logs into instances
    logger.logInfo(`${'='.repeat((75 - String(pjson.version).length) / 2 - 1)}| v${pjson.version} |${'='.repeat((75 - String(pjson.version).length) / 2 - 1)}`);
}

let ta = require('./tokenAuth.js');
let mdb = require('./database/mdb.js');
let rdb = require('./database/rdb.js');
let io = require('./io.js');
let kue = require('./controller/kue.js');

let logDirectory = path.join(__dirname, 'log');

let app;
let router;

// Initialize app instance for master when in test mode
if (process.env.NODE_ENV === 'test') {
    app = express();
    router = express.Router();
}

const SETUP = function (app, router) {

    // Create a rotating write stream
    let accessLogStream = rfs('access.log', {
        interval: '1d', // Rotate daily
        path: logDirectory
    });

    // Setup the logger
    app.use(morgan('Timestamp: :date[iso] -- Method: :method, URL: :url, Status :status, RESP: :response-time ms', {stream: accessLogStream}));
    app.use(bodyParser.json());

    // let options = {
    //     key: fs.readFileSync('./ssl/key.pem'),
    //     cert: fs.readFileSync('./ssl/cert.pem')
    // };

    // JWT Authentication middleware
    app.use((req, res, next) => {
        // If this is a get image request just continue
        if (req.method === 'GET' && req.originalUrl.match('\/api\/v[1-9]{1}\/(user|vehicle)\/.*?\/image$')) {
            logger.logRequest(req.originalUrl, req.headers, req.body);

            next();
        } else {
            logger.logRequest(req.originalUrl, req.headers, req.body);

            // Not the debug, login or register user endpoints
            if (!req.originalUrl.match('\/api\/v[1-9]{1}\/(user|login|(debug\/.*))$')) {
                ta.validateToken(req.headers['x-access-token'], (code, message) => {
                    if (code === 401) {
                        res.status(401).json({success: false, message: message});
                    } else {
                        next();
                    }
                });
            } else {
                next();
            }
        }
    });

    // Use 'api/v1' as prefix to routes.
    app.use('/api/v1', router);

    // Include all routers in 'routes' folder
    if (process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'development') {
        // Don't use the debug endpoints when in the production environment
        require('./routes/debug.js')(router);
    }

    require('./routes/users.js')(router);
    require('./routes/active.js')(router);
    require('./routes/schedule.js')(router);
    require('./routes/gps.js')(router);
    require('./routes/vehicle.js')(router);
    require('./routes/reviews.js')(router);
    require('./routes/login.js')(router);
    require('./routes/index.js')(router);
};

// Master logic
if (cluster.isMaster) {

    // Ensure log directory exists
    fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

    // Create the image storage folders
    io.createImageDirs();

    rdb.init(true).then(() => {
        return mdb.init(true);
    }).then(() => {
        logger.logInfo('(server) Create kue job queues');
        return kue.init(true);
    }).then(() => {

        let mdbid = require('./database/mdbIds.js');

        logger.logInfo('(server.init) Retrieving all system IDs from database');
        // Retrieve all IDs from mongodb and save them in the redis db
        let calls = [
            mdbid.getGids(),
            mdbid.getSids(),
            mdbid.getVids(),
            mdbid.getAids(),
            mdbid.getTids(),
            mdbid.getLids(),
            mdbid.getWaitingList()
        ];

        return Promise.all(calls);
    }).then(v => {
        logger.logInfo('(server.init) Adding system IDs to redis database');
        return rdb.loadData(v[0], v[1], v[2], v[3], v[4], v[5], v[6]);
    }).then(() => {
        // If the server is in test mode the master worker will do everything
        if (process.env.NODE_ENV === 'test') {

            SETUP(app, router);

            // Will be replaced with HTTPS listen
            app.listen(config.PORT, () => {
                logger.logInfo(`(server) Server is now listening on port ${config.PORT}`);
            });

            // Disabled for now. Certificate problems
            // https.createServer(options, app).listen(port, function () {
            //     console.log("Server listening on port " + port);
            // });
        } else {
            let numWorkers = require('os').cpus().length;

            logger.logInfo('(server.init) Master cluster setting up ' + numWorkers + ' workers...');

            for (let i = 0; i < numWorkers; i = i + 1) {
                cluster.fork();
            }

            cluster.on('online', (worker) => {
                logger.logInfo('(server) Worker ' + worker.process.pid + ' is online');
            });

            cluster.on('exit', (worker, code, signal) => {
                logger.logWarn('(server) Worker ' + worker.process.pid + ' died with code: ' + code + ' and signal: ' + signal);
                cluster.fork();
            });
        }
    }).catch(err => {
        logger.logFatal('(server) ' + err);
        logger.logFatal('(server) Exiting...');
        process.exit();
    });
// Worker logic
} else {

    app = express();
    router = express.Router();

    SETUP(app, router);

    rdb.init().then(() => {
        return mdb.init();
    }).then(() => {
        logger.logInfo('(server) Create kue job queues');
        return kue.init();
    }).then(() => {
        // Will be replaced with HTTPS listen
        app.listen(config.PORT, () => {
            logger.logInfo(`(server) Worker ${cluster.worker.id} is now listening on port ${config.PORT}`);
        });

        // Disabled for now. Certificate problems
        // https.createServer(options, app).listen(port, function () {
        //     console.log("Server listening on port " + port);
        // });
    }).catch(err => {
        logger.logFatal('(server) ' + err);
        logger.logInfo('(server) Exiting...');
        process.exit();
    });
}

module.exports = app;
