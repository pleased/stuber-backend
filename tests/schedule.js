/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let server = require('../server.js');
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let objects = require('./setup/objects.js');
let actions = require('./setup/actions.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;

chai.use(chaiHttp);

const copyObject = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

const getDayOfWeek = function () {
    return ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'][new Date().getDay()];
};

// Clear the database before starting the tests
beforeEach(done => {

    // NOTE: The timeout is required so that kue does not crash and cause tests to fail.
    // For some reason kue does not like the rapid flushing of the Redis database.
    // In a production setting this will not cause an issue as the Redis database will never get flushed as it is here
    // The error that gets thrown is 'job __ does not exist'. If it is encountered first try and increase the
    // below timeout value
    setTimeout(() => {
        Promise.all([
            mdb.dropCollections(),
            rdb.dropCollections()
        ]).then(() => {
            return Promise.all([
                actions.registerUser(objects.USER_ONE),
                actions.registerUser(objects.USER_TWO),
                actions.registerUser(objects.USER_THREE),
                actions.registerUser(objects.USER_FOUR)
            ]);
        }).then(res => {
            objects.USER_ONE.sid = res[0][0];
            objects.USER_ONE.token = res[0][1];

            objects.USER_TWO.sid = res[1][0];
            objects.USER_TWO.token = res[1][1];

            objects.USER_THREE.sid = res[2][0];
            objects.USER_THREE.token = res[2][1];

            objects.USER_THREE.sid = res[3][0];
            objects.USER_THREE.token = res[3][1];

            objects.VEHICLE_ONE.sid = objects.USER_ONE.sid;
            objects.VEHICLE_TWO.sid = objects.USER_TWO.sid;

            // Register vehicles and make users drivers
            return Promise.all([
                actions.setUserDriverStatus(objects.USER_ONE.sid, objects.USER_ONE.token),
                actions.setUserDriverStatus(objects.USER_TWO.sid, objects.USER_TWO.token),

                actions.registerVehicle(objects.USER_ONE.token, objects.VEHICLE_ONE),
                actions.registerVehicle(objects.USER_TWO.token, objects.VEHICLE_TWO)
            ]);
        }).then(res => {
            objects.VEHICLE_ONE.vid = res[2];
            objects.VEHICLE_TWO.vid = res[3];

            done();
        }).catch(err => {
            done(err);
        });
    }, 500);

});

// Parent block
describe('Schedule', () => {

    describe('trip retrieval', () => {
        it('should be successful with no content', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });

        it('should be successful with content', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(() => {
                chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message', 'data');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                    expect(res.body.data).to.be.a('object').that.to.have.all.keys('trips');

                    expect(res.body.data.trips).to.be.a('array').that.to.have.lengthOf(1);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(() => {
                chai.request(server).get(`/api/v1/user/some_garbage_value/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('trip', () => {
        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            chai.request(server).post(`/api/v1/user/some_garbage_value/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as vehicle does not exist', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = 'some_garbage_value';

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as user is not driver', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setUserDriverStatus(objects.USER_ONE.sid, objects.USER_ONE.token, false).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as user not owner of vehicle', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_TWO.vid;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as more seats specified than vehicle supports', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;
            trip.singleDateOpenSeats = objects.VEHICLE_ONE.seats + 1;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('singleDate should be successful', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('object').that.to.have.all.keys('tid');

                expect(res.body.data.tid).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('weekRepeat should be successful', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_WEEKLY_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('object').that.to.have.all.keys('tid');

                expect(res.body.data.tid).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('singleDate and WeekRepeat should be successful', done => {

            let tripSingleDate = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            tripSingleDate.vid = objects.VEHICLE_ONE.vid;

            let tripWeekly = copyObject(objects.SCHEDULE_TRIP_WEEKLY_ONE);
            tripWeekly.vid = objects.VEHICLE_ONE.vid;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(tripSingleDate).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('object').that.to.have.all.keys('tid');

                expect(res.body.data.tid).to.be.a('string').that.to.not.have.lengthOf(0);

                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).send(tripWeekly).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message', 'data');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                    expect(res.body.data).to.be.a('object').that.to.have.all.keys('tid');

                    expect(res.body.data.tid).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            });
        });
    });

    describe('lift retrieval', () => {
        it('should be successful with no content', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/lift/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });

        it('should be successful with content', done => {

            let lift = copyObject(objects.SCHEDULE_LIFT_SINGLE_ONE);

            actions.scheduleLift(objects.USER_ONE.sid, lift, objects.USER_ONE.token).then(() => {
                chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/lift/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message', 'data');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                    expect(res.body.data).to.be.a('object').that.to.have.all.keys('lifts');

                    expect(res.body.data.lifts).to.be.a('array').that.to.have.lengthOf(1);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.SCHEDULE_LIFT_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleLift(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(() => {
                chai.request(server).get(`/api/v1/user/some_garbage_value/lift/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('lift', () => {
        it('should fail as invalid SID', done => {

            chai.request(server).post(`/api/v1/user/some_garbage_value/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(objects.SCHEDULE_LIFT_SINGLE_ONE).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as singleDate and weekRepeat specified', done => {

            let lift = copyObject(objects.SCHEDULE_LIFT_SINGLE_ONE);
            lift.singleDate = true;
            lift.weekRepeat = true;

            chai.request(server).post(`/api/v1/user/some_garbage_value/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(lift).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as neither singleDate or weekRepeat specified', done => {

            let lift = copyObject(objects.SCHEDULE_LIFT_SINGLE_ONE);
            lift.singleDate = false;
            lift.weekRepeat = false;

            chai.request(server).post(`/api/v1/user/some_garbage_value/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(lift).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as no destination specified', done => {

            let lift = copyObject(objects.SCHEDULE_LIFT_SINGLE_ONE);
            delete lift.destination;

            chai.request(server).post(`/api/v1/user/some_garbage_value/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(lift).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as no start time specified', done => {

            let lift = copyObject(objects.SCHEDULE_LIFT_SINGLE_ONE);
            delete lift.startTime;

            chai.request(server).post(`/api/v1/user/some_garbage_value/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(lift).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('weekRepeat should fail as no days specified', done => {

            let lift = copyObject(objects.SCHEDULE_LIFT_WEEKLY_ONE);
            delete lift.days;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(lift).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('singleDate should be successful', done => {

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(objects.SCHEDULE_LIFT_SINGLE_ONE).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('object').that.to.have.all.keys('lid');

                expect(res.body.data.lid).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('weekRepeat should be successful', done => {

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(objects.SCHEDULE_LIFT_WEEKLY_ONE).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('object').that.to.have.all.keys('lid');

                expect(res.body.data.lid).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('singleDate and WeekRepeat should be successful', done => {

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(objects.SCHEDULE_LIFT_SINGLE_ONE).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('object').that.to.have.all.keys('lid');

                expect(res.body.data.lid).to.be.a('string').that.to.not.have.lengthOf(0);

                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/lift/schedule`).set('x-access-token', objects.USER_ONE.token).send(objects.SCHEDULE_LIFT_WEEKLY_ONE).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message', 'data');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                    expect(res.body.data).to.be.a('object').that.to.have.all.keys('lid');

                    expect(res.body.data.lid).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            });
        });
    });

    describe('trip activate', () => {
        it('singleDate should be successful with no pickups', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return Promise.resolve();
            }).then(() => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule/${trip.tid}/activate`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('aid');

                        expect(res.body.data.aid).to.be.a('string').that.to.not.have.lengthOf(0);

                        trip.aid = res.body.data.aid;

                        chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                            expect(res).to.have.status(200);

                            expect(res).to.be.json;

                            expect(res.body).to.have.all.keys('success', 'message', 'data');

                            expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                            expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                            expect(res.body.data).to.be.a('object').that.to.have.all.keys('trip');

                            expect(res.body.data.trip.sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                            expect(res.body.data.trip.aid).to.be.a('string').that.to.equal(trip.aid);
                            expect(res.body.data.trip.vid).to.be.a('string').that.to.equal(trip.vid);
                            expect(res.body.data.trip.route).to.be.a('array').that.to.have.lengthOf(1).that.to.deep.equal([trip.destination]);
                            expect(res.body.data.trip.pickups).to.be.a('array').that.to.have.lengthOf(0);
                            expect(res.body.data.trip.passengers).to.be.a('array').that.to.have.lengthOf(0);

                            done();
                        });
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('singleDate should be successful with pickups', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return actions.scheduleLift(objects.USER_TWO.sid, objects.SCHEDULE_LIFT_SINGLE_ONE, objects.USER_TWO.token);
            }).then(() => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule/${trip.tid}/activate`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('aid');

                        expect(res.body.data.aid).to.be.a('string').that.to.not.have.lengthOf(0);

                        trip.aid = res.body.data.aid;

                        chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                            expect(res).to.have.status(200);

                            expect(res).to.be.json;

                            expect(res.body).to.have.all.keys('success', 'message', 'data');

                            expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                            expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                            expect(res.body.data).to.be.a('object').that.to.have.all.keys('trip');

                            expect(res.body.data.trip.sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                            expect(res.body.data.trip.aid).to.be.a('string').that.to.equal(trip.aid);
                            expect(res.body.data.trip.vid).to.be.a('string').that.to.equal(trip.vid);
                            expect(res.body.data.trip.route).to.be.a('array').that.to.have.lengthOf(3).that.to.deep.equal([objects.SCHEDULE_LIFT_SINGLE_ONE.origin, objects.SCHEDULE_LIFT_SINGLE_ONE.destination, trip.destination]);
                            expect(res.body.data.trip.pickups).to.be.a('array').that.to.have.lengthOf(1);
                            expect(res.body.data.trip.passengers).to.be.a('array').that.to.have.lengthOf(0);

                            done();
                        });
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('weekRepeat should be successful with no pickups', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_WEEKLY_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;
            trip.days[getDayOfWeek()] = true;
            trip.weekRepeatOpenSeats[getDayOfWeek()] = 1;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return Promise.resolve();
            }).then(() => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule/${trip.tid}/activate`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('aid');

                        expect(res.body.data.aid).to.be.a('string').that.to.not.have.lengthOf(0);

                        trip.aid = res.body.data.aid;

                        chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                            expect(res).to.have.status(200);

                            expect(res).to.be.json;

                            expect(res.body).to.have.all.keys('success', 'message', 'data');

                            expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                            expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                            expect(res.body.data).to.be.a('object').that.to.have.all.keys('trip');

                            expect(res.body.data.trip.sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                            expect(res.body.data.trip.aid).to.be.a('string').that.to.equal(trip.aid);
                            expect(res.body.data.trip.vid).to.be.a('string').that.to.equal(trip.vid);
                            expect(res.body.data.trip.route).to.be.a('array').that.to.have.lengthOf(1).that.to.deep.equal([trip.destination]);
                            expect(res.body.data.trip.pickups).to.be.a('array').that.to.have.lengthOf(0);
                            expect(res.body.data.trip.passengers).to.be.a('array').that.to.have.lengthOf(0);

                            done();
                        });
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('weekRepeat should be successful with pickups', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_WEEKLY_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;
            trip.days[getDayOfWeek()] = true;
            trip.weekRepeatOpenSeats[getDayOfWeek()] = 1;

            let lift = copyObject(objects.SCHEDULE_LIFT_WEEKLY_ONE);
            lift.days[getDayOfWeek()] = true;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return actions.scheduleLift(objects.USER_TWO.sid, lift, objects.USER_TWO.token);
            }).then(() => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule/${trip.tid}/activate`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('aid');

                        expect(res.body.data.aid).to.be.a('string').that.to.not.have.lengthOf(0);

                        trip.aid = res.body.data.aid;

                        chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                            expect(res).to.have.status(200);

                            expect(res).to.be.json;

                            expect(res.body).to.have.all.keys('success', 'message', 'data');

                            expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                            expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                            expect(res.body.data).to.be.a('object').that.to.have.all.keys('trip');

                            expect(res.body.data.trip.sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                            expect(res.body.data.trip.aid).to.be.a('string').that.to.equal(trip.aid);
                            expect(res.body.data.trip.vid).to.be.a('string').that.to.equal(trip.vid);
                            expect(res.body.data.trip.route).to.be.a('array').that.to.have.lengthOf(3).that.to.deep.equal([objects.SCHEDULE_LIFT_SINGLE_ONE.origin, objects.SCHEDULE_LIFT_SINGLE_ONE.destination, trip.destination]);
                            expect(res.body.data.trip.pickups).to.be.a('array').that.to.have.lengthOf(1);
                            expect(res.body.data.trip.passengers).to.be.a('array').that.to.have.lengthOf(0);

                            done();
                        });
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(aid=> {
                chai.request(server).post(`/api/v1/user/some_garbage_value/trip/schedule/${aid}/activate`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid TID', done => {

            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE}/trip/schedule/some_garbage_value/activate`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('routes', () => {
        it('singleDate #1', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return Promise.resolve();
            }).then(() => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('trips');

                        expect(res.body.data.trips[0].sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                        expect(res.body.data.trips[0].tid).to.be.a('string').that.to.equal(trip.tid);
                        expect(res.body.data.trips[0].vid).to.be.a('string').that.to.equal(trip.vid);
                        expect(res.body.data.trips[0].singleDateRoute).to.be.a('array').that.to.have.lengthOf(1).that.to.deep.equal([trip.destination]);
                        expect(res.body.data.trips[0].singleDatePassengers).to.be.a('array').that.to.have.lengthOf(0);

                        done();
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('singleDate #2', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return actions.scheduleLift(objects.USER_TWO.sid, objects.SCHEDULE_LIFT_SINGLE_ONE, objects.USER_TWO.token);
            }).then(lid => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('trips');

                        expect(res.body.data.trips[0].sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                        expect(res.body.data.trips[0].tid).to.be.a('string').that.to.equal(trip.tid);
                        expect(res.body.data.trips[0].vid).to.be.a('string').that.to.equal(trip.vid);
                        expect(res.body.data.trips[0].singleDateRoute).to.be.a('array').that.to.have.lengthOf(3).that.to.deep.equal([
                            objects.SCHEDULE_LIFT_SINGLE_ONE.origin,
                            objects.SCHEDULE_LIFT_SINGLE_ONE.destination,
                            trip.destination
                        ]);
                        expect(res.body.data.trips[0].singleDatePassengers).to.be.a('array').that.to.have.lengthOf(1).that.to.deep.equal([lid]);

                        done();
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('singleDate #3', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                // By making both requests at the same time we also test the kue functionality
                return Promise.all([
                    actions.scheduleLift(objects.USER_TWO.sid, objects.SCHEDULE_LIFT_SINGLE_ONE, objects.USER_TWO.token),
                    actions.scheduleLift(objects.USER_THREE.sid, objects.SCHEDULE_LIFT_SINGLE_TWO, objects.USER_THREE.token)
                ]);
            }).then(result => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('trips');

                        expect(res.body.data.trips[0].sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                        expect(res.body.data.trips[0].tid).to.be.a('string').that.to.equal(trip.tid);
                        expect(res.body.data.trips[0].vid).to.be.a('string').that.to.equal(trip.vid);
                        expect(res.body.data.trips[0].singleDateRoute).to.be.a('array').that.to.have.lengthOf(5).that.to.deep.equal([
                            objects.SCHEDULE_LIFT_SINGLE_TWO.origin,
                            objects.SCHEDULE_LIFT_SINGLE_ONE.origin,
                            objects.SCHEDULE_LIFT_SINGLE_TWO.destination,
                            objects.SCHEDULE_LIFT_SINGLE_ONE.destination,
                            trip.destination
                        ]);
                        expect(res.body.data.trips[0].singleDatePassengers).to.be.a('array').that.to.have.lengthOf(2).that.to.deep.equal([result[0], result[1]]);

                        done();
                    });
                }, 15000);
            }).catch(err => {
                done(err);
            });
        });

        it('weekRepeat #1', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_WEEKLY_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return Promise.resolve();
            }).then(() => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('trips');

                        expect(res.body.data.trips[0].sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                        expect(res.body.data.trips[0].tid).to.be.a('string').that.to.equal(trip.tid);
                        expect(res.body.data.trips[0].vid).to.be.a('string').that.to.equal(trip.vid);
                        expect(res.body.data.trips[0].days).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
                        expect(res.body.data.trips[0].weekRepeatRoute).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
                        expect(res.body.data.trips[0].passengers).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');

                        expect(res.body.data.trips[0].days.monday).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.data.trips[0].days.tuesday).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.data.trips[0].days.wednesday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.thursday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.friday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.saturday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.sunday).to.be.a('boolean').that.to.equal(false);

                        expect(res.body.data.trips[0].weekRepeatRoute.monday).to.be.a('array').that.to.have.length(1).that.to.deep.equal([trip.destination]);
                        expect(res.body.data.trips[0].weekRepeatRoute.tuesday).to.be.a('array').that.to.have.length(1).that.to.deep.equal([trip.destination]);
                        expect(res.body.data.trips[0].weekRepeatRoute.wednesday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.thursday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.friday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.saturday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.sunday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);

                        expect(res.body.data.trips[0].passengers.monday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.tuesday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.wednesday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.thursday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.friday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.saturday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.sunday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);

                        done();
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('weekRepeat #2', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_WEEKLY_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return actions.scheduleLift(objects.USER_TWO.sid, objects.SCHEDULE_LIFT_WEEKLY_ONE, objects.USER_TWO.token);
            }).then(lid => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('trips');

                        expect(res.body.data.trips[0].sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                        expect(res.body.data.trips[0].tid).to.be.a('string').that.to.equal(trip.tid);
                        expect(res.body.data.trips[0].vid).to.be.a('string').that.to.equal(trip.vid);
                        expect(res.body.data.trips[0].days).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
                        expect(res.body.data.trips[0].weekRepeatRoute).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
                        expect(res.body.data.trips[0].passengers).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');

                        expect(res.body.data.trips[0].days.monday).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.data.trips[0].days.tuesday).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.data.trips[0].days.wednesday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.thursday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.friday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.saturday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.sunday).to.be.a('boolean').that.to.equal(false);

                        expect(res.body.data.trips[0].weekRepeatRoute.monday).to.be.a('array').that.to.have.length(3).that.to.deep.equal([objects.SCHEDULE_LIFT_WEEKLY_ONE.origin, objects.SCHEDULE_LIFT_WEEKLY_ONE.destination, trip.destination]);
                        expect(res.body.data.trips[0].weekRepeatRoute.tuesday).to.be.a('array').that.to.have.length(1).that.to.deep.equal([trip.destination]);
                        expect(res.body.data.trips[0].weekRepeatRoute.wednesday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.thursday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.friday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.saturday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.sunday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);

                        expect(res.body.data.trips[0].passengers.monday).to.be.a('array').that.to.have.length(1).that.to.deep.equal([lid]);
                        expect(res.body.data.trips[0].passengers.tuesday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.wednesday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.thursday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.friday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.saturday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.sunday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);

                        done();
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('weekRepeat #3', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_WEEKLY_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                // By making both requests at the same time we also test the kue functionality
                return Promise.all([
                    actions.scheduleLift(objects.USER_TWO.sid, objects.SCHEDULE_LIFT_WEEKLY_ONE, objects.USER_TWO.token),
                    actions.scheduleLift(objects.USER_THREE.sid, objects.SCHEDULE_LIFT_WEEKLY_TWO, objects.USER_THREE.token)
                ]);
            }).then(result => {
                // Wait a few seconds for the kue job to complete
                setTimeout(() => {
                    chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message', 'data');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                        expect(res.body.data).to.be.a('object').that.to.have.all.keys('trips');

                        expect(res.body.data.trips[0].sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                        expect(res.body.data.trips[0].tid).to.be.a('string').that.to.equal(trip.tid);
                        expect(res.body.data.trips[0].vid).to.be.a('string').that.to.equal(trip.vid);
                        expect(res.body.data.trips[0].days).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
                        expect(res.body.data.trips[0].weekRepeatRoute).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');
                        expect(res.body.data.trips[0].passengers).to.be.a('object').that.to.have.keys('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday');

                        expect(res.body.data.trips[0].days.monday).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.data.trips[0].days.tuesday).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.data.trips[0].days.wednesday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.thursday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.friday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.saturday).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.data.trips[0].days.sunday).to.be.a('boolean').that.to.equal(false);

                        expect(res.body.data.trips[0].weekRepeatRoute.monday).to.be.a('array').that.to.have.length(5).that.to.deep.equal([objects.SCHEDULE_LIFT_WEEKLY_TWO.origin, objects.SCHEDULE_LIFT_WEEKLY_ONE.origin, objects.SCHEDULE_LIFT_WEEKLY_TWO.destination, objects.SCHEDULE_LIFT_WEEKLY_ONE.destination, trip.destination]);
                        expect(res.body.data.trips[0].weekRepeatRoute.tuesday).to.be.a('array').that.to.have.length(3).that.to.deep.equal([objects.SCHEDULE_LIFT_WEEKLY_TWO.origin, objects.SCHEDULE_LIFT_WEEKLY_TWO.destination, trip.destination]);
                        expect(res.body.data.trips[0].weekRepeatRoute.wednesday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.thursday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.friday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.saturday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].weekRepeatRoute.sunday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);

                        expect(res.body.data.trips[0].passengers.monday).to.be.a('array').that.to.have.length(2).that.to.deep.equal([result[1], result[0]]);
                        expect(res.body.data.trips[0].passengers.tuesday).to.be.a('array').that.to.have.length(1).that.to.deep.equal([result[1]]);
                        expect(res.body.data.trips[0].passengers.wednesday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.thursday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.friday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.saturday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);
                        expect(res.body.data.trips[0].passengers.sunday).to.be.a('array').that.to.have.length(0).that.to.deep.equal([]);

                        done();
                    });
                }, 15000);
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('deleting', () => {
        it('trip should be successful', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return Promise.resolve();
            }).then(() => {
                chai.request(server).delete(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule/${trip.tid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('trip should fail as invalid SID', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return Promise.resolve();
            }).then(() => {
                chai.request(server).delete(`/api/v1/user/some_garbage_value/trip/schedule/${trip.tid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('trip should fail as invalid TID', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return Promise.resolve();
            }).then(() => {
                chai.request(server).delete(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule/some_garbage_value`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('lift should be successful', done => {
            actions.scheduleLift(objects.USER_TWO.sid, objects.SCHEDULE_LIFT_SINGLE_ONE, objects.USER_TWO.token).then(lid => {
                chai.request(server).delete(`/api/v1/user/${objects.USER_TWO.sid}/lift/schedule/${lid}`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('lift should fail as invalid SID', done => {
            actions.scheduleLift(objects.USER_TWO.sid, objects.SCHEDULE_LIFT_SINGLE_ONE, objects.USER_TWO.token).then(lid => {
                chai.request(server).delete(`/api/v1/user/some_garbage_value/lift/schedule/${lid}`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('lift fail as invalid LID', done => {
            actions.scheduleLift(objects.USER_TWO.sid, objects.SCHEDULE_LIFT_SINGLE_ONE, objects.USER_TWO.token).then(() => {
                chai.request(server).delete(`/api/v1/user/${objects.USER_TWO.sid}/lift/schedule/some_garbage_value`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('refresh', () => {
        it('singleDate lift should get new driver', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_SINGLE_ONE);
            let trip2 = copyObject(objects.SCHEDULE_TRIP_SINGLE_TWO);

            trip.vid = objects.VEHICLE_ONE.vid;
            trip2.vid = objects.VEHICLE_TWO.vid;

            let Lid;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return actions.scheduleLift(objects.USER_THREE.sid, objects.SCHEDULE_LIFT_SINGLE_ONE, objects.USER_THREE.token);
            }).then(lid => {
                Lid = lid;

                return actions.scheduleTrip(objects.USER_TWO.sid, trip2, objects.USER_TWO.token);
            }).then(() => {
                setTimeout(() => {
                    chai.request(server).delete(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule/${trip.tid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                        setTimeout(() => {

                            chai.request(server).get(`/api/v1/user/${objects.USER_TWO.sid}/trip/schedule`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                                expect(res).to.have.status(200);

                                expect(res).to.be.json;

                                expect(res.body).to.have.all.keys('success', 'message', 'data');

                                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                                expect(res.body.data).to.be.a('object').that.to.have.keys('trips');
                                expect(res.body.data.trips).to.be.a('array').that.to.have.lengthOf(1);
                                expect(res.body.data.trips[0]).to.be.a('object').that.to.contain.all.keys('singleDatePassengers');

                                expect(res.body.data.trips[0].singleDatePassengers).to.be.a('array').that.to.have.lengthOf(1).that.to.deep.equal([Lid]);
                                expect(res.body.data.trips[0].singleDateRoute).to.be.a('array').that.to.have.lengthOf(3).that.to.deep.equal([objects.SCHEDULE_LIFT_SINGLE_ONE.origin, objects.SCHEDULE_LIFT_SINGLE_ONE.destination, trip2.destination]);

                                done();
                            });
                        }, 20000);
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });

        it('weekRepeat lift should get new driver', done => {
            let trip = copyObject(objects.SCHEDULE_TRIP_WEEKLY_ONE);
            let trip2 = copyObject(objects.SCHEDULE_TRIP_WEEKLY_TWO);

            trip.vid = objects.VEHICLE_ONE.vid;
            trip.days[getDayOfWeek()] = true;
            trip.weekRepeatOpenSeats[getDayOfWeek()] = 1;

            trip2.vid = objects.VEHICLE_TWO.vid;
            trip2.days[getDayOfWeek()] = true;
            trip2.weekRepeatOpenSeats[getDayOfWeek()] = 1;

            let lift = copyObject(objects.SCHEDULE_LIFT_WEEKLY_ONE);
            lift.days[getDayOfWeek()] = true;

            actions.scheduleTrip(objects.USER_ONE.sid, trip, objects.USER_ONE.token).then(tid => {
                trip.tid = tid;

                return actions.scheduleLift(objects.USER_THREE.sid, lift, objects.USER_THREE.token);
            }).then(lid => {
                lift.lid = lid;

                return actions.scheduleTrip(objects.USER_TWO.sid, trip2, objects.USER_TWO.token);
            }).then(() => {
                setTimeout(() => {
                    chai.request(server).delete(`/api/v1/user/${objects.USER_ONE.sid}/trip/schedule/${trip.tid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                        expect(res).to.have.status(200);

                        expect(res).to.be.json;

                        expect(res.body).to.have.all.keys('success', 'message');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                        setTimeout(() => {

                            chai.request(server).get(`/api/v1/user/${objects.USER_TWO.sid}/trip/schedule`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                                expect(res).to.have.status(200);

                                expect(res).to.be.json;

                                expect(res.body).to.have.all.keys('success', 'message', 'data');

                                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                                expect(res.body.data).to.be.a('object').that.to.have.keys('trips');
                                expect(res.body.data.trips).to.be.a('array').that.to.have.lengthOf(1);
                                expect(res.body.data.trips[0]).to.be.a('object').that.to.contain.all.keys('passengers');

                                expect(res.body.data.trips[0].passengers[getDayOfWeek()]).to.be.a('array').that.to.have.lengthOf(1).that.to.deep.equal([lift.lid]);
                                expect(res.body.data.trips[0].weekRepeatRoute[getDayOfWeek()]).to.be.a('array').that.to.have.lengthOf(3).that.to.deep.equal([lift.origin, lift.destination, trip2.destination]);

                                done();
                            });
                        }, 20000);
                    });
                }, 10000);
            }).catch(err => {
                done(err);
            });
        });
    });
});
