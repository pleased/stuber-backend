/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// A collection of objects that is used in the testing of the server
module.exports = {

    USER_ONE: {
        fullname: "John Carmack",
        gid: "doom_is_life",
        age: 53
    },

    USER_TWO: {
        fullname: "Ironman",
        gid: "i_am_ironman",
        age: 45
    },

    USER_THREE: {
        fullname: "Bruce Wayne",
        gid: "I'm Batman",
        age: "45"
    },

    USER_FOUR: {
        fullname: "Peter Parker",
        gid: "Spidey",
        age: "25"
    },

    VEHICLE_ONE: {
        model: "Mustang Boss 500",
        make: "Ford",
        seats: 4,
        regNumber: "ITooCool",
        color: "black"
    },

    VEHICLE_TWO: {
        model: "911 GTR 3",
        make: "Porche",
        seats: 3,
        regNumber: "IAmCool",
        color: "royal blue"
    },

    TRIP_ONE: {
        destination: {
            lat: -33.92819,
            lon: 18.87567
        },
        origin: {
            lat: -33.93712,
            lon: 18.86020
        },
        currLoc: {
            lat: -33.93712,
            lon: 18.86020
        },

        startTime: {
            hour: 8,
            minute: 30
        },
        endTime: {
            hour: 8,
            minute: 50
        },

        openSeats: 1
    },

    LIFT_ONE: {
        destination: {
            lat: -33.92735,
            lon: 18.87413
        },
        origin: {
            lat: -33.93430,
            lon: 18.86800
        },

        startTime: {
            hour: 8,
            minute: 35
        },
        endTime: {
            hour: 8,
            minute: 50
        }
    },

    SCHEDULE_TRIP_SINGLE_ONE: {
        weekRepeat: false,
        singleDate: true,
        date: (new Date()).getTime(),

        destination: {
            lat: -33.92819,
            lon: 18.87567
        },
        origin: {
            lat: -33.93712,
            lon: 18.86020
        },

        startTime: {
            hour: 8,
            minute: 30
        },
        endTime: {
            hour: 8,
            minute: 55
        },

        singleDateOpenSeats: 2
    },

    SCHEDULE_TRIP_SINGLE_TWO: {
        weekRepeat: false,
        singleDate: true,
        date: (new Date()).getTime(),

        destination: {
            lat: -33.92819,
            lon: 18.87567
        },
        origin: {
            lat: -33.93712,
            lon: 18.86020
        },

        startTime: {
            hour: 8,
            minute: 30
        },
        endTime: {
            hour: 8,
            minute: 55
        },

        singleDateOpenSeats: 2
    },

    SCHEDULE_TRIP_WEEKLY_ONE: {
        weekRepeat: true,
        singleDate: false,

        days: {
            monday: true,
            tuesday: true,
            thursday: false
        },

        destination: {
            lat: -33.92819,
            lon: 18.87567
        },
        origin: {
            lat: -33.93712,
            lon: 18.86020
        },

        startTime: {
            hour: 8,
            minute: 30
        },
        endTime: {
            hour: 8,
            minute: 50
        },

        weekRepeatOpenSeats: {
            monday: 2,
            tuesday: 1,
            thursday: 1
        }
    },

    SCHEDULE_TRIP_WEEKLY_TWO: {
        weekRepeat: true,
        singleDate: false,

        days: {
            monday: true,
            tuesday: true,
            thursday: false
        },

        destination: {
            lat: -33.92819,
            lon: 18.87567
        },
        origin: {
            lat: -33.93712,
            lon: 18.86020
        },

        startTime: {
            hour: 8,
            minute: 30
        },
        endTime: {
            hour: 8,
            minute: 50
        },

        weekRepeatOpenSeats: {
            monday: 2,
            tuesday: 1,
            thursday: 1
        }
    },

    SCHEDULE_LIFT_SINGLE_ONE: {
        weekRepeat: false,
        singleDate: true,
        date: (new Date()).getTime(),

        destination: {
            lat: -33.92735,
            lon: 18.87413
        },
        origin: {
            lat: -33.93430,
            lon: 18.86800
        },

        startTime: {
            hour: 8,
            minute: 35
        },
        endTime: {
            hour: 10,
            minute: 30
        }
    },

    SCHEDULE_LIFT_SINGLE_TWO: {
        weekRepeat: false,
        singleDate: true,
        date: (new Date()).getTime(),

        destination: {
            lat: -33.93156,
            lon: 18.86772
        },
        origin: {
            lat: -33.93693,
            lon: 18.86350
        },

        startTime: {
            hour: 8,
            minute: 35
        },
        endTime: {
            hour: 9,
            minute: 5
        }
    },

    SCHEDULE_LIFT_WEEKLY_ONE: {
        weekRepeat: true,
        singleDate: false,

        days: {
            monday: true,
            wednesday: true,
            thursday: false,
            sunday: true
        },

        destination: {
            lat: -33.92735,
            lon: 18.87413
        },
        origin: {
            lat: -33.93430,
            lon: 18.86800
        },

        startTime: {
            hour: 8,
            minute: 35
        },
        endTime: {
            hour: 8,
            minute: 50
        }
    },

    SCHEDULE_LIFT_WEEKLY_TWO: {
        weekRepeat: true,
        singleDate: false,

        days: {
            monday: true,
            tuesday: true,
            friday: false,
            saturday: true
        },

        destination: {
            lat: -33.93156,
            lon: 18.86772
        },
        origin: {
            lat: -33.93693,
            lon: 18.86350
        },

        startTime: {
            hour: 8,
            minute: 35
        },
        endTime: {
            hour: 8,
            minute: 50
        }
    }
};
