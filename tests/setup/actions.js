/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
// A collection of actions that eases the testing of the server
let request = require('request');

module.exports = {
    registerUser: function (user) {
        return new Promise((resolve, reject) => {
            request.post('http://127.0.0.1:3000/api/v1/user', {json: user}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('registerUser fail');
                    reject(err || res.body);
                } else {
                    resolve([res.body.data.sid, res.body.data.token]);
                }
            });
        });
    },

    registerVehicle: function (token, vehicle) {
        return new Promise((resolve, reject) => {
            request.post('http://127.0.0.1:3000/api/v1/vehicle', {json: vehicle, headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('registerVehicle fail');
                    reject(err || res.body);
                } else {
                    resolve(res.body.data.vid);
                }
            });
        });
    },

    setUserDriverStatus: function (sid, token, status=true) {
        return new Promise((resolve, reject) => {
            request.put(`http://127.0.0.1:3000/api/v1/user/${sid}/driver`, {json: {isDriver: status}, headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('setUserDriverStatus fail');
                    reject(err || res.body);
                } else {
                    resolve();
                }
            });
        });
    },

    setTrip: function (sid, token, trip) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/trip`, {json: trip, headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('setTrip fail');
                    reject(err || res.body);
                } else {
                    resolve(res.body.data.aid);
                }
            });
        });
    },

    completeTrip: function (sid, token) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/trip/complete`, {headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('completeTrip fail');
                    reject(err || res.body);
                } else {
                    resolve();
                }
            });
        });
    },

    requestLift: function (sid, token, lift) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/lift`, {json: lift, headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('requestLift fail');
                    reject(err || res.body);
                } else {
                    resolve(res.body.data.matches);
                }
            });
        });
    },

    selectDriver: function (sid, aid, token) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/lift/selected`, {json: {aid: aid}, headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('selectDriver fail');
                    reject(err || res.body);
                } else {
                    resolve();
                }
            });
        });
    },

    driverDecide: function (sid, aid, passenger, decision, token) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/trip/${aid}/decided`, {json: {decided: decision, passenger: passenger}, headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('driverDecide fail');
                    reject(err || res.body);
                } else {
                    resolve();
                }
            });
        });
    },

    pickupPassenger: function (sid, token) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/lift/pickup`, {headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('pickupPassenger fail');
                    reject(err || res.body);
                } else {
                    resolve();
                }
            });
        });
    },

    dropoffPassenger: function (sid, token) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/lift/complete`, {headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('dropoffPassenger fail');
                    reject(err || res.body);
                } else {
                    resolve();
                }
            });
        });
    },

    scheduleTrip: function (sid, trip, token) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/trip/schedule`, {json: trip, headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('scheduleTrip fail');
                    reject(err || res.body);
                } else {
                    resolve(res.body.data.tid);
                }
            });
        });
    },

    scheduleLift: function (sid, lift, token) {
        return new Promise((resolve, reject) => {
            request.post(`http://127.0.0.1:3000/api/v1/user/${sid}/lift/schedule`, {json: lift, headers: {'x-access-token': token}}, (err, res) => {
                if (err || res.statusCode !== 200) {
                    console.log('scheduleLift fail');
                    reject(err || res.body);
                } else {
                    resolve(res.body.data.lid);
                }
            });
        });
    }
};
