/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let fs = require('fs');

let server = require('../server.js');
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let objects = require('./setup/objects.js');
let actions = require('./setup/actions.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;

chai.use(chaiHttp);

// Clear the database before starting the tests
before(done => {

    Promise.all([
        mdb.dropCollections(),
        rdb.dropCollections()
    ]).then(() => {
        return actions.registerUser(objects.USER_ONE);
    }).then(res => {
        objects.USER_ONE.sid = res[0];
        objects.USER_ONE.token = res[1];

        objects.VEHICLE_ONE.sid = res[0];
        objects.VEHICLE_TWO.sid = res[0];

        done();
    }).catch(err => {
        done(err);
    });
});

// Parent block
describe('Vehicle', () => {

    describe('user vehicle list before adding vehicles', () => {
        it('should be empty', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/vehicles`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });
    });

    describe('create new vehicle', () => {
        it('should be successful', done => {
            chai.request(server).post('/api/v1/vehicle').set('x-access-token', objects.USER_ONE.token).send(objects.VEHICLE_ONE).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('vid');
                expect(res.body.data.vid).to.be.a('string').that.to.not.have.lengthOf(0);

                objects.VEHICLE_ONE.vid = res.body.data.vid;

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).post('/api/v1/vehicle').set('x-access-token', objects.USER_ONE.token).send(objects.VEHICLE_TWO).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('vid');
                expect(res.body.data.vid).to.be.a('string').that.to.not.have.lengthOf(0);

                objects.VEHICLE_TWO.vid = res.body.data.vid;

                done();
            });
        });

        it('should fail as missing field', done => {

            delete objects.VEHICLE_TWO.make;

            chai.request(server).post('/api/v1/vehicle').set('x-access-token', objects.USER_ONE.token).send(objects.VEHICLE_TWO).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                // Add the removed field to the object again
                objects.VEHICLE_TWO.make = 'Porche';

                done();
            });
        });

        it('should fail as duplicate regNumber', done => {
            chai.request(server).post('/api/v1/vehicle').set('x-access-token', objects.USER_ONE.token).send(objects.VEHICLE_TWO).end((err, res) => {

                expect(res.statusCode).to.equal(400);

                expect(res).to.be.json;
                expect(res.body).to.have.property('success');
                expect(res.body.success).to.equal(false);

                expect(res.body).to.have.property('message');

                done();
            });
        });
    });

    describe('user vehicle list after adding vehicles', () => {
        it('should not be empty', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/vehicles`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('array');
                expect(res.body.data).to.have.lengthOf(2);

                // Setup the correct object fields
                objects.VEHICLE_ONE.picture = '';
                objects.VEHICLE_TWO.picture = '';
                objects.VEHICLE_ONE.owners = [objects.VEHICLE_ONE.sid];
                objects.VEHICLE_TWO.owners = [objects.VEHICLE_TWO.sid];

                delete objects.VEHICLE_ONE.sid;
                delete objects.VEHICLE_TWO.sid;

                // TODO: Not working now as it also takes order of keys into account :(
                expect(res.body.data[0]).to.have.all.keys('vid', 'model', 'make', 'color', 'regNumber', 'owners', 'seats', 'picture').that.to.deep.equal(objects.VEHICLE_ONE);
                expect(res.body.data[1]).to.have.all.keys('vid', 'model', 'make', 'color', 'regNumber', 'owners', 'seats', 'picture').that.to.deep.equal(objects.VEHICLE_TWO);

                done();
            });
        });
    });

    describe('uploading image', () => {
        it('should fail as invalid VID', done => {
            chai.request(server).post(`/api/v1/vehicle/just_some_random_text/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as no image file', done => {
            chai.request(server).post(`/api/v1/vehicle/${objects.VEHICLE_ONE.vid}/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).post(`/api/v1/vehicle/${objects.VEHICLE_ONE.vid}/image`).set('x-access-token', objects.USER_ONE.token).attach("file", fs.readFileSync("./tests/vehicle_pic.jpg"), "file").end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('retrieving image', () => {
        it('should fail as invalid VID', done => {
            chai.request(server).get(`/api/v1/vehicle/just_some_random_text/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be empty', done => {
            chai.request(server).get(`/api/v1/vehicle/${objects.VEHICLE_TWO.vid}/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).get(`/api/v1/vehicle/${objects.VEHICLE_ONE.vid}/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                done();
            });
        });
    });

    describe('deleting', () => {
        it('should fail as invalid VID', done => {
            chai.request(server).delete(`/api/v1/vehicle/just_some_random_text`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).delete(`/api/v1/vehicle/${objects.VEHICLE_TWO.vid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('retrieving', () => {
        it('should fail as invalid VID', done => {
            chai.request(server).get(`/api/v1/vehicle/just_some_random_text`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                objects.VEHICLE_ONE.picture = objects.VEHICLE_ONE.vid + '.file';

                done();
            });
        });

        it('should fail as vehicle deleted', done => {
            chai.request(server).get(`/api/v1/vehicle/${objects.VEHICLE_TWO.vid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).get(`/api/v1/vehicle/${objects.VEHICLE_ONE.vid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('object');

                // TODO: Not working now as it also takes order of keys into account :(
                expect(res.body.data).to.have.all.keys('vid', 'model', 'make', 'color', 'regNumber', 'owners', 'seats', 'picture').that.to.deep.equal(objects.VEHICLE_ONE);


                done();
            });
        });
    });

    describe('user vehicle list after adding vehicles', () => {
        it('should not be empty', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/vehicles`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('array');
                expect(res.body.data).to.have.lengthOf(1);

                // TODO: Not working now as it also takes order of keys into account :(
                expect(res.body.data[0]).to.have.all.keys('vid', 'model', 'make', 'color', 'regNumber', 'owners', 'seats', 'picture').that.to.equal(objects.VEHICLE_ONE);

                done();
            });
        });
    });
});
