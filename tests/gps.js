/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let server = require('../server.js');
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let objects = require('./setup/objects.js');
let actions = require('./setup/actions.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;

chai.use(chaiHttp);

// Clear the database before starting the tests
beforeEach(done => {

    Promise.all([
        mdb.dropCollections(),
        rdb.dropCollections()
    ]).then(() => {
        return Promise.all([
            actions.registerUser(objects.USER_ONE),
            actions.registerUser(objects.USER_TWO)
        ]);
    }).then(res => {
        objects.USER_ONE.sid = res[0][0];
        objects.USER_ONE.token = res[0][1];

        objects.USER_TWO.sid = res[1][0];
        objects.USER_TWO.token = res[1][1];

        objects.VEHICLE_ONE.sid = res[0][0];

        return Promise.all([
            actions.setUserDriverStatus(res[0][0], res[0][1]),
            actions.registerVehicle(res[0][1], objects.VEHICLE_ONE)
        ]);
    }).then(res => {
        objects.TRIP_ONE.vid = res[1];

        return actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, objects.TRIP_ONE);
    }).then(() => {
        done();
    }).catch(err => {
        done(err);
    });
});

// Parent block
describe('GPS', () => {

    describe('update', () => {
        it('should be successful', done => {
            chai.request(server).put(`/api/v1/gps/${objects.USER_ONE.sid}`).set('x-access-token', objects.USER_ONE.token).send({
                lat: 33.33,
                lon: 66.66
            }).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as user not driver of trip', done => {
            chai.request(server).put(`/api/v1/gps/${objects.USER_TWO.sid}`).set('x-access-token', objects.USER_TWO.token).send({
                lat: 33.33,
                lon: 66.66
            }).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as missing field', done => {
            chai.request(server).put(`/api/v1/gps/${objects.USER_ONE.sid}`).set('x-access-token', objects.USER_ONE.token).send({
                lat: 33.33
            }).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as invalid SID', done => {
            chai.request(server).put(`/api/v1/gps/some_garbage_value`).set('x-access-token', objects.USER_ONE.token).send({
                lat: 33.33,
                lon: 66.66
            }).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('retrieval', () => {
        it('should fail as user not driver of trip', done => {
            chai.request(server).get(`/api/v1/gps/${objects.USER_TWO.sid}`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).get(`/api/v1/gps/${objects.USER_ONE.sid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('object').that.to.have.all.keys('lat', 'lon').that.to.deep.equal(objects.TRIP_ONE.currLoc);

                done();
            });
        });

        it('should get new after update', done => {
            chai.request(server).put(`/api/v1/gps/${objects.USER_ONE.sid}`).set('x-access-token', objects.USER_ONE.token).send({
                lat: 33.33,
                lon: 66.66
            }).end(err => {

                if (err) {
                    return done(err);
                }

                chai.request(server).get(`/api/v1/gps/${objects.USER_ONE.sid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message', 'data');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                    expect(res.body.data).to.be.a('object').that.to.have.all.keys('lat', 'lon').that.to.deep.equal({
                        lat: 33.33,
                        lon: 66.66
                    });

                    done();
                });
            });
        });
    });

    describe('retrieval all', () => {
        it('should be empty', done => {
            actions.completeTrip(objects.USER_ONE.sid, objects.USER_ONE.token).then(() => {
                chai.request(server).get(`/api/v1/gps`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(204);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should not be empty', done => {
            chai.request(server).get(`/api/v1/gps`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('array').that.to.have.lengthOf(1);

                expect(res.body.data[0]).to.have.all.keys('sid', 'currLoc');
                expect(res.body.data[0].sid).to.equal(objects.USER_ONE.sid);
                expect(res.body.data[0].currLoc).to.have.all.keys('lon', 'lat').that.to.deep.equal(objects.TRIP_ONE.currLoc);

                done();
            });
        });
    });
});
