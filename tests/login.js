/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let server = require('../server.js');
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let objects = require('./setup/objects.js');
let actions = require('./setup/actions.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;

chai.use(chaiHttp);

// Clear the database before starting the tests
before(function(done) {

    Promise.all([
        mdb.dropCollections(),
        rdb.dropCollections()
    ]).then(() => {
        return actions.registerUser(objects.USER_ONE);
    }).then(res => {
        objects.USER_ONE.sid = res[0];
        objects.USER_ONE.token = res[1];

        done();
    }).catch(err => {
        done(err);
    });
});

// Parent block
describe('Login', () => {

    describe('attempt', () => {
        it('should be successful', (done) => {
            chai.request(server).post('/api/v1/login').send({gid: objects.USER_ONE.gid}).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('sid', 'token');
                expect(res.body.data.sid).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data.token).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data.sid).to.be.equal(objects.USER_ONE.sid);
                expect(res.body.data.token).to.be.equal(objects.USER_ONE.token);

                objects.USER_ONE.token_2 = res.body.data.token;

                done();
            });
        });

        it('should fail as invalid GID', (done) => {
            chai.request(server).post('/api/v1/login').send({gid: 'GARBAGE'}).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as no GID provided', (done) => {
            chai.request(server).post('/api/v1/login').end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });
});
