/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let fs = require('fs');

let server = require('../server.js');
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let objects = require('./setup/objects.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;

chai.use(chaiHttp);

// Clear the database before starting the tests
before(done => {

    Promise.all([
        mdb.dropCollections(),
        rdb.dropCollections()
    ]).then(() => {
        done();
    });
});

// Parent block
describe('User', () => {

    describe('creation', () => {
        it('should be successful', done => {
            chai.request(server).post('/api/v1/user').send(objects.USER_ONE).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('sid', 'token');
                expect(res.body.data.sid).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data.token).to.be.a('string').that.to.not.have.lengthOf(0);

                objects.USER_ONE.sid = res.body.data.sid;
                objects.USER_ONE.token = res.body.data.token;

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).post('/api/v1/user').send(objects.USER_TWO).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('sid', 'token');
                expect(res.body.data.sid).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data.token).to.be.a('string').that.to.not.have.lengthOf(0);

                objects.USER_TWO.sid = res.body.data.sid;
                objects.USER_TWO.token = res.body.data.token;

                done();
            });
        });

        it('should fail as duplicate GID', done => {
            chai.request(server).post('/api/v1/user').send(objects.USER_ONE).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('set as driver', () => {
        it('should fail as no isDriver field', done => {
            chai.request(server).put(`/api/v1/user/${objects.USER_ONE.sid}/driver`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be sucessfull', done => {
            chai.request(server).put(`/api/v1/user/${objects.USER_ONE.sid}/driver`).set('x-access-token', objects.USER_ONE.token).send({isDriver: true}).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as invalid SID', done => {
            chai.request(server).put(`/api/v1/user/just_some_random_text/driver`).set('x-access-token', objects.USER_ONE.token).send({'isDriver': true}).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('retrieve driver status', () => {
        it('should be false', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_TWO.sid}`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('fullname', 'age', 'sid', 'picture', 'tripHistory', 'liftHistory', 'isDriver', 'rating', 'likes', 'dislikes', 'address', 'email');
                expect(res.body.data.isDriver).to.be.a('boolean').that.to.equal(false);

                done();
            });
        });

        it('should be true', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('fullname', 'age', 'sid', 'picture', 'tripHistory', 'liftHistory', 'isDriver', 'rating', 'likes', 'dislikes', 'address', 'email');
                expect(res.body.data.isDriver).to.be.a('boolean').that.to.equal(true);

                done();
            });
        });
    });

    describe('uploading profile image', () => {
        it('should fail as invalid SID', done => {
            chai.request(server).get(`/api/v1/user/just_some_random_text/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as no image file', done => {
            chai.request(server).post(`/api/v1/user/just_some_random_text/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/image`).set('x-access-token', objects.USER_ONE.token).attach("file", fs.readFileSync("./tests/user_pic.jpg"), "file").end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('retrieving profile image', () => {
        it('should fail as invalid SID', done => {
            chai.request(server).get(`/api/v1/user/just_some_random_text/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be empty', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_TWO.sid}/image`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/image`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                done();
            });
        });
    });

    describe('deletion', () => {
        it('should fail as invalid SID', done => {
            chai.request(server).delete(`/api/v1/user/some_random_text`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).delete(`/api/v1/user/${objects.USER_TWO.sid}`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('retrieval', () => {
        it('should be successful', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('fullname', 'age', 'sid', 'picture', 'tripHistory', 'liftHistory', 'isDriver', 'rating', 'likes', 'dislikes', 'address', 'email');
                expect(res.body.data.sid).to.be.a('string').that.to.equal(objects.USER_ONE.sid);
                expect(res.body.data.age).to.be.a('number').that.to.equal(objects.USER_ONE.age);
                expect(res.body.data.fullname).to.be.a('string').that.to.equal(objects.USER_ONE.fullname);

                done();
            });
        });

        it('should fail as SID does not exist', done => {
            chai.request(server).get(`/api/v1/user/some_random_text`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as user deleted', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_TWO.sid}`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('retrieving inbox', () => {
        it('should fail as invalid SID', done => {
            chai.request(server).get(`/api/v1/user/just_some_random_text/inbox`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be empty', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/inbox`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });
    });
});
