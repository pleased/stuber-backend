/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let server = require('../server.js');
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');
let config = require('../config.js');

let objects = require('./setup/objects.js');
let actions = require('./setup/actions.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;

chai.use(chaiHttp);

const copyObject = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

// Clear the database before starting the tests
beforeEach(done => {

    Promise.all([
        mdb.dropCollections(),
        rdb.dropCollections()
    ]).then(() => {
        return Promise.all([
            actions.registerUser(objects.USER_ONE),
            actions.registerUser(objects.USER_TWO),
            actions.registerUser(objects.USER_THREE),
            actions.registerUser(objects.USER_FOUR)
        ]);
    }).then(res => {
        objects.USER_ONE.sid = res[0][0];
        objects.USER_ONE.token = res[0][1];

        objects.USER_TWO.sid = res[1][0];
        objects.USER_TWO.token = res[1][1];

        objects.USER_THREE.sid = res[2][0];
        objects.USER_THREE.token = res[2][1];

        objects.USER_THREE.sid = res[3][0];
        objects.USER_THREE.token = res[3][1];

        objects.VEHICLE_ONE.sid = objects.USER_ONE.sid;
        objects.VEHICLE_TWO.sid = objects.USER_TWO.sid;

        // Register vehicles and make users drivers
        return Promise.all([
            actions.setUserDriverStatus(objects.USER_ONE.sid, objects.USER_ONE.token),
            actions.setUserDriverStatus(objects.USER_TWO.sid, objects.USER_TWO.token),

            actions.registerVehicle(objects.USER_ONE.token, objects.VEHICLE_ONE),
            actions.registerVehicle(objects.USER_TWO.token, objects.VEHICLE_TWO)
        ]);
    }).then(res => {
        objects.VEHICLE_ONE.vid = res[2];
        objects.VEHICLE_TWO.vid = res[3];

        done();
    }).catch(err => {
        done(err);
    });
});

// Parent block
describe('Active', () => {

    describe('set trip', () => {
        it('should be successful', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                expect(res.body.data).to.be.a('object');
                expect(res.body.data).to.have.all.keys('aid');
                expect(res.body.data.aid).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as user already on trip', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as no vehicle supplied', done => {

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).send(objects.TRIP_ONE).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as no destination provided', done => {

            let trip = copyObject(objects.TRIP_ONE);
            delete trip.destination;
            trip.vid = objects.VEHICLE_ONE.vid;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as user is not owner of vehicle', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_TWO.vid;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as user is not a driver', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setUserDriverStatus(objects.USER_ONE.sid, objects.USER_ONE.token, false).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            chai.request(server).post(`/api/v1/user/some_garbage_value/trip`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as more seats specified than vehicle supports', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;
            trip.openSeats = objects.VEHICLE_ONE.seats + 1;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).send(trip).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('requesting lift', () => {
        it('should be successful with matches', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift`).set('x-access-token', objects.USER_TWO.token).send(objects.LIFT_ONE).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message', 'data');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    expect(res.body.data).to.be.a('object');
                    expect(res.body.data).to.have.all.keys('matches');
                    expect(res.body.data.matches).to.be.a('array').that.to.have.lengthOf(1);

                    done();
                });
            }).catch(err => {
               done(err);
            });
        });

        it('should be successful with no matches', done => {

            chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift`).set('x-access-token', objects.USER_TWO.token).send(objects.LIFT_ONE).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });

        it('should fail as user already on a lift', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(matches => {
                return actions.selectDriver(objects.USER_TWO.sid, matches[0], objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift`).set('x-access-token', objects.USER_TWO.token).send(objects.LIFT_ONE).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as user driver of trip', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/lift`).set('x-access-token', objects.USER_ONE.token).send(objects.LIFT_ONE).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as user already on waiting list', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(matches => {
                return actions.selectDriver(objects.USER_TWO.sid, matches[0], objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift`).set('x-access-token', objects.USER_TWO.token).send(objects.LIFT_ONE).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res).to.be.json;

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('retrieving trip', () => {
        it('should be successful', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(() => {
                chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message', 'data');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    expect(res.body.data).to.be.a('object');

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should be successful with no content', done => {
            chai.request(server).get(`/api/v1/user/${objects.USER_TWO.sid}/trip`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });

        it('should fail as invalid SID', done => {
            chai.request(server).get(`/api/v1/user/some_garbage_value/trip`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('passenger selected', () => {
        it('should fail as user not on waiting list', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/selected`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid AID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(() => {
                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/selected`).set('x-access-token', objects.USER_TWO.token).send({
                    aid: 'some_garbage_value'
                }).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as user already selected driver', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/selected`).set('x-access-token', objects.USER_TWO.token).send({
                    aid: trip.aid
                }).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as selection period has expired', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                setTimeout(() => { // Wait x amount so that the driver lock expires
                    chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/selected`).set('x-access-token', objects.USER_TWO.token).send({
                        aid: trip.aid
                    }).end((err, res) => {

                        expect(res).to.have.status(400);

                        expect(res.body).to.have.all.keys('success', 'message');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                        done();
                    });
                }, config.ACTIVE_LOCK_TIME);
            }).catch(err => {
                done(err);
            });
        });

        it('should be successful', done => {
            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/selected`).set('x-access-token', objects.USER_TWO.token).send({
                    aid: trip.aid
                }).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('driver decided', () => {
        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/some_garbage_value/trip/${trip.aid}/decided`).set('x-access-token', objects.USER_ONE.token).send({
                    decided: true,
                    passenger: objects.USER_TWO.sid
                }).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid AID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/some_garbage_value/decided`).set('x-access-token', objects.USER_ONE.token).send({
                    decided: true,
                    passenger: objects.USER_TWO.sid
                }).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as passenger does not exist', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/${trip.aid}/decided`).set('x-access-token', objects.USER_ONE.token).send({
                    decided: true,
                    passenger: 'some_garbage_value'
                }).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as passenger not on waiting list', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/${trip.aid}/decided`).set('x-access-token', objects.USER_ONE.token).send({
                decided: true,
                passenger: objects.USER_TWO.sid
            }).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as selection period has expired', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                setTimeout(() => {
                    chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/${trip.aid}/decided`).set('x-access-token', objects.USER_ONE.token).send({
                        decided: true,
                        passenger: objects.USER_TWO.sid
                    }).end((err, res) => {

                        expect(res).to.have.status(400);

                        expect(res.body).to.have.all.keys('success', 'message');

                        expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                        expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                        done();
                    });
                }, config.ACTIVE_LOCK_TIME);
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as missing field', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/${trip.aid}/decided`).set('x-access-token', objects.USER_ONE.token).send({
                    passenger: objects.USER_TWO.sid
                }).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('accept should be successful', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/${trip.aid}/decided`).set('x-access-token', objects.USER_ONE.token).send({
                    decided: true,
                    passenger: objects.USER_TWO.sid
                }).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('false should be successful', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/${trip.aid}/decided`).set('x-access-token', objects.USER_ONE.token).send({
                    decided: false,
                    passenger: objects.USER_TWO.sid
                }).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('pickup passenger', () => {
        it('should be successful', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/pickup`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as user not on lift', done => {
            chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/pickup`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as user not on pickup list', done => {
            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                return actions.pickupPassenger(objects.USER_TWO.sid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/pickup`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/some_garbage_value/lift/pickup`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('dropoff passenger', () => {
        it('should be successful', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                return actions.pickupPassenger(objects.USER_TWO.sid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/complete`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as user not on lift', done => {
            chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/complete`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as user not on dropoff list', done => {
            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_TWO.sid}/lift/complete`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                return actions.pickupPassenger(objects.USER_TWO.sid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/some_garbage_value/lift/complete`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('trip complete', () => {
        it('should be successful without passenger', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/complete`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should be successful with passenger', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                return actions.pickupPassenger(objects.USER_TWO.sid, objects.USER_TWO.token);
            }).then(() => {
                return actions.dropoffPassenger(objects.USER_TWO.sid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/complete`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as pickup still present', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/complete`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as dropoff still present', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                return actions.pickupPassenger(objects.USER_TWO.sid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/complete`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as user not driver of trip', done => {

            chai.request(server).post(`/api/v1/user/${objects.USER_ONE.sid}/trip/complete`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                return actions.pickupPassenger(objects.USER_TWO.sid, objects.USER_TWO.token);
            }).then(() => {
                return actions.dropoffPassenger(objects.USER_TWO.sid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).post(`/api/v1/user/some_garbage_value/trip/complete`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });

    describe('trip requests', () => {
        it('should be successful with no results (no passengers)', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;
            }).then(() => {
                chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/requests/${trip.aid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(204);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should be successful with no results (driver decided)', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                return actions.driverDecide(objects.USER_ONE.sid, trip.aid, objects.USER_TWO.sid, true, objects.USER_ONE.token);
            }).then(() => {
                chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/requests/${trip.aid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(204);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should be successful with result', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/requests/${trip.aid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(200);

                    expect(res.body).to.have.all.keys('success', 'message', 'data');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                    expect(res.body.data).to.be.a('object').that.to.have.all.keys('passenger');
                    // TODO: Expand to check passenger object

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid SID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).get(`/api/v1/user/some_garbage_value/trip/requests/${trip.aid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });

        it('should fail as invalid AID', done => {

            let trip = copyObject(objects.TRIP_ONE);
            trip.vid = objects.VEHICLE_ONE.vid;

            actions.setTrip(objects.USER_ONE.sid, objects.USER_ONE.token, trip).then(aid => {
                trip.aid = aid;

                return actions.requestLift(objects.USER_TWO.sid, objects.USER_TWO.token, objects.LIFT_ONE);
            }).then(() => {
                return actions.selectDriver(objects.USER_TWO.sid, trip.aid, objects.USER_TWO.token);
            }).then(() => {
                chai.request(server).get(`/api/v1/user/${objects.USER_ONE.sid}/trip/requests/some_garbage_value`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                    expect(res).to.have.status(400);

                    expect(res.body).to.have.all.keys('success', 'message');

                    expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                    expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                    done();
                });
            }).catch(err => {
                done(err);
            });
        });
    });
});
