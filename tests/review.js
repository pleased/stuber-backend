/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/. */
//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let server = require('../server.js');
let mdb = require('../database/mdb.js');
let rdb = require('../database/rdb.js');

let objects = require('./setup/objects.js');
let actions = require('./setup/actions.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;

chai.use(chaiHttp);

// Clear the database before starting the tests
before(done => {

    Promise.all([
        mdb.dropCollections(),
        rdb.dropCollections()
    ]).then(() => {
        return Promise.all([
            actions.registerUser(objects.USER_ONE),
            actions.registerUser(objects.USER_TWO)
        ]);
    }).then(res => {
        objects.USER_ONE.sid = res[0][0];
        objects.USER_ONE.token = res[0][1];

        objects.USER_TWO.sid = res[1][0];
        objects.USER_TWO.token = res[1][1];

        done();
    }).catch(err => {
        done(err);
    });
});

// Parent block
describe('Review', () => {

    describe('submit', () => {
        it('should fail as invalid reviewer', done => {
            chai.request(server).post('/api/v1/review').set('x-access-token', objects.USER_TWO.token).send({
                rating: 5.0,
                message: "Would go again anytime",
                reviewed: objects.USER_ONE.sid,
                reviewer: "some_garbage_value"
            }).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as invalid reviewed', done => {
            chai.request(server).post('/api/v1/review').set('x-access-token', objects.USER_ONE.token).send({
                rating: 5.0,
                message: "Would go again anytime",
                reviewed: "some_garbage_value",
                reviewer: objects.USER_ONE.sid
            }).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should fail as missing rating', done => {
            chai.request(server).post('/api/v1/review').set('x-access-token', objects.USER_ONE.token).send({
                message: "Would go again anytime",
                reviewed: objects.USER_TWO.sid,
                reviewer: objects.USER_ONE.sid
            }).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });

        it('should be successful', done => {
            chai.request(server).post('/api/v1/review').set('x-access-token', objects.USER_ONE.token).send({
                rating: 5.0,
                message: "Would go again anytime",
                reviewed: objects.USER_TWO.sid,
                reviewer: objects.USER_ONE.sid
            }).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });

    describe('retrieval', () => {
        it('should be empty', done => {
            chai.request(server).get(`/api/v1/review/${objects.USER_ONE.sid}`).set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(204);

                done();
            });
        });

        it('should not be empty', done => {
            chai.request(server).get(`/api/v1/review/${objects.USER_TWO.sid}`).set('x-access-token', objects.USER_TWO.token).end((err, res) => {

                expect(res).to.have.status(200);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message', 'data');

                expect(res.body.success).to.be.a('boolean').that.to.equal(true);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);
                expect(res.body.data).to.be.a('array').that.to.have.lengthOf(1);

                expect(res.body.data[0]).to.have.all.keys('rating', 'message', 'reviewed', 'reviewer').that.to.deep.equal({
                    rating: 5.0,
                    message: "Would go again anytime",
                    reviewed: objects.USER_TWO.sid,
                    reviewer: objects.USER_ONE.sid
                });

                done();
            });
        });

        it('should fail as invalid SID', done => {
            chai.request(server).get('/api/v1/review/some_garbage_value').set('x-access-token', objects.USER_ONE.token).end((err, res) => {

                expect(res).to.have.status(400);

                expect(res).to.be.json;

                expect(res.body).to.have.all.keys('success', 'message');

                expect(res.body.success).to.be.a('boolean').that.to.equal(false);
                expect(res.body.message).to.be.a('string').that.to.not.have.lengthOf(0);

                done();
            });
        });
    });
});
